using Ionimaging
import DelimitedFiles: readdlm
import Printf
using Test

const Image = Ionimaging.Image
const Pco = Ionimaging.Pco

const testimage1 = [ 0 0 0 0 0 0 0 0 0
                     0 1 2 1 0 0 0 0 0
                     0 2 3 2 0 0 0 0 0
                     0 1 2 1 0 0 0 0 0
                     0 0 0 0 0 0 0 0 0
                     0 0 0 0 0 0 0 0 0
                     0 0 0 0 0 0 0 0 0
                     0 0 0 0 0 0 0 0 0
                     0 0 0 0 0 0 0 0 0 ]

const testimage2 = [ 0 0 0 0 0 0 0 0 0
                     0 0 0 0 0 0 0 0 0
                     0 0 0 0 0 0 0 0 0
                     0 0 0 0 0 0 0 0 0
                     0 0 0 0 0 0 0 0 0
                     0 1 2 1 0 0 0 0 0
                     0 2 3 2 0 0 0 0 0
                     0 1 2 1 0 0 0 0 0
                     0 0 0 0 0 0 0 0 0 ]

const testimage3 = [ 0 0 0 0 0 0 0 0 0
                     0 0 0 0 0 1 2 1 0
                     0 0 0 0 0 2 3 2 0
                     0 0 0 0 0 1 2 1 0
                     0 0 0 0 0 0 0 0 0
                     0 0 0 0 0 0 0 0 0
                     0 0 0 0 0 0 0 0 0
                     0 0 0 0 0 0 0 0 0
                     0 0 0 0 0 0 0 0 0 ]

const testimage4 = [ 0 0 0 0 0 0 0 0 0
                     0 0 0 0 0 0 0 0 0
                     0 0 0 0 0 0 0 0 0
                     0 0 0 0 0 0 0 0 0
                     0 0 0 0 0 0 0 0 0
                     0 0 0 0 0 1 2 1 0
                     0 0 0 0 0 2 3 2 0
                     0 0 0 0 0 1 2 1 0
                     0 0 0 0 0 0 0 0 0 ]

const shape = size(testimage1)


@testset "Image processing" begin
    # Use the virtual camera
    Ionimaging.CONFIG["interface"]["code"] = Pco.API.PCO_INTERFACE_VIRTUAL

    # Suppress monitor window in CI
    if haskey(ENV, "CI")
        Ionimaging.CONFIG["measurement"]["monitor"] = false
    end


    @testset "Ionimaging.Image.background_correction!" begin
        @test Image.background_correction!(copy(testimage1), nothing) == testimage1
        @test Image.background_correction!(copy(testimage1), 1) == testimage1 .- 1
        @test Image.background_correction!(copy(testimage1), UInt16(1)) == testimage1 .- 1

        bg = ones(Int, shape)
        @test Image.background_correction!(copy(testimage1), bg) == testimage1 .- 1

        bg = ones(UInt16, shape)
        @test Image.background_correction!(copy(testimage1), bg) == testimage1 .- 1

        bg = [ 0 0 0 0 0 0 0 0 0
               0 0 0 0 0 0 0 0 0
               0 0 3 0 0 0 0 0 0
               0 0 0 0 0 0 0 0 0
               0 0 0 0 0 0 0 0 0
               0 0 0 0 0 0 0 0 0
               0 0 0 0 0 0 0 0 0
               0 0 0 0 0 0 0 0 0
               0 0 0 0 0 0 0 0 0 ]
        @test Image.background_correction!(copy(testimage1), bg) == [ 0 0 0 0 0 0 0 0 0
                                                                      0 1 2 1 0 0 0 0 0
                                                                      0 2 0 2 0 0 0 0 0
                                                                      0 1 2 1 0 0 0 0 0
                                                                      0 0 0 0 0 0 0 0 0
                                                                      0 0 0 0 0 0 0 0 0
                                                                      0 0 0 0 0 0 0 0 0
                                                                      0 0 0 0 0 0 0 0 0
                                                                      0 0 0 0 0 0 0 0 0 ]
    end


    @testset "Ionimaging.Image.ismasked" begin
        @test Image.ismasked(1, 1, []) == false
        @test Image.ismasked(1, 1, [(1, 1)]) == true
        @test Image.ismasked(1, 1, [(1, 2)]) == false
        @test Image.ismasked(1, 2, [(1, 2)]) == true
        @test Image.ismasked(2, 1, [(1, 2)]) == false
    end


    @testset "Ionimaging.Image.sum!" begin
        @test Image.sum!(zeros(3, 3), [1 0 0;0 0 0;0 0 0], nothing) == [1 0 0
                                                                        0 0 0
                                                                        0 0 0]
        @test Image.sum!(zeros(3, 3), [1 0 0;0 0 0;0 0 0], 1) == [ 0 -1 -1
                                                                  -1 -1 -1
                                                                  -1 -1 -1]
        @test Image.sum!(zeros(3, 3), [1 0 0;0 0 0;0 0 0], UInt16(1)) == [ 0 -1 -1
                                                                          -1 -1 -1
                                                                          -1 -1 -1]
        @test Image.sum!(zeros(3, 3), [1 0 0;0 0 0;0 0 0], 0.1) == [ 0.9 -0.1 -0.1
                                                                    -0.1 -0.1 -0.1
                                                                    -0.1 -0.1 -0.1]
        @test Image.sum!(zeros(3, 3), [1 0 0;0 0 0;0 0 0], zeros(3, 3)) == [1 0 0
                                                                            0 0 0
                                                                            0 0 0]
        @test Image.sum!(zeros(3, 3), [1 0 0;0 0 0;0 0 0], ones(3, 3)) == [ 0 -1 -1
                                                                           -1 -1 -1
                                                                           -1 -1 -1]
        @test Image.sum!(zeros(3, 3), [1 0 0;0 0 0;0 0 0], ones(UInt16, 3, 3)) == [ 0 -1 -1
                                                                                   -1 -1 -1
                                                                                   -1 -1 -1]
        @test Image.sum!(zeros(3, 3), [1 0 0;0 0 0;0 0 0], ones(3, 3).*0.1) == [ 0.9 -0.1 -0.1
                                                                                -0.1 -0.1 -0.1
                                                                                -0.1 -0.1 -0.1]
    end


    @testset "Ionimage.Image.isnoise" begin
        @test Image.isnoise([1 2 1;2 3 2;1 2 1], 1, 1, 1, nothing) == false
        @test Image.isnoise([1 2 1;2 3 2;1 2 1], 2, 1, 1, nothing) == false
        @test Image.isnoise([1 2 1;2 3 2;1 2 1], 2, 2, 1, nothing) == false

        @test Image.isnoise([1 2 1;2 3 2;1 2 1], 1, 1, 2, nothing) == true
        @test Image.isnoise([1 2 1;2 3 2;1 2 1], 2, 1, 2, nothing) == false
        @test Image.isnoise([1 2 1;2 3 2;1 2 1], 2, 2, 2, nothing) == false

        @test Image.isnoise([1 2 1;2 3 2;1 2 1], 1, 1, 3, nothing) == true
        @test Image.isnoise([1 2 1;2 3 2;1 2 1], 2, 1, 3, nothing) == true
        @test Image.isnoise([1 2 1;2 3 2;1 2 1], 2, 2, 3, nothing) == false

        @test Image.isnoise([1 2 1;2 3 2;1 2 1], 1, 1, 4, nothing) == true
        @test Image.isnoise([1 2 1;2 3 2;1 2 1], 2, 1, 4, nothing) == true
        @test Image.isnoise([1 2 1;2 3 2;1 2 1], 2, 2, 4, nothing) == true

        @test Image.isnoise([1 2 1;2 3 2;1 2 1], 1, 1, 1, 1) == true
        @test Image.isnoise([1 2 1;2 3 2;1 2 1], 2, 1, 1, 1) == false
        @test Image.isnoise([1 2 1;2 3 2;1 2 1], 2, 2, 1, 1) == false

        @test Image.isnoise([1 2 1;2 3 2;1 2 1], 1, 1, 1, 2) == true
        @test Image.isnoise([1 2 1;2 3 2;1 2 1], 2, 1, 1, 2) == true
        @test Image.isnoise([1 2 1;2 3 2;1 2 1], 2, 2, 1, 2) == false

        @test Image.isnoise([1 2 1;2 3 2;1 2 1], 1, 1, 1, 3) == true
        @test Image.isnoise([1 2 1;2 3 2;1 2 1], 2, 1, 1, 3) == true
        @test Image.isnoise([1 2 1;2 3 2;1 2 1], 2, 2, 1, 3) == true

        @test Image.isnoise([1 2 1;2 3 2;1 2 1], 1, 1, 1, ones(Int, 3, 3)) == true
        @test Image.isnoise([1 2 1;2 3 2;1 2 1], 2, 1, 1, ones(Int, 3, 3)) == false
        @test Image.isnoise([1 2 1;2 3 2;1 2 1], 2, 2, 1, ones(Int, 3, 3)) == false

        @test Image.isnoise([1 2 1;2 3 2;1 2 1], 1, 1, 1, ones(Int, 3, 3).*2) == true
        @test Image.isnoise([1 2 1;2 3 2;1 2 1], 2, 1, 1, ones(Int, 3, 3).*2) == true
        @test Image.isnoise([1 2 1;2 3 2;1 2 1], 2, 2, 1, ones(Int, 3, 3).*2) == false

        @test Image.isnoise([1 2 1;2 3 2;1 2 1], 1, 1, 1, ones(Int, 3, 3).*3) == true
        @test Image.isnoise([1 2 1;2 3 2;1 2 1], 2, 1, 1, ones(Int, 3, 3).*3) == true
        @test Image.isnoise([1 2 1;2 3 2;1 2 1], 2, 2, 1, ones(Int, 3, 3).*3) == true

        @test Image.isnoise([1 2 1;2 3 2;1 2 1], 1, 1, 1, ones(UInt16, 3, 3)) == true
        @test Image.isnoise([1 2 1;2 3 2;1 2 1], 2, 1, 1, ones(UInt16, 3, 3)) == false
        @test Image.isnoise([1 2 1;2 3 2;1 2 1], 2, 2, 1, ones(UInt16, 3, 3)) == false

        @test Image.isnoise([1 2 1;2 3 2;1 2 1], 1, 1, 1, ones(UInt16, 3, 3).*2) == true
        @test Image.isnoise([1 2 1;2 3 2;1 2 1], 2, 1, 1, ones(UInt16, 3, 3).*2) == true
        @test Image.isnoise([1 2 1;2 3 2;1 2 1], 2, 2, 1, ones(UInt16, 3, 3).*2) == false

        @test Image.isnoise([1 2 1;2 3 2;1 2 1], 1, 1, 1, ones(UInt16, 3, 3).*3) == true
        @test Image.isnoise([1 2 1;2 3 2;1 2 1], 2, 1, 1, ones(UInt16, 3, 3).*3) == true
        @test Image.isnoise([1 2 1;2 3 2;1 2 1], 2, 2, 1, ones(UInt16, 3, 3).*3) == true

        @test Image.isnoise([1 2 1;2 3 2;1 2 1], 1, 1, 1, ones(Float64, 3, 3)) == true
        @test Image.isnoise([1 2 1;2 3 2;1 2 1], 2, 1, 1, ones(Float64, 3, 3)) == false
        @test Image.isnoise([1 2 1;2 3 2;1 2 1], 2, 2, 1, ones(Float64, 3, 3)) == false

        @test Image.isnoise([1 2 1;2 3 2;1 2 1], 1, 1, 1, ones(Float64, 3, 3).*2) == true
        @test Image.isnoise([1 2 1;2 3 2;1 2 1], 2, 1, 1, ones(Float64, 3, 3).*2) == true
        @test Image.isnoise([1 2 1;2 3 2;1 2 1], 2, 2, 1, ones(Float64, 3, 3).*2) == false

        @test Image.isnoise([1 2 1;2 3 2;1 2 1], 1, 1, 1, ones(Float64, 3, 3).*3) == true
        @test Image.isnoise([1 2 1;2 3 2;1 2 1], 2, 1, 1, ones(Float64, 3, 3).*3) == true
        @test Image.isnoise([1 2 1;2 3 2;1 2 1], 2, 2, 1, ones(Float64, 3, 3).*3) == true
    end


    @testset "Ionimage.Image.ispeak" begin
        @test Image.ispeak([0 0 1 0 0;0 1 2 1 0;1 2 3 2 1;0 1 2 1 0;0 0 1 0 0], 2, 2, nothing) == false
        @test Image.ispeak([0 0 1 0 0;0 1 2 1 0;1 2 3 2 1;0 1 2 1 0;0 0 1 0 0], 3, 2, nothing) == false
        @test Image.ispeak([0 0 1 0 0;0 1 2 1 0;1 2 3 2 1;0 1 2 1 0;0 0 1 0 0], 3, 3, nothing) == true

        @test Image.ispeak([0 0 1 0 0;0 1 2 1 0;1 2 3 2 1;0 1 2 1 0;0 0 1 0 0], 2, 2, 1) == false
        @test Image.ispeak([0 0 1 0 0;0 1 2 1 0;1 2 3 2 1;0 1 2 1 0;0 0 1 0 0], 3, 2, 1) == false
        @test Image.ispeak([0 0 1 0 0;0 1 2 1 0;1 2 3 2 1;0 1 2 1 0;0 0 1 0 0], 3, 3, 1) == true

        @test Image.ispeak([0 0 1 0 0;0 1 2 1 0;1 2 3 2 1;0 1 2 1 0;0 0 1 0 0], 2, 2, 2) == false
        @test Image.ispeak([0 0 1 0 0;0 1 2 1 0;1 2 3 2 1;0 1 2 1 0;0 0 1 0 0], 3, 2, 2) == false
        @test Image.ispeak([0 0 1 0 0;0 1 2 1 0;1 2 3 2 1;0 1 2 1 0;0 0 1 0 0], 3, 3, 2) == true

        @test Image.ispeak([0 0 1 0 0;0 1 2 1 0;1 2 3 2 1;0 1 2 1 0;0 0 1 0 0], 2, 2, 3) == false
        @test Image.ispeak([0 0 1 0 0;0 1 2 1 0;1 2 3 2 1;0 1 2 1 0;0 0 1 0 0], 3, 2, 3) == false
        @test Image.ispeak([0 0 1 0 0;0 1 2 1 0;1 2 3 2 1;0 1 2 1 0;0 0 1 0 0], 3, 3, 3) == true

        @test Image.ispeak([0 0 1 0 0;0 1 2 1 0;1 2 3 2 1;0 1 2 1 0;0 0 1 0 0], 2, 2, [0 0 1 0 0;0 1 2 1 0;1 2 3 2 1;0 1 2 1 0;0 0 1 0 0]) == false
        @test Image.ispeak([0 0 1 0 0;0 1 2 1 0;1 2 3 2 1;0 1 2 1 0;0 0 1 0 0], 3, 2, [0 0 1 0 0;0 1 2 1 0;1 2 3 2 1;0 1 2 1 0;0 0 1 0 0]) == false
        @test Image.ispeak([0 0 1 0 0;0 1 2 1 0;1 2 3 2 1;0 1 2 1 0;0 0 1 0 0], 3, 3, [0 0 1 0 0;0 1 2 1 0;1 2 3 2 1;0 1 2 1 0;0 0 1 0 0]) == false

        @test Image.ispeak([0 0 1 0 0;0 1 2 1 0;1 2 3 2 1;0 1 2 1 0;0 0 1 0 0], 2, 2, UInt16.([0 0 2 0 0;0 2 3 2 0;2 3 4 3 2;0 2 3 2 0;0 0 2 0 0])) == false
        @test Image.ispeak([0 0 1 0 0;0 1 2 1 0;1 2 3 2 1;0 1 2 1 0;0 0 1 0 0], 3, 2, UInt16.([0 0 2 0 0;0 2 3 2 0;2 3 4 3 2;0 2 3 2 0;0 0 2 0 0])) == false
        @test Image.ispeak([0 0 1 0 0;0 1 2 1 0;1 2 3 2 1;0 1 2 1 0;0 0 1 0 0], 3, 3, UInt16.([0 0 2 0 0;0 2 3 2 0;2 3 4 3 2;0 2 3 2 0;0 0 2 0 0])) == false

        @test Image.ispeak([0 0 1 0 0;0 1 2 1 0;1 2 3 2 1;0 1 2 1 0;0 0 1 0 0], 2, 2, Float64.([0 0 1 0 0;0 1 2 1 0;1 2 3 2 1;0 1 2 1 0;0 0 1 0 0])) == false
        @test Image.ispeak([0 0 1 0 0;0 1 2 1 0;1 2 3 2 1;0 1 2 1 0;0 0 1 0 0], 3, 2, Float64.([0 0 1 0 0;0 1 2 1 0;1 2 3 2 1;0 1 2 1 0;0 0 1 0 0])) == false
        @test Image.ispeak([0 0 1 0 0;0 1 2 1 0;1 2 3 2 1;0 1 2 1 0;0 0 1 0 0], 3, 3, Float64.([0 0 1 0 0;0 1 2 1 0;1 2 3 2 1;0 1 2 1 0;0 0 1 0 0])) == false
    end


    @testset "Ionimaging.Image.eventcount!" begin
        got = zeros(Int, shape)
        count = Image.eventcount!(got, testimage1, 1)
        @test count == 1
        @test got == [ 0 0 0 0 0 0 0 0 0
                       0 0 0 0 0 0 0 0 0
                       0 0 1 0 0 0 0 0 0
                       0 0 0 0 0 0 0 0 0
                       0 0 0 0 0 0 0 0 0
                       0 0 0 0 0 0 0 0 0
                       0 0 0 0 0 0 0 0 0
                       0 0 0 0 0 0 0 0 0
                       0 0 0 0 0 0 0 0 0 ]

        # High but acceptable threshold
        got = zeros(Int, shape)
        count = Image.eventcount!(got, testimage1, 3)
        @test count == 1
        @test got == [ 0 0 0 0 0 0 0 0 0
                       0 0 0 0 0 0 0 0 0
                       0 0 1 0 0 0 0 0 0
                       0 0 0 0 0 0 0 0 0
                       0 0 0 0 0 0 0 0 0
                       0 0 0 0 0 0 0 0 0
                       0 0 0 0 0 0 0 0 0
                       0 0 0 0 0 0 0 0 0
                       0 0 0 0 0 0 0 0 0 ]

        # Too high threshold
        got = zeros(Int, shape)
        count = Image.eventcount!(got, testimage1, 4)
        @test count == 0
        @test got == zeros(shape)

        # background is `nothing`
        got = zeros(Int, shape)
        count = Image.eventcount!(got, testimage1, 1, nothing)
        @test count == 1
        @test got == [ 0 0 0 0 0 0 0 0 0
                       0 0 0 0 0 0 0 0 0
                       0 0 1 0 0 0 0 0 0
                       0 0 0 0 0 0 0 0 0
                       0 0 0 0 0 0 0 0 0
                       0 0 0 0 0 0 0 0 0
                       0 0 0 0 0 0 0 0 0
                       0 0 0 0 0 0 0 0 0
                       0 0 0 0 0 0 0 0 0 ]

        # background is a scalar
        got = zeros(Int, shape)
        count = Image.eventcount!(got, testimage1, 1, 3)
        @test count == 0
        @test got == zeros(shape)

        # background is an array
        got = zeros(Int, shape)
        count = Image.eventcount!(got, testimage1, 1, ones(Int, shape).*3)
        @test count == 0
        @test got == zeros(shape)

        # background is an UInt16 array
        got = zeros(Int, shape)
        count = Image.eventcount!(got, testimage1, 1, ones(UInt16, shape).*3)
        @test count == 0
        @test got == zeros(shape)

        # mask
        got = zeros(Int, shape)
        count = Image.eventcount!(got, testimage1, 1, nothing, [(3, 3)])
        @test count == 0
        @test got == zeros(shape)
    end


    @testset "Ionimaging.Image.extractBLOB" begin
        # T. B. Moeslund, Introduction to Video and Image Processing: Building Real
        # Systems and Applications, Springer London, London, 2012.
        img = [0 0 1 1 1
               0 0 0 1 0
               0 0 0 1 0
               0 1 1 0 0
               0 1 1 0 0
               0 1 1 0 0]
        got = Image.extractBLOB(img, 1)
        expect = [[(4, 2), (5, 2), (6, 2), (4, 3), (5, 3), (6, 3)],
                  [(1, 3), (1, 4), (2, 4), (3, 4), (1, 5)]]
        @test length(got) == length(expect) == 2
        @test sort(got[1]) == sort(expect[1])
        @test sort(got[2]) == sort(expect[2])
    end


    @testset "Ionimaging.Image.countcentroid!" begin
        got = Image.countcentroid!([], testimage1, 1)
        @test got == [(3.0, 3.0)]

        # Push new spot into `spots`
        got = Image.countcentroid!([(1.0, 1.0)], testimage1,1)
        @test got == [(1.0, 1.0), (3.0, 3.0)]

        # Do *NOT* count too small BLOB
        got = Image.countcentroid!([], testimage1, 3)
        @test got == []

        # Too high threshold
        got = Image.countcentroid!([], testimage1, 4)
        @test got == []

        # Background is nothing
        got = Image.countcentroid!([], testimage1, 1, nothing)
        @test got == [(3.0, 3.0)]

        # Background is a scalar
        got = Image.countcentroid!([], testimage1, 1, 3)
        @test got == []

        # Background is an array
        got = Image.countcentroid!([], testimage1, 1, ones(Int, shape).*3)
        @test got == []

        # Background is an UInt16 array
        got = Image.countcentroid!([], testimage1, 1, ones(UInt16, shape).*3)
        @test got == []

        # Mask
        given = [ 0 0 9 0 0 0 0 0 0
                  0 1 2 1 0 0 0 0 0
                  0 2 3 2 0 0 0 0 0
                  0 1 2 1 0 0 0 0 0
                  0 0 0 0 0 0 0 0 0
                  0 0 0 0 0 0 0 0 0
                  0 0 0 0 0 0 0 0 0
                  0 0 0 0 0 0 0 0 0
                  0 0 0 0 0 0 0 0 0 ]
        got = Image.countcentroid!([], given, 1, nothing, [(1, 3)])
        @test got == [(3.0, 3.0)]
    end


    @testset "Ionimaging.Types.magnify" begin
        # convert a coordinate in 3×3 to that in 9×9
        @test Ionimaging.Types.magnify((1, 1), 3) === (2, 2)
        @test Ionimaging.Types.magnify((2, 1), 3) === (5, 2)
        @test Ionimaging.Types.magnify((3, 1), 3) === (8, 2)
        @test Ionimaging.Types.magnify((1, 2), 3) === (2, 5)
        @test Ionimaging.Types.magnify((2, 2), 3) === (5, 5)
        @test Ionimaging.Types.magnify((3, 2), 3) === (8, 5)
        @test Ionimaging.Types.magnify((1, 3), 3) === (2, 8)
        @test Ionimaging.Types.magnify((2, 3), 3) === (5, 8)
        @test Ionimaging.Types.magnify((3, 3), 3) === (8, 8)
        @test Ionimaging.Types.magnify((0.83, 0.83), 3) === (1, 1)
        @test Ionimaging.Types.magnify((0.84, 0.84), 3) === (2, 2)
        @test Ionimaging.Types.magnify((1.16, 1.16), 3) === (2, 2)
        @test Ionimaging.Types.magnify((1.17, 1.17), 3) === (3, 3)

        # convert a coordinate in 3×3 to that in 6×6
        @test Ionimaging.Types.magnify((1, 1), 2) === (2, 2)
        @test Ionimaging.Types.magnify((2, 1), 2) === (4, 2)
        @test Ionimaging.Types.magnify((3, 1), 2) === (6, 2)
        @test Ionimaging.Types.magnify((1, 2), 2) === (2, 4)
        @test Ionimaging.Types.magnify((2, 2), 2) === (4, 4)
        @test Ionimaging.Types.magnify((3, 2), 2) === (6, 4)
        @test Ionimaging.Types.magnify((1, 3), 2) === (2, 6)
        @test Ionimaging.Types.magnify((2, 3), 2) === (4, 6)
        @test Ionimaging.Types.magnify((3, 3), 2) === (6, 6)
        @test Ionimaging.Types.magnify((0.9, 0.9), 2) === (1, 1)
        @test Ionimaging.Types.magnify((1.4, 1.4), 2) === (2, 2)
        @test Ionimaging.Types.magnify((1.5, 1.5), 2) === (3, 3)
    end


    @testset "take" begin
        Pco.set_testimage((w, h) -> testimage1, shape)
        @test take().arr == testimage1

        queue = [testimage1, testimage2, testimage3, testimage4]
        Pco.set_testimage((w, h) -> popfirst!(queue), shape)
        @test take(4).arr == [ 0 0 0 0 0 0 0 0 0
                               0 1 2 1 0 1 2 1 0
                               0 2 3 2 0 2 3 2 0
                               0 1 2 1 0 1 2 1 0
                               0 0 0 0 0 0 0 0 0
                               0 1 2 1 0 1 2 1 0
                               0 2 3 2 0 2 3 2 0
                               0 1 2 1 0 1 2 1 0
                               0 0 0 0 0 0 0 0 0 ]

        # background is nothing
        queue = [testimage1, testimage2, testimage3, testimage4]
        Pco.set_testimage((w, h) -> popfirst!(queue), shape)
        @test take(4, bg=nothing).arr == [ 0 0 0 0 0 0 0 0 0
                                           0 1 2 1 0 1 2 1 0
                                           0 2 3 2 0 2 3 2 0
                                           0 1 2 1 0 1 2 1 0
                                           0 0 0 0 0 0 0 0 0
                                           0 1 2 1 0 1 2 1 0
                                           0 2 3 2 0 2 3 2 0
                                           0 1 2 1 0 1 2 1 0
                                           0 0 0 0 0 0 0 0 0 ]

        # background is a scalar
        queue = [testimage1, testimage2, testimage3, testimage4]
        Pco.set_testimage((w, h) -> popfirst!(queue), shape)
        @test take(4, bg=1).arr == zeros(shape)

        # background is an array
        queue = [testimage1, testimage2, testimage3, testimage4]
        Pco.set_testimage((w, h) -> popfirst!(queue), shape)
        @test take(4, bg=ones(Int, shape)).arr == zeros(shape)

        # background is an UInt array
        queue = [testimage1, testimage2, testimage3, testimage4]
        Pco.set_testimage((w, h) -> popfirst!(queue), shape)
        @test take(4, bg=ones(UInt16, shape)).arr == zeros(shape)

        # background is an Float64 array
        queue = [testimage1, testimage2, testimage3, testimage4]
        Pco.set_testimage((w, h) -> popfirst!(queue), shape)
        @test take(4, bg=ones(Float64, shape).*0.1).arr ≈ [ 0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0
                                                            0.0  0.6  1.6  0.6  0.0  0.6  1.6  0.6  0.0
                                                            0.0  1.6  2.6  1.6  0.0  1.6  2.6  1.6  0.0
                                                            0.0  0.6  1.6  0.6  0.0  0.6  1.6  0.6  0.0
                                                            0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0
                                                            0.0  0.6  1.6  0.6  0.0  0.6  1.6  0.6  0.0
                                                            0.0  1.6  2.6  1.6  0.0  1.6  2.6  1.6  0.0
                                                            0.0  0.6  1.6  0.6  0.0  0.6  1.6  0.6  0.0
                                                            0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0 ]
    end


    @testset "imaging/test" begin
        queue = [testimage1]
        Pco.set_testimage((w, h) -> popfirst!(queue), shape)
        @test test(1, 1).arr == [ 0 0 0 0 0 0 0 0 0
                                  0 0 0 0 0 0 0 0 0
                                  0 0 1 0 0 0 0 0 0
                                  0 0 0 0 0 0 0 0 0
                                  0 0 0 0 0 0 0 0 0
                                  0 0 0 0 0 0 0 0 0
                                  0 0 0 0 0 0 0 0 0
                                  0 0 0 0 0 0 0 0 0
                                  0 0 0 0 0 0 0 0 0 ]

        queue = [testimage2]
        Pco.set_testimage((w, h) -> popfirst!(queue), shape)
        @test test(1, 1).arr == [ 0 0 0 0 0 0 0 0 0
                                  0 0 0 0 0 0 0 0 0
                                  0 0 0 0 0 0 0 0 0
                                  0 0 0 0 0 0 0 0 0
                                  0 0 0 0 0 0 0 0 0
                                  0 0 0 0 0 0 0 0 0
                                  0 0 1 0 0 0 0 0 0
                                  0 0 0 0 0 0 0 0 0
                                  0 0 0 0 0 0 0 0 0 ]

        queue = [testimage3]
        Pco.set_testimage((w, h) -> popfirst!(queue), shape)
        @test test(1, 1).arr == [ 0 0 0 0 0 0 0 0 0
                                  0 0 0 0 0 0 0 0 0
                                  0 0 0 0 0 0 1 0 0
                                  0 0 0 0 0 0 0 0 0
                                  0 0 0 0 0 0 0 0 0
                                  0 0 0 0 0 0 0 0 0
                                  0 0 0 0 0 0 0 0 0
                                  0 0 0 0 0 0 0 0 0
                                  0 0 0 0 0 0 0 0 0 ]

        queue = [testimage4]
        Pco.set_testimage((w, h) -> popfirst!(queue), shape)
        @test test(1, 1).arr == [ 0 0 0 0 0 0 0 0 0
                                  0 0 0 0 0 0 0 0 0
                                  0 0 0 0 0 0 0 0 0
                                  0 0 0 0 0 0 0 0 0
                                  0 0 0 0 0 0 0 0 0
                                  0 0 0 0 0 0 0 0 0
                                  0 0 0 0 0 0 1 0 0
                                  0 0 0 0 0 0 0 0 0
                                  0 0 0 0 0 0 0 0 0 ]

        # Background is `nothing`
        queue = [testimage1]
        Pco.set_testimage((w, h) -> popfirst!(queue), shape)
        @test test(1, 1, bg=nothing).arr == [ 0 0 0 0 0 0 0 0 0
                                              0 0 0 0 0 0 0 0 0
                                              0 0 1 0 0 0 0 0 0
                                              0 0 0 0 0 0 0 0 0
                                              0 0 0 0 0 0 0 0 0
                                              0 0 0 0 0 0 0 0 0
                                              0 0 0 0 0 0 0 0 0
                                              0 0 0 0 0 0 0 0 0
                                              0 0 0 0 0 0 0 0 0 ]

        # High but acceptable background
        queue = [testimage1]
        Pco.set_testimage((w, h) -> popfirst!(queue), size(queue[1]))
        @test test(1, 1, bg=2).arr == [ 0 0 0 0 0 0 0 0 0
                                        0 0 0 0 0 0 0 0 0
                                        0 0 1 0 0 0 0 0 0
                                        0 0 0 0 0 0 0 0 0
                                        0 0 0 0 0 0 0 0 0
                                        0 0 0 0 0 0 0 0 0
                                        0 0 0 0 0 0 0 0 0
                                        0 0 0 0 0 0 0 0 0
                                        0 0 0 0 0 0 0 0 0 ]

        # Background is a scalar
        queue = [testimage1]
        Pco.set_testimage((w, h) -> popfirst!(queue), shape)
        @test test(1, 1, bg=3).arr == zeros(shape)

        # Background is an array
        queue = [testimage1]
        Pco.set_testimage((w, h) -> popfirst!(queue), shape)
        @test test(1, 1, bg=ones(Int, shape).*3).arr == zeros(shape)

        # Background is an UInt16 array
        queue = [testimage1]
        Pco.set_testimage((w, h) -> popfirst!(queue), shape)
        @test test(1, 1, bg=ones(UInt16, shape).*3).arr == zeros(shape)

        # Background is an Float64 array
        queue = [testimage1]
        Pco.set_testimage((w, h) -> popfirst!(queue), shape)
        @test test(1, 1, bg=ones(Float64, shape).*3).arr == zeros(shape)

        # Pinpoint background
        bg = [ 0 0 0 0 0 0 0 0 0
               0 3 3 3 0 0 0 0 0
               0 3 3 3 0 0 0 0 0
               0 3 3 3 0 0 0 0 0
               0 0 0 0 0 0 0 0 0
               0 0 0 0 0 0 0 0 0
               0 0 0 0 0 0 0 0 0
               0 0 0 0 0 0 0 0 0
               0 0 0 0 0 0 0 0 0 ]
        queue = [testimage1, testimage2]
        Pco.set_testimage((w, h) -> popfirst!(queue), shape)
        @test test(2, 1, bg=bg).arr == [ 0 0 0 0 0 0 0 0 0
                                         0 0 0 0 0 0 0 0 0
                                         0 0 0 0 0 0 0 0 0
                                         0 0 0 0 0 0 0 0 0
                                         0 0 0 0 0 0 0 0 0
                                         0 0 0 0 0 0 0 0 0
                                         0 0 1 0 0 0 0 0 0
                                         0 0 0 0 0 0 0 0 0
                                         0 0 0 0 0 0 0 0 0 ]

        # Mask
        ### hotpixel injection
        hotpixel = [ 0 0 9 0 0 0 0 0 0
                     0 1 2 1 0 0 0 0 0
                     0 2 3 2 0 0 0 0 0
                     0 1 2 1 0 0 0 0 0
                     0 0 0 0 0 0 0 0 0
                     0 1 2 1 0 0 0 0 0
                     0 2 3 2 0 0 0 0 0
                     0 1 2 1 0 0 0 0 0
                     0 0 0 0 0 0 0 0 0 ]
        queue = [hotpixel]
        Pco.set_testimage((w, h) -> popfirst!(queue), shape)
        @test test(1, 1, mask=[(1, 3)]).arr == [ 0 0 0 0 0 0 0 0 0
                                                 0 0 0 0 0 0 0 0 0
                                                 0 0 1 0 0 0 0 0 0
                                                 0 0 0 0 0 0 0 0 0
                                                 0 0 0 0 0 0 0 0 0
                                                 0 0 0 0 0 0 0 0 0
                                                 0 0 1 0 0 0 0 0 0
                                                 0 0 0 0 0 0 0 0 0
                                                 0 0 0 0 0 0 0 0 0 ]

        queue = [ testimage1, testimage2, testimage3, testimage4,
                  testimage1, testimage2, testimage3, testimage4,
                  testimage1, testimage2, testimage3, testimage4,
                  testimage1, testimage2, testimage3, testimage4,
                  testimage1 ]
        Pco.set_testimage((w, h) -> popfirst!(queue), size(queue[1]))
        @test test(17, 1).arr == [ 0 0 0 0 0 0 0 0 0
                                   0 0 0 0 0 0 0 0 0
                                   0 0 5 0 0 0 4 0 0
                                   0 0 0 0 0 0 0 0 0
                                   0 0 0 0 0 0 0 0 0
                                   0 0 0 0 0 0 0 0 0
                                   0 0 4 0 0 0 4 0 0
                                   0 0 0 0 0 0 0 0 0
                                   0 0 0 0 0 0 0 0 0 ]
    end


    @testset "imagingsp/testsp" begin
        queue = [testimage1]
        Pco.set_testimage((w, h) -> popfirst!(queue), shape)
        @test testsp(1, 1).arr == [ 0 0 0 0 0 0 0 0 0
                                    0 0 0 0 0 0 0 0 0
                                    0 0 1 0 0 0 0 0 0
                                    0 0 0 0 0 0 0 0 0
                                    0 0 0 0 0 0 0 0 0
                                    0 0 0 0 0 0 0 0 0
                                    0 0 0 0 0 0 0 0 0
                                    0 0 0 0 0 0 0 0 0
                                    0 0 0 0 0 0 0 0 0 ]

        queue = [testimage2]
        Pco.set_testimage((w, h) -> popfirst!(queue), shape)
        @test testsp(1, 1).arr == [ 0 0 0 0 0 0 0 0 0
                                    0 0 0 0 0 0 0 0 0
                                    0 0 0 0 0 0 0 0 0
                                    0 0 0 0 0 0 0 0 0
                                    0 0 0 0 0 0 0 0 0
                                    0 0 0 0 0 0 0 0 0
                                    0 0 1 0 0 0 0 0 0
                                    0 0 0 0 0 0 0 0 0
                                    0 0 0 0 0 0 0 0 0 ]

        queue = [testimage3]
        Pco.set_testimage((w, h) -> popfirst!(queue), shape)
        @test testsp(1, 1).arr == [ 0 0 0 0 0 0 0 0 0
                                    0 0 0 0 0 0 0 0 0
                                    0 0 0 0 0 0 1 0 0
                                    0 0 0 0 0 0 0 0 0
                                    0 0 0 0 0 0 0 0 0
                                    0 0 0 0 0 0 0 0 0
                                    0 0 0 0 0 0 0 0 0
                                    0 0 0 0 0 0 0 0 0
                                    0 0 0 0 0 0 0 0 0 ]

        queue = [testimage4]
        Pco.set_testimage((w, h) -> popfirst!(queue), shape)
        @test testsp(1, 1).arr == [ 0 0 0 0 0 0 0 0 0
                                    0 0 0 0 0 0 0 0 0
                                    0 0 0 0 0 0 0 0 0
                                    0 0 0 0 0 0 0 0 0
                                    0 0 0 0 0 0 0 0 0
                                    0 0 0 0 0 0 0 0 0
                                    0 0 0 0 0 0 1 0 0
                                    0 0 0 0 0 0 0 0 0
                                    0 0 0 0 0 0 0 0 0 ]

        # Background is `nothing`
        queue = [testimage1]
        Pco.set_testimage((w, h) -> popfirst!(queue), shape)
        @test testsp(1, 1, bg=nothing).arr == [ 0 0 0 0 0 0 0 0 0
                                                0 0 0 0 0 0 0 0 0
                                                0 0 1 0 0 0 0 0 0
                                                0 0 0 0 0 0 0 0 0
                                                0 0 0 0 0 0 0 0 0
                                                0 0 0 0 0 0 0 0 0
                                                0 0 0 0 0 0 0 0 0
                                                0 0 0 0 0 0 0 0 0
                                                0 0 0 0 0 0 0 0 0 ]

        # Background is a scalar
        queue = [testimage1]
        Pco.set_testimage((w, h) -> popfirst!(queue), shape)
        @test testsp(1, 1, bg=3).arr == zeros(shape)

        # High but acceptable background
        queue = [testimage1]
        Pco.set_testimage((w, h) -> popfirst!(queue), shape)
        @test testsp(1, 1, bg=1).arr == [ 0 0 0 0 0 0 0 0 0
                                          0 0 0 0 0 0 0 0 0
                                          0 0 1 0 0 0 0 0 0
                                          0 0 0 0 0 0 0 0 0
                                          0 0 0 0 0 0 0 0 0
                                          0 0 0 0 0 0 0 0 0
                                          0 0 0 0 0 0 0 0 0
                                          0 0 0 0 0 0 0 0 0
                                          0 0 0 0 0 0 0 0 0 ]

        # Background is an array
        queue = [testimage1]
        Pco.set_testimage((w, h) -> popfirst!(queue), shape)
        @test testsp(1, 1, bg=ones(Int, shape).*3).arr == zeros(shape)

        # Background is an UInt16 array
        queue = [testimage1]
        Pco.set_testimage((w, h) -> popfirst!(queue), shape)
        @test testsp(1, 1, bg=ones(UInt16, shape).*3).arr == zeros(shape)

        # Background is an UInt16 array
        queue = [testimage1]
        Pco.set_testimage((w, h) -> popfirst!(queue), shape)
        @test testsp(1, 1, bg=ones(Float64, shape).*3).arr == zeros(shape)

        # Pinpoint background
        bg = [ 0 0 0 0 0 0 0 0 0
               0 3 3 3 0 0 0 0 0
               0 3 3 3 0 0 0 0 0
               0 3 3 3 0 0 0 0 0
               0 0 0 0 0 0 0 0 0
               0 0 0 0 0 0 0 0 0
               0 0 0 0 0 0 0 0 0
               0 0 0 0 0 0 0 0 0
               0 0 0 0 0 0 0 0 0 ]
        queue = [testimage1, testimage2]
        Pco.set_testimage((w, h) -> popfirst!(queue), shape)
        @test testsp(2, 1, bg=bg).arr == [ 0 0 0 0 0 0 0 0 0
                                           0 0 0 0 0 0 0 0 0
                                           0 0 0 0 0 0 0 0 0
                                           0 0 0 0 0 0 0 0 0
                                           0 0 0 0 0 0 0 0 0
                                           0 0 0 0 0 0 0 0 0
                                           0 0 1 0 0 0 0 0 0
                                           0 0 0 0 0 0 0 0 0
                                           0 0 0 0 0 0 0 0 0 ]

        # Mask
        ### hotpixel injection
        hotpixel = [ 0 0 9 0 0 0 0 0 0
                     0 1 2 1 0 0 0 0 0
                     0 2 3 2 0 0 0 0 0
                     0 1 2 1 0 0 0 0 0
                     0 0 0 0 0 0 0 0 0
                     0 1 2 1 0 0 0 0 0
                     0 2 3 2 0 0 0 0 0
                     0 1 2 1 0 0 0 0 0
                     0 0 0 0 0 0 0 0 0 ]
        queue = [hotpixel]
        Pco.set_testimage((w, h) -> popfirst!(queue), shape)
        ### hotpixel should not affect
        @test testsp(1, 1, mask=[(1, 3)]).arr == [ 0 0 0 0 0 0 0 0 0
                                                   0 0 0 0 0 0 0 0 0
                                                   0 0 1 0 0 0 0 0 0
                                                   0 0 0 0 0 0 0 0 0
                                                   0 0 0 0 0 0 0 0 0
                                                   0 0 0 0 0 0 0 0 0
                                                   0 0 1 0 0 0 0 0 0
                                                   0 0 0 0 0 0 0 0 0
                                                   0 0 0 0 0 0 0 0 0 ]

        queue = [
            testimage1, testimage2, testimage3, testimage4,
            testimage1, testimage2, testimage3, testimage4,
            testimage1, testimage2, testimage3, testimage4,
            testimage1, testimage2, testimage3, testimage4,
            testimage1
        ]
        Pco.set_testimage((w, h) -> popfirst!(queue), shape)
        @test testsp(17, 1).arr == [ 0 0 0 0 0 0 0 0 0
                                     0 0 0 0 0 0 0 0 0
                                     0 0 5 0 0 0 4 0 0
                                     0 0 0 0 0 0 0 0 0
                                     0 0 0 0 0 0 0 0 0
                                     0 0 0 0 0 0 0 0 0
                                     0 0 4 0 0 0 4 0 0
                                     0 0 0 0 0 0 0 0 0
                                     0 0 0 0 0 0 0 0 0 ]
    end
end
