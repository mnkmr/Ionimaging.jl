using Ionimaging
using Test

function test_basic_operations(a, b)
    @test a .+ b == [5 5;5 5]
    @test a .+ zeros(Int, 2, 2) == a.arr
    @test a .+ 0 == a.arr
    @test a + 0 == a.arr

    @test a .- b == [0 0;1 3]
    @test a .- zeros(Int, 2, 2) == a.arr
    @test a .- 0 == a.arr
    @test a - 0 == a.arr

    @test a .* b == [4 6;6 4]
    @test a .* ones(Int, 2, 2) == a.arr
    @test a .* 1 == a.arr
    @test a * 1 == a.arr

    @test a ./ b == [1/4 2/3;3/2 4/1]
    @test a ./ ones(Int, 2, 2) == a.arr
    @test a ./ 1 == a.arr
    @test a / 1 == a.arr

    @test a .÷ b == [0 0;1 4]
    @test a .÷ ones(Int, 2, 2) == a.arr
    @test a .÷ 1 == a.arr
    @test a ÷ 1 == a.arr

    @test size(a) == size(a.arr)
    @test a[1, 1] == a.arr[1, 1]
    @test (a[1, 1] = 1) == 1
    @test length(a) == length(a.arr)
    @test size(similar(a).arr) == size(similar(a.arr))
end


@testset "Types" begin
    @testset "Ionimage type" begin
        @test Ionimage <: AbstractIonimage

        a = Ionimage([1 2;3 4])
        b = Ionimage([4 3;2 1])
        test_basic_operations(a, b)
    end


    @testset "Rawimage type" begin
        @test Rawimage <: AbstractIonimage

        a = Rawimage([1 2;3 4])
        b = Rawimage([4 3;2 1])
        test_basic_operations(a, b)
    end


    @testset "Ionspots type" begin
        @test Ionspots <: AbstractIonimage

        spots_a = [(1.0, 1.0), (1.0, 2.0), (1.0, 2.0), (2.0, 1.0), (2.0, 1.0), (2.0, 1.0), (2.0, 2.0), (2.0, 2.0), (2.0, 2.0), (2.0, 2.0)]
        spots_b = [(1.0, 1.0), (1.0, 1.0), (1.0, 1.0), (1.0, 1.0), (1.0, 2.0), (1.0, 2.0), (1.0, 2.0), (2.0, 1.0), (2.0, 1.0), (2.0, 2.0)]
        a = Ionspots(spots_a, 2, 2)
        b = Ionspots(spots_b, 2, 2)
        test_basic_operations(a, b)
    end
end
