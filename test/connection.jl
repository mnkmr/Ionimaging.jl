import Base: Filesystem
using Ionimaging
using Test

@testset "Connection" begin
    tempdir = Filesystem.tempdir()
    Ionimaging.DATADIR[] = tempdir
    @info "Images will be saved into '$(tempdir)'"

    # Suppress monitor window in CI
    if haskey(ENV, "CI")
        Ionimaging.CONFIG["measurement"]["monitor"] = false
    end

    img = take()
    @test isa(img, Rawimage)

    window(img)

    histogram(img)

    @test isa(take(16), Rawimage)

    # live()

    hp = hotpixel(12, 100)

    bg = background(32)

    try
        @test isa(imaging(32, 260, mask=hp), Ionimage)

        @test isa(test(32, 260, mask=hp), Ionimage)

        @test isa(imagingsp(32, 260, mask=hp), Ionspots)

        @test isa(testsp(32, 260, mask=hp), Ionspots)

        @test readimg(joinpath(tempdir, "1.hdf5")) == b[1]

        @test readimg(joinpath(tempdir, "2.hdf5")) == b[2]

        # Check default triggers
        @test take().meta["triggermode"] == 0
        @test take(2).meta["triggermode"] == 0
        @test test(1, 260).meta["triggermode"] == 2
        @test testsp(1, 260).meta["triggermode"] == 2
    finally
        Filesystem.rm(joinpath(tempdir, "1.hdf5"), force=true)
        Filesystem.rm(joinpath(tempdir, "2.hdf5"), force=true)
    end
end
