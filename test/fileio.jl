import Base: Filesystem
import DelimitedFiles: readdlm
using Ionimaging
import Printf
using Test

const testimage1 = [ 0 0 0 0 0 0 0 0 0
                     0 1 2 1 0 0 0 0 0
                     0 2 3 2 0 0 0 0 0
                     0 1 2 1 0 0 0 0 0
                     0 0 0 0 0 0 0 0 0
                     0 0 0 0 0 0 0 0 0
                     0 0 0 0 0 0 0 0 0
                     0 0 0 0 0 0 0 0 0
                     0 0 0 0 0 0 0 0 0 ]

const shape = size(testimage1)

@testset "File I/O" begin
    # Use the virtual camera
    Ionimaging.CONFIG["interface"]["code"] = Ionimaging.Pco.API.PCO_INTERFACE_VIRTUAL

    # Suppress monitor window in CI
    if haskey(ENV, "CI")
        Ionimaging.CONFIG["measurement"]["monitor"] = false
    end

    tempdir = Filesystem.tempdir()
    Ionimaging.DATADIR[] = tempdir
    @info "Images will be saved into '$(tempdir)'"
    tempfile = joinpath(tempdir, "test1.hdf5")

    try
        Ionimaging.Pco.set_testimage((w, h) -> testimage1, shape)
        img = take()
        saveimg(tempfile, img)
        @test isfile(tempfile)
        fromfile = readimg(tempfile)
        @test img.arr == fromfile.arr
        for k in keys(img.meta)
            @test img.meta[k] == fromfile.meta[k]
        end
        Filesystem.rm(tempfile, force=false)

        Ionimaging.Pco.set_testimage((w, h) -> testimage1, shape)
        img = test(1, 201)
        saveimg(tempfile, img)
        @test isfile(tempfile)
        fromfile = readimg(tempfile)
        @test img.arr == fromfile.arr
        for k in keys(img.meta)
            @test img.meta[k] == fromfile.meta[k]
        end
        Filesystem.rm(tempfile, force=false)

        Ionimaging.Pco.set_testimage((w, h) -> testimage1, shape)
        img = testsp(1, 201)
        saveimg(tempfile, img)
        @test isfile(tempfile)
        fromfile = readimg(tempfile)
        @test img.arr == fromfile.arr
        for k in keys(img.meta)
            @test img.meta[k] == fromfile.meta[k]
        end
        Filesystem.rm(tempfile, force=false)
    finally
        Filesystem.rm(tempfile, force=true)
    end
end
