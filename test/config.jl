import Base: Filesystem
using Ionimaging
using Test

@testset "Config file" begin
    # default
    @test Ionimaging.CONFIG["measurement"]["imaging"]["trig"] == 2
    @test Ionimaging.CONFIG["measurement"]["imagingsp"]["trig"] == 2

    confpath = joinpath(@__DIR__, "configs", "testconf_ALL.yaml")
    Ionimaging.loadconf(confpath)
    @test Ionimaging.CONFIG["interface"]["code"] == 0x0001
    @test Ionimaging.CONFIG["measurement"]["take"]["trig"] == 0
    @test Ionimaging.CONFIG["measurement"]["take"]["delay"] == 0.0
    @test Ionimaging.CONFIG["measurement"]["take"]["exposure"] == 0.01
    @test Ionimaging.CONFIG["measurement"]["take"]["pixelrate"] == 12000000
    @test Ionimaging.CONFIG["measurement"]["take"]["hbin"] == 1
    @test Ionimaging.CONFIG["measurement"]["take"]["vbin"] == 1
    @test Ionimaging.CONFIG["measurement"]["take"]["exsensor"] == 0
    @test Ionimaging.CONFIG["measurement"]["take"]["hotpixcorr"] == 0
    @test Ionimaging.CONFIG["measurement"]["take"]["convfactor"] == 100
    @test Ionimaging.CONFIG["measurement"]["take"]["roi"] == [1, 1, 601, 801]
    @test Ionimaging.CONFIG["measurement"]["live"]["trig"] == 0
    @test Ionimaging.CONFIG["measurement"]["live"]["delay"] == 0.0
    @test Ionimaging.CONFIG["measurement"]["live"]["exposure"] == 0.01
    @test Ionimaging.CONFIG["measurement"]["live"]["pixelrate"] == 12000000
    @test Ionimaging.CONFIG["measurement"]["live"]["hbin"] == 1
    @test Ionimaging.CONFIG["measurement"]["live"]["vbin"] == 1
    @test Ionimaging.CONFIG["measurement"]["live"]["exsensor"] == 0
    @test Ionimaging.CONFIG["measurement"]["live"]["hotpixcorr"] == 0
    @test Ionimaging.CONFIG["measurement"]["live"]["convfactor"] == 100
    @test Ionimaging.CONFIG["measurement"]["live"]["roi"] == [1, 1, 601, 801]
    @test Ionimaging.CONFIG["measurement"]["imaging"]["trig"] == 0
    @test Ionimaging.CONFIG["measurement"]["imaging"]["delay"] == 0.0
    @test Ionimaging.CONFIG["measurement"]["imaging"]["exposure"] == 0.01
    @test Ionimaging.CONFIG["measurement"]["imaging"]["pixelrate"] == 12000000
    @test Ionimaging.CONFIG["measurement"]["imaging"]["hbin"] == 1
    @test Ionimaging.CONFIG["measurement"]["imaging"]["vbin"] == 1
    @test Ionimaging.CONFIG["measurement"]["imaging"]["exsensor"] == 0
    @test Ionimaging.CONFIG["measurement"]["imaging"]["hotpixcorr"] == 0
    @test Ionimaging.CONFIG["measurement"]["imaging"]["convfactor"] == 100
    @test Ionimaging.CONFIG["measurement"]["imaging"]["roi"] == [1, 1, 601, 801]
    @test Ionimaging.CONFIG["measurement"]["imaging"]["interval"] == 1
    @test Ionimaging.CONFIG["measurement"]["imagingsp"]["trig"] == 0
    @test Ionimaging.CONFIG["measurement"]["imagingsp"]["delay"] == 0.0
    @test Ionimaging.CONFIG["measurement"]["imagingsp"]["exposure"] == 0.01
    @test Ionimaging.CONFIG["measurement"]["imagingsp"]["pixelrate"] == 12000000
    @test Ionimaging.CONFIG["measurement"]["imagingsp"]["hbin"] == 1
    @test Ionimaging.CONFIG["measurement"]["imagingsp"]["vbin"] == 1
    @test Ionimaging.CONFIG["measurement"]["imagingsp"]["exsensor"] == 0
    @test Ionimaging.CONFIG["measurement"]["imagingsp"]["hotpixcorr"] == 0
    @test Ionimaging.CONFIG["measurement"]["imagingsp"]["convfactor"] == 100
    @test Ionimaging.CONFIG["measurement"]["imagingsp"]["roi"] == [1, 1, 601, 801]
    @test Ionimaging.CONFIG["measurement"]["imagingsp"]["interval"] == 1
    @test Ionimaging.CONFIG["fileio"]["data_directory"] == Ionimaging.Config.expandpath("testdir")

    confpath = joinpath(@__DIR__, "configs", "testconf_interface.code.yaml")
    Ionimaging.loadconf(confpath)
    @test Ionimaging.CONFIG["interface"]["code"] == 0x0002

    confpath = joinpath(@__DIR__, "configs", "testconf_measurement.take.trig.yaml")
    Ionimaging.loadconf(confpath)
    @test Ionimaging.CONFIG["measurement"]["take"]["trig"] == 2

    confpath = joinpath(@__DIR__, "configs", "testconf_measurement.live.trig.yaml")
    Ionimaging.loadconf(confpath)
    @test Ionimaging.CONFIG["measurement"]["live"]["trig"] == 2

    confpath = joinpath(@__DIR__, "configs", "testconf_measurement.imaging.trig.yaml")
    Ionimaging.loadconf(confpath)
    @test Ionimaging.CONFIG["measurement"]["imaging"]["trig"] == 2

    confpath = joinpath(@__DIR__, "configs", "testconf_measurement.imagingsp.trig.yaml")
    Ionimaging.loadconf(confpath)
    @test Ionimaging.CONFIG["measurement"]["imagingsp"]["trig"] == 2

    confpath = joinpath(@__DIR__, "configs", "testconf_measurement.xxx.interval.yaml")
    Ionimaging.loadconf(confpath)
    @test Ionimaging.CONFIG["measurement"]["imaging"]["interval"] == 10
    @test Ionimaging.CONFIG["measurement"]["imagingsp"]["interval"] == 10

    confpath = joinpath(@__DIR__, "configs", "testconf_fileio.data_directory.yaml")
    Ionimaging.loadconf(confpath)
    @test Ionimaging.CONFIG["fileio"]["data_directory"] == Ionimaging.Config.expandpath("testdir2")

    confpath = joinpath(@__DIR__, "configs", "testconf_EMPTY.yaml")
    Ionimaging.loadconf(confpath)
    @test Ionimaging.CONFIG["interface"]["code"] == 0x0002
    @test Ionimaging.CONFIG["measurement"]["take"]["trig"] == 2
    @test Ionimaging.CONFIG["measurement"]["take"]["delay"] == 0.0
    @test Ionimaging.CONFIG["measurement"]["take"]["exposure"] == 0.01
    @test Ionimaging.CONFIG["measurement"]["take"]["pixelrate"] == 12000000
    @test Ionimaging.CONFIG["measurement"]["take"]["hbin"] == 1
    @test Ionimaging.CONFIG["measurement"]["take"]["vbin"] == 1
    @test Ionimaging.CONFIG["measurement"]["take"]["exsensor"] == 0
    @test Ionimaging.CONFIG["measurement"]["take"]["hotpixcorr"] == 0
    @test Ionimaging.CONFIG["measurement"]["take"]["convfactor"] == 100
    @test Ionimaging.CONFIG["measurement"]["take"]["roi"] == [1, 1, 601, 801]
    @test Ionimaging.CONFIG["measurement"]["take"]["progress"] == false
    @test Ionimaging.CONFIG["measurement"]["live"]["trig"] == 2
    @test Ionimaging.CONFIG["measurement"]["live"]["delay"] == 0.0
    @test Ionimaging.CONFIG["measurement"]["live"]["exposure"] == 0.01
    @test Ionimaging.CONFIG["measurement"]["live"]["pixelrate"] == 12000000
    @test Ionimaging.CONFIG["measurement"]["live"]["hbin"] == 1
    @test Ionimaging.CONFIG["measurement"]["live"]["vbin"] == 1
    @test Ionimaging.CONFIG["measurement"]["live"]["exsensor"] == 0
    @test Ionimaging.CONFIG["measurement"]["live"]["hotpixcorr"] == 0
    @test Ionimaging.CONFIG["measurement"]["live"]["convfactor"] == 100
    @test Ionimaging.CONFIG["measurement"]["live"]["roi"] == [1, 1, 601, 801]
    @test Ionimaging.CONFIG["measurement"]["imaging"]["trig"] == 2
    @test Ionimaging.CONFIG["measurement"]["imaging"]["delay"] == 0.0
    @test Ionimaging.CONFIG["measurement"]["imaging"]["exposure"] == 0.01
    @test Ionimaging.CONFIG["measurement"]["imaging"]["pixelrate"] == 12000000
    @test Ionimaging.CONFIG["measurement"]["imaging"]["hbin"] == 1
    @test Ionimaging.CONFIG["measurement"]["imaging"]["vbin"] == 1
    @test Ionimaging.CONFIG["measurement"]["imaging"]["exsensor"] == 0
    @test Ionimaging.CONFIG["measurement"]["imaging"]["hotpixcorr"] == 0
    @test Ionimaging.CONFIG["measurement"]["imaging"]["convfactor"] == 100
    @test Ionimaging.CONFIG["measurement"]["imaging"]["roi"] == [1, 1, 601, 801]
    @test Ionimaging.CONFIG["measurement"]["imaging"]["interval"] == 10
    @test Ionimaging.CONFIG["measurement"]["imaging"]["monitor"] == false
    @test Ionimaging.CONFIG["measurement"]["imaging"]["progress"] == false
    @test Ionimaging.CONFIG["measurement"]["imagingsp"]["trig"] == 2
    @test Ionimaging.CONFIG["measurement"]["imagingsp"]["delay"] == 0.0
    @test Ionimaging.CONFIG["measurement"]["imagingsp"]["exposure"] == 0.01
    @test Ionimaging.CONFIG["measurement"]["imagingsp"]["pixelrate"] == 12000000
    @test Ionimaging.CONFIG["measurement"]["imagingsp"]["hbin"] == 1
    @test Ionimaging.CONFIG["measurement"]["imagingsp"]["vbin"] == 1
    @test Ionimaging.CONFIG["measurement"]["imagingsp"]["exsensor"] == 0
    @test Ionimaging.CONFIG["measurement"]["imagingsp"]["hotpixcorr"] == 0
    @test Ionimaging.CONFIG["measurement"]["imagingsp"]["convfactor"] == 100
    @test Ionimaging.CONFIG["measurement"]["imagingsp"]["roi"] == [1, 1, 601, 801]
    @test Ionimaging.CONFIG["measurement"]["imagingsp"]["interval"] == 10
    @test Ionimaging.CONFIG["measurement"]["imagingsp"]["monitor"] == false
    @test Ionimaging.CONFIG["measurement"]["imagingsp"]["progress"] == false
    @test Ionimaging.CONFIG["fileio"]["data_directory"] == Ionimaging.Config.expandpath("testdir2")

    if Sys.iswindows()
        ENV["IONIMAGING_TEST_ENV"] = "env_test"
        @test Ionimaging.Config.expandpath("%IONIMAGING_TEST_ENV%") == "env_test"
        @test Ionimaging.Config.expandpath("%IONIMAGING_TEST_ENV%\\%IONIMAGING_TEST_ENV%") == "env_test\\env_test"
    end

    tempdir = Filesystem.tempdir()
    confpath = joinpath(tempdir, "config.yaml")
    try
        @test begin Ionimaging.makeconf_example(confpath); isfile(confpath) end
    finally
        Filesystem.rm(confpath, force=true)
    end
end
