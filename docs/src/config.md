# Configuration

## Overview

`Ionimaging` package searches a configuration file placed at a certain
directory and loads it if it is found. The config file allows to optimize the
behavior of the program for a particular usage.

It is expected that the config file is named as `config.yaml`. As can be seen
from the file extension, it is an text file in [YAML](https://yaml.org/) format.

The `config.yaml` should be placed in the `Ionimaging` folder under the
directory pointed by the environmental variable `IONIMAGING_CONFIGDIR`.
If `IONIMAGING_CONFIGDIR` is not defined, `APPDATA` is used instead. That is,
`C:\Users\{Username}\AppData\Roaming\Ionimaging` would be the most probable
place.


## Getting started

`Ionimaging` package includes an example config file. A lot of
examples are already written in this file with commented out. So, it would be
easier to start with copying it and uncommenting the desired lines.

The `makeconf_example` function helps user to copy the file to an appropriate
place. This function returns the file path to the copied configuration file.

Then, open the file with a text editor and uncomment (remove the leading single
'#' character) to enable settings. Available values for some options depend on
the camera, please check the specification sheet of your camera.

```
julia> using Ionimaging

julia> conffile = Ionimaging.makeconf_example()
"C:\Users\foo\AppData\Roaming\Ionimaging\config.yaml"

julia> edit(conffile)
```

You may be asked what program you use to open it. Choose one of your favorite text editor, for instance `notepad.exe`.


## Overview

```
interface:
    code: int

measurement:
    take:
        trig: int
        delay: float
        exposure: float
        pixelrate: int
        vbin: int
        hbin: int
        exsensor: int
        hotpixcorr: int
        convfactor: int
        roi: [int, int, int, int]
        progress: bool
    live:
        trig: int
        delay: float
        exposure: float
        pixelrate: int
        vbin: int
        hbin: int
        exsensor: int
        hotpixcorr: int
        convfactor: int
        roi: [int, int, int, int]
    imaging:
        trig: int
        delay: float
        exposure: float
        pixelrate: int
        vbin: int
        hbin: int
        exsensor: int
        hotpixcorr: int
        convfactor: int
        roi: [int, int, int, int]
        interval: int
        monitor: bool
        progress: bool
    imagingsp:
        trig: int
        delay: float
        exposure: float
        pixelrate: int
        vbin: int
        hbin: int
        exsensor: int
        hotpixcorr: int
        convfactor: int
        roi: [int, int, int, int]
        interval: int
        monitor: bool
        progress: bool

fileio:
    data_directory: str
```

 - [interface](#interface-1)
   - [code](#code-1)
 - [measurement](#measurement-1)
   - [take](#take-1)
     - [trig](#trig-1)
     - [delay](#delay-1)
     - [exposure](#exposure-1)
     - [pixelrate](#pixelrate-1)
     - [vbin](#vbin-1)
     - [hbin](#hbin-1)
     - [exsensor](#exsensor-1)
     - [hotpixcorr](#hotpixcorr-1)
     - [convfactor](#convfactor-1)
     - [roi](#roi-1)
     - [progress](#progress-1)
   - [live](#live-1)
     - [trig](#trig-1)
     - [delay](#delay-1)
     - [exposure](#exposure-1)
     - [pixelrate](#pixelrate-1)
     - [vbin](#vbin-1)
     - [hbin](#hbin-1)
     - [exsensor](#exsensor-1)
     - [hotpixcorr](#hotpixcorr-1)
     - [convfactor](#convfactor-1)
     - [roi](#roi-1)
   - [imaging](#imaging-1)
     - [trig](#trig-1)
     - [delay](#delay-1)
     - [exposure](#exposure-1)
     - [pixelrate](#pixelrate-1)
     - [vbin](#vbin-1)
     - [hbin](#hbin-1)
     - [exsensor](#exsensor-1)
     - [hotpixcorr](#hotpixcorr-1)
     - [convfactor](#convfactor-1)
     - [roi](#roi-1)
     - [interval](#interval-1)
     - [monitor](#monitor-1)
     - [progress](#progress-1)
   - [imagingsp](#imagingsp-1)
     - common with `imaging`
 - [fileio](#fileio-1)
   - [data_directory](#data_directory-1)


## interface

### code

*int*

Setting an appropriate number to the `code` key allow to find your camera
quicker when starting-up. Uncomment one of the following lines.

|   code   | description |
|:---------|:------------|
| `0x0001` | Firewire interface |
| `0x0002` | Cameralink Matrox Solios / Helios |
| `0x0003` | Cameralink Silicon Software Me3 |
| `0x0004` | Cameralink National Instruments |
| `0x0005` | Gigabit Ethernet |
| `0x0006` | USB 2.0 |
| `0x0007` | Cameralink Silicon Software Me4 |
| `0x0008` | USB 3.0 and USB 3.1 Gen1 |
| `0x0009` | WLan (Only control path, not data path) |
| `0x0011` | Cameralink HS |
| `0x1000` | Virtual Camera for test |
| `0xffff` | Scan all the available interfaces to find camera |


## measurement

### trig

*int*

Set the default trigger mode. Without any special reason, use `0` (internal) or
`2` (external).

| trig | description |
|:-----|:------------|
| `0`  | Internal (auto) trigger |
| `1`  | Software trigger |
| `2`  | External trigger |
| `3`  | External exposure control |
| `4`  | External synchronized |

### delay

*float*

Set the default delay time from a trigger to shutter open in **second**. The
available delay time depends on a camera. Check the specification sheet.

### exposure

*float*

Set the default exposure time in **second**. The available exposure time
depends on a camera. Check the specification sheet.

### pixelrate

*int*

Set the default pixel scan rate of the camera in **Hz**. It is related to the
buffer readout time and thus the resulted frame-per-second (fps). The available
pixel rate depends on a camera. Check the specification sheet.

### vbin

*int*

Set the default vertical binning size in **pixel**. The available binning sizes
depend on a camera. Check the specification sheet.

### hbin

*int*

Set the default horizontal binning size in **pixel**. The available binning
sizes depend on a camera. Check the specification sheet.

### exsensor

*int*

Set the default sensor format. Some cameras have the second camera format, this
option enables one of those format. Check the specification sheet.

| exsensor | description |
|:---------|:------------|
|   `0`    | Standard sensor format |
|   `1`    | Extended sensor format |

### hotpixcorr

*int*

Set ON/OFF of hotpixel correction mode. Some cameras have the hotpixel
correction mode but others may not. Check the specification sheet.

| hotpixcorr | description |
|:-----------|:------------|
|    `0`     | OFF |
|    `1`     | ON |

### convfactor

*int*

Set the default current conversion factor. The available values depend on a
camera. Check the specification sheet.

### roi

*[int, int, int, int]*

Set the default region-of-interest (ROI) in pixels.
The ROI is defined by four integers in an array, like `[y0, x0, y1, x1]`.
The coordinate (y0, x0) points the upper-left corner and (y1, x1) points the
bottom-right corner. This option is not supported for all cameras.

```
(1, 1)
   -------------------------------------------
   |                                         |
   |  (y0, x0)                               |
   |      ---------------------              |
   |      |                   |              |
   |      |                   |              |
   |      |                   |              |
   |      |                   |              |
   |      |                   |              |
   |      |                   |              |
   |      ---------------------              |
   |                       (y1, x1)          |
   |                                         |
   |                                         |
   -------------------------------------------
                                       (height, width)
```

### interval

*int*

Control the interval of monitor window update. In general, image plotting is
costly, and thus too frequent monitor update drops the rate of measurement.
This option is useful when working with high repetition rate. The default is 1
and the monitor is updated every
`{interval} × {number of internal image buffers}` triggers.
`{number of internal memory}` would be 16 for almost all cameras. Hence, in
this case, the monitor window is updated every `1 × 16 = 16` triggers.

### monitor

*bool*

If `monitor` keyword is false, the monitor window would not show up.

### progress

*bool*

If `progress` keyword is false, the progress message would be suppressed.


## fileio

### data_directory

*str*

Set the path to the directory to save experimental data. Ionimaging package
makes subfolders named by year and date under this directory. It is recommended
to use the full path to the directory.

 - Example
   - `data_directory: 'C:\Users\username\Documents'`
   - `data_directory: '%USERPROFILE%\Documents'`
