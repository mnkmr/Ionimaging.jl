# Reference

## Index

```@index
```


## Types

```@docs
AbstractIonimage
```

```@docs
Rawimage
```

```@docs
Ionimage
```

```@docs
Ionspots
```

```@docs
develop
```


## Measurement

```@docs
take
```

```@docs
background
```

```@docs
imaging
```

```@docs
test
```

```@docs
imagingsp
```

```@docs
testsp
```

```@docs
live
```


## Visualization and analysis

```@docs
window
```

```@docs
histogram
```

```@docs
preview_imaging
```

```@docs
preview_imagingsp
```

```@docs
hotpixel
```


### File I/O

```@docs
saveimg!
```

```@docs
saveimg
```

```@docs
readimg
```


## Supplementary note

```@docs
Ionimaging.run
```

