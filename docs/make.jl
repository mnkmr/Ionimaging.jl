using Documenter
using Ionimaging

makedocs(
    sitename = "Ionimaging.jl",
    modules = [Ionimaging],
    repo = "https://gitlab.com/mnkmr/Ionimaging.jl/blob/{commit}{path}#{line}",
    pages = ["index.md", "reference.md"]
)

# Documenter can also automatically deploy documentation to gh-pages.
# See "Hosting Documentation" and deploydocs() in the Documenter manual
# for more information.
#=deploydocs(
    repo = "<repository url>"
)=#
