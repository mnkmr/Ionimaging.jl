Ionimaging.jl
=============


[![pipeline status](https://gitlab.com/mnkmr/Ionimaging.jl/badges/master/pipeline.svg)](https://gitlab.com/mnkmr/Ionimaging.jl/commits/master)
[![documentation (placeholder)](https://img.shields.io/badge/docs-latest-blue.svg)](https://mnkmr.gitlab.io/Ionimaging.jl/)

A [Julia](https://julialang.org/) package for photo-fragment/photo-electron imaging experiment with a CCD camera supplied by [pco](https://www.pco-imaging.com/de/).


## License

[MIT License](LICENSE)


## Requirement

 - Julia 1.0 or later version
 - pco camera interface driver
 - pco SDK runtime - software development runtime

The driver and SDK runtime can be downloaded from [pco support page](https://www.pco.de/support/). Choose the appropriate driver for your camera.


## Install

Open a julia REPL and press `]` key to start [Pkg REPL mode](https://docs.julialang.org/en/v1/stdlib/Pkg/#Getting-Started-1). Then execute the following command.

```
(v1.0) pkg> add https://gitlab.com/mnkmr/Ionimaging.jl
```


## Usage

First of all, load this package.

```
julia> using Ionimaging
```

When this package is loaded, it searches a camera from all possible interfaces for later use. However, note that even if any camera is not found at this moment, the following functions would still work, by searching a camera when it is called.


### Take a raw image

Run `take` function to obtain a raw image.

```
julia> img = take();
```

The function takes an image immediately with the internal trigger by default. If you need to synchronize with the external trigger, assign 2 to `trig` keyword.

```
julia> img = take(trig=2);
```

The returned `img` is an `Rawimage` type instance which have two fields; `arr` and `meta`. The `arr` is an `Array` of the image and `meta` is a dictionary of metadata.

```
julia> img.arr
600×800 Array{Int64,2}:
 0  0  0  0  0  0  0  0  0  0  0  0  0  …  0  0  0  0  0  0  0  0  0  0  0  0
 0  0  0  0  0  0  0  0  0  0  0  0  0     0  0  0  0  0  0  0  0  0  0  0  0
 0  0  0  0  0  0  0  0  0  0  0  0  0     0  0  0  0  0  0  0  0  0  0  0  0
 0  0  0  0  0  0  0  0  0  0  0  0  0     0  0  0  0  0  0  0  0  0  0  0  0
 0  0  0  0  0  0  0  0  0  0  0  0  0     0  0  0  0  0  0  0  0  0  0  0  0
 0  0  0  0  0  0  0  0  0  0  0  0  0  …  0  0  0  0  0  0  0  0  0  0  0  0
 0  0  0  0  0  0  0  0  0  0  0  0  0     0  0  0  0  0  0  0  0  0  0  0  0
 0  0  0  0  0  0  0  0  0  0  0  0  0     0  0  0  0  0  0  0  0  0  0  0  0
 0  0  0  0  0  0  0  0  0  0  0  0  0     0  0  0  0  0  0  0  0  0  0  0  0
 0  0  0  0  0  0  0  0  0  0  0  0  0     0  0  0  0  0  0  0  0  0  0  0  0
 ⋮              ⋮              ⋮        ⋱        ⋮              ⋮
 0  0  0  0  0  0  0  0  0  0  0  0  0  …  0  0  0  0  0  0  0  0  0  0  0  0
 0  0  0  0  0  0  0  0  0  0  0  0  0     0  0  0  0  0  0  0  0  0  0  0  0
 0  0  0  0  0  0  0  0  0  0  0  0  0     0  0  0  0  0  0  0  0  0  0  0  0
 0  0  0  0  0  0  0  0  0  0  0  0  0     0  0  0  0  0  0  0  0  0  0  0  0
 0  0  0  0  0  0  0  0  0  0  0  0  0     0  0  0  0  0  0  0  0  0  0  0  0
 0  0  0  0  0  0  0  0  0  0  0  0  0  …  0  0  0  0  0  0  0  0  0  0  0  0
 0  0  0  0  0  0  0  0  0  0  0  0  0     0  0  0  0  0  0  0  0  0  0  0  0
 0  0  0  0  0  0  0  0  0  0  0  0  0     0  0  0  0  0  0  0  0  0  0  0  0
 0  0  0  0  0  0  0  0  0  0  0  0  0     0  0  0  0  0  0  0  0  0  0  0  0
 0  0  0  0  0  0  0  0  0  0  0  0  0     0  0  0  0  0  0  0  0  0  0  0  0

julia> img.meta
Dict{String,Any} with 20 entries:
  "pixelrate"          => 12000000
  "triggermode"        => 0x0000
  "starttime"          => 2019-01-19T20:30:27.435
  "peakcount"          => 0
  "dynamicrange"       => 14
  "interface"          => "USB 2.0"
  "binning"            => [1, 1]
  "sensor"             => "ICX285AL"
  "conversionfactor"   => 100
  "exposure"           => 0.01
  "resolution"         => [600, 800]
  "sensorformat"       => 0x0001
  "camera"             => "pco.pixelfly USB"
  "finishtime"         => 2019-01-19T20:30:27.678
  "delay"              => 0.0
  "accumulation"       => 1
  "type"               => "Rawimage"
  "hotpixelcorrection" => 0x0000
```

The obtained image can be visualized by `window` function.

```
julia> window(img);
```


### Coutinuous monitoring

Run `live` function for continuous monitoring. It uses the camera like a video cam. It will be stopped by pressing `CTRL` and `Break` keys at the same time.

```
julia> live()
```

If you need to synchronize with external trigger, assign 2 to `trig` keyword.

```
julia> live(trig=2)
```


### Imaging experiment

Run `imaging` function to obtain a photo-fragment/photo-electron image. This function uses the ion counting method written in [B. Y. Chang, R. C. Hoetzlein, J. A. Mueller, J. D. Geiser and P. L. Houston, Rev. Sci. Instrum., 1998, 69, 1665–1670.].

```
julia> imaging(100, 260);
```

The first argument is the number of accumulation. The above example shows the case of 100 times accumulation.

The second argument is the threshold; the pixels which intensity is lower than the threshold will not be counted in peak counting. It should be set at a little higher than the maximum of white noise.

The result is saved as a file in `Ionimaging.DATADIR[]` and is also stored in `b` as an instance of `Ionimage` type which is similart to `Rawimage` type.

```
julia> imaging(100, 260);

julia> b
1-element Array{Ionimage,1}

julia> b[1]
Ionimaging.Ionimage:
  date  : 2019-01-19T20:30:27.435
  size  : 600x800

julia> window(b[1]);
```

Or you can get the result directly as a return value.

```
julia> img = imaging(100, 260);
```

If you don't want to save the file, use `test` function. It does same thing with `imaging` only without saving neither in file nor in `b`.

Strongly recommended to check out the following [Histogram](#histogram) and [Hot pixels](#hot-pixels) descriptions.


### Histogram

The function `histogram` may be useful to determine the threshold for `imaging` function. First, stop all the signal source. Then, take a raw image and pass the image to `histogram` function.

```
julia> img = take();
julia> histogram(img);
```

It will show a histogram plot of pixel intensities. The x axis represents the intensity of pixels, while the y axis represents the number of pixels in the each intensity range.

Note that the y-axis is set as log scale by default. Assign `true`/`false` to `log` keyword for control.

```
julia> histogram(img, log=true);   # log scale

julia> histogram(img, log=false);  # normal scale
```

The displayed range can be changed by `xlim` and `ylim` keyword.

```
julia> histogram(img, xlim=[190, 210]);  # display 190 <= x <= 210

julia> histogram(img, ylim=[1, 100]);    # display 1 <= y <= 100
```

Note that it would be better to use `log=false` together when you want to zoom the low counts region; otherwise there is no way to tell the difference between y = 0 and y = 1. Or set the lower limit at 0.1, 0 is not an effective lower limit for log scale.

```
julia> histogram(img, log=false, ylim=[0, 10])

julia> histogram(img, ylim=[0.1, 10])
```


### Hot pixels

Sometimes you may found some pixels are always bright. It may be caused by environmental lighting or may be caused by CCD inside. With the former case, you should be careful to shield the CCD sensor from anbient light or do background correction. If it is the latter case, it is difficult to avoid since the weird signal comes from CCD sensor defect or electric circuits of camera inside. However, in many cases, such a strange pixel stays at a certain position. Therefore, we can mask the pixel to eliminate fake signals from the pixels.

The function `hotpixel` is useful to detect hot pixels. First, cover the camera sensor to block anbient light, then run `hotpixel` function.

```
julia> hp = hotpixel()
```

It measures several images, 500 by default, and detects exceptionally bright pixels from the averaged image. This function judges that the pixels brighter than a threshold are hot pixels. This threshold is derived by the equation

```
threshold = xc + m*s
```

where, `xc` and `s` are the average and standard deviation of all pixel intensities. `m` is the coefficients; it is 12 in default. This value may not be correct always. The function plots the pixel intensities as a histogram and draws the threshold as a red line. Please judge by yourself with the histogram plot. It can be adjusted by the first optional argument.

```
julia> hp = hotpixel(10)
```

The accumulation count can be changed by the second arguments. It is expected that the larger accumulation is more accurate but too many accumulation may end up as the waste of time.

```
julia> hp = hotpixel(12, 1000)
```

The returned value can be passed to `mask` keyword of `imaging` function to ignore those pixels.

```
julia> imaging(100, 260, mask=hp)
```


## Acknowledgement

This program was developed under the supervision of [Prof. King-Chuen Lin](https://www.ch.ntu.edu.tw/english/faculty/?id=kclin&lang=en) in National Taiwan University. An author MN greatly appreciates his kind support.
