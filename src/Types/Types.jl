module Types

export
    AbstractIonimage,
    Rawimage,
    Ionimage,
    Ionspots,
    develop

import Base: show, size, getindex, setindex!, length, similar, axes, strides, stride, +, -, *, /, ÷
import Base.Broadcast: broadcasted, Broadcasted
import Dates
import Printf


"""
    AbstractIonimage{T} <: AbstractMatrix{T}

The abstract type for experimental images.
"""
abstract type AbstractIonimage{T} <: AbstractMatrix{T} end

# AbstractArray interfaces

size(img::AbstractIonimage) = size(img.arr)
getindex(img::AbstractIonimage, I...) = getindex(img.arr, I...)
setindex!(img::AbstractIonimage, v, I...) = setindex!(img.arr, v, I...)
length(img::AbstractIonimage) = length(img.arr)
axes(img::AbstractIonimage) = map(Base.OneTo, size(img.arr))
strides(img::AbstractIonimage) = strides(img.arr)
stride(img::AbstractIonimage, k::Integer) = stride(img.arr, k)


sub_with_bottom(a, b, bottom) = max(a - b, bottom)

+(a::AbstractIonimage, b::Real) = a.arr .+ b
+(a::Real, b::AbstractIonimage) = a .+ b.arr
-(a::AbstractIonimage, b::Real) = sub_with_bottom.(a.arr, b, 0)
-(a::Real, b::AbstractIonimage) = sub_with_bottom.(a, b.arr, 0)
*(a::AbstractIonimage, b::Real) = a.arr .* b
*(a::Real, b::AbstractIonimage) = a .* b.arr
/(a::AbstractIonimage, b::Real) = a.arr ./ b
/(a::Real, b::AbstractIonimage) = a ./ b.arr
÷(a::AbstractIonimage, b::Real) = a.arr .÷ b
÷(a::Real, b::AbstractIonimage) = a .÷ b.arr

broadcasted(f, a::AbstractIonimage, b::AbstractIonimage) = Broadcasted(f, (a.arr, b.arr))
broadcasted(f, a::AbstractIonimage, b::AbstractMatrix{<:Real}) = Broadcasted(f, (a.arr, b))
broadcasted(f, a::AbstractMatrix{<:Real}, b::AbstractIonimage) = Broadcasted(f, (a, b.arr))
broadcasted(f, a::AbstractIonimage, b::Real) = Broadcasted(f, (a.arr, b))
broadcasted(f, a::Real, b::AbstractIonimage) = Broadcasted(f, (a, b.arr))

broadcasted(::typeof(-), a::AbstractIonimage, b::AbstractIonimage) = Broadcasted(sub_with_bottom, (a.arr, b.arr, 0))
broadcasted(::typeof(-), a::AbstractIonimage, b::AbstractMatrix{<:Real}) = Broadcasted(sub_with_bottom, (a.arr, b, 0))
broadcasted(::typeof(-), a::AbstractMatrix{<:Real}, b::AbstractIonimage) = Broadcasted(sub_with_bottom, (a, b.arr, 0))
broadcasted(::typeof(-), a::AbstractIonimage, b::Real) = Broadcasted(sub_with_bottom, (a.arr, b, 0))
broadcasted(::typeof(-), a::Real, b::AbstractIonimage) = Broadcasted(sub_with_bottom, (a, b.arr, 0))




"""
    Rawimage{T<:Real} <: AbstractIonimage{T}

The type for an raw image read out from a CCD camera.

`Rawimage.arr` is the image array and `Rawimage.meta` is the metadata.

Generally, an `Rawimage` type variable behaves like an `Array`.
Basic mathematical operations are also supported. The answer is lowered to
`Array` type.

**NOTE** that the subtraction of `Rawimage` suppresses negative pixels. To
avoid this, use `Rawimage.arr` property directry.

Examples
```julia-repl
julia> raw = take()
Ionimaging.Rawimage:
  date  : 2019-01-19T20:30:27.435
  size  : 600x800

julia> raw[1, 1]
0

julia> size(raw)
(600, 800)

julia> raw .+ raw;

julia> raw .- raw;

julia> raw .* raw;

julia> raw ./ raw;

julia> raw .÷ raw;

julia> minimum(raw .- (raw .+ 1))
0

julia> minimum(raw.arr .- (raw.arr .+ 1))
-1
```
"""
struct Rawimage{T<:Real} <: AbstractIonimage{T}
    arr::Matrix{T}
    meta::Dict{String,Any}

    function Rawimage(arr::AbstractMatrix{T}, meta) where {T<:Real}
        meta["type"] = "Rawimage"
        new{T}(arr, meta)
    end
end

Rawimage(arr) = Rawimage(arr, Dict{String,Any}())


function show(io::IO, ::MIME"text/plain", z::Rawimage)
    print("Ionimaging.Rawimage:")

    if haskey(z.meta, "starttime")
        timestr = Dates.format(z.meta["starttime"], "yyyy/mm/dd HH:MM:SS")
        Printf.@printf("\n  %-6s: %s", "date", timestr)
    end

    height, width = size(z.arr)
    Printf.@printf("\n  %-6s: %dx%d", "size", height, width)
end

# AbstractArray interfaces

similar(img::Rawimage) = Rawimage(similar(img.arr))
similar(img::Rawimage, ::Type{T}) where {T} = Rawimage(similar(img.arr, T))
similar(img::Rawimage, dims::Dims) = Rawimage(similar(img.arr, dims))
similar(img::Rawimage, ::Type{T}, dims::Dims) where {T} = Rawimage(similar(img.arr, T, dims))
similar(img::Rawimage, ::Type{T}, inds) where {T} = similar(img, T, Base.to_shape(inds))




"""
    Ionimage{T<:Real} <: AbstractIonimage{T}

The type for an ion image with event counting.

`Ionimage.arr` is the image array and `Ionimage.meta` is the metadata.

Generally, an `Ionimage` type variable behaves like an `Array`.
Basic mathematical operations are also supported. The answer is lowered to
`Array` type.

**NOTE** that the subtraction of `Ionimage` suppresses negative pixels. To
avoid this, use `Ionimage.arr` property directry.

Examples
```julia-repl
julia> img = imaging(100, 240)
Ionimaging.Ionimage:
  date  : 2019-01-19T20:30:27.435
  size  : 600×800
  count : 1002 (accumulation 100)

julia> img[1, 1]
0

julia> size(img)
(600, 800)

julia> img .+ img;

julia> img .- img;

julia> img .* img;

julia> img ./ img;

julia> img .÷ img;

julia> minimum(img .- (img .+ 1))
0

julia> minimum(img.arr .- (img.arr .+ 1))
-1
```
"""
struct Ionimage{T<:Real} <: AbstractIonimage{T}
    arr::Matrix{T}
    meta::Dict{String,Any}

    function Ionimage(arr::AbstractMatrix{T}, meta) where {T<:Real}
        meta["type"] = "Ionimage"
        new{T}(arr, meta)
    end
end

Ionimage(arr) = Ionimage(arr, Dict{String,Any}())


function show(io::IO, ::MIME"text/plain", z::Ionimage)
    print("Ionimaging.Ionimage:")

    if haskey(z.meta, "starttime")
        timestr = Dates.format(z.meta["starttime"], "yyyy/mm/dd HH:MM:SS")
        Printf.@printf("\n  %-6s: %s", "date", timestr)
    end

    height, width = size(z.arr)
    Printf.@printf("\n  %-6s: %d×%d", "size", height, width)

    Printf.@printf("\n  %-6s: %d", count, sum(z.arr))
    if haskey(z.meta, "accumulation")
        Printf.@printf(" (%d accumulation)", z.meta["accumulation"])
    end
end

# AbstractArray interfaces

similar(img::Ionimage) = Ionimage(similar(img.arr))
similar(img::Ionimage, ::Type{T}) where {T} = Ionimage(similar(img.arr, T))
similar(img::Ionimage, dims::Dims) = Ionimage(similar(img.arr, dims))
similar(img::Ionimage, ::Type{T}, dims::Dims) where {T} = Ionimage(similar(img.arr, T, dims))
similar(img::Ionimage, ::Type{T}, inds) where {T} = similar(img, T, Base.to_shape(inds))




"""
    Ionspots{T<:Real} <: AbstractIonimage{T}

The type for a list of ion spot coordinates with subpixel event counting.

`Ionspots.arr` is the image array with 1x magnification.
`Ionspots.spots` is the array of ion spot coordinates.
`Ionspots.meta` is the metadata.

Generally, an `Ionspots` type variable behaves like an `Array`.
Basic mathematical operations are also supported. The answer is lowered to
`Array` type.

**NOTE** that the subtraction of `Ionspots` suppresses negative pixels. To
avoid this, use `Ionspots.arr` property directry.

An `Ionspots` variable can be converted to an `Ionimage` variable with an
arbitrary magnification factor (typically 1 ~ 5) through [`develop`](@ref).

Examples
```julia-repl
julia> isp = imagingsp(100, 240)
Ionimaging.Ionimage:
  date  : 2019-01-19T20:30:27.435
  size  : 600×800 (1×)
  count : 1002 (accumulation 100)

julia> isp[1, 1]
0

julia> size(isp)
(600, 800)

julia> isp .+ isp;

julia> isp .- isp;

julia> isp .* isp;

julia> isp ./ isp;

julia> isp .÷ isp;

julia> minimum(isp .- (isp .+ 1))
0

julia> minimum(isp.arr .- (isp.arr .+ 1))
-1

julia> develop(isp, 1)
Ionimaging.Ionimage:
  date  : 2019-01-19T20:30:27.435
  size  : 600×800
  count : 1002 (accumulation 100)

julia> develop(isp, 2)
Ionimaging.Ionimage:
  date  : 2019-01-19T20:30:27.435
  size  : 1200×1600
  count : 1002 (accumulation 100)
```
"""
struct Ionspots{T<:Real} <: AbstractIonimage{T}
    spots::Vector{NTuple{2,Float64}}
    arr::Matrix{T}
    meta::Dict{String,Any}

    function Ionspots(spots, arr::AbstractMatrix{T}, meta) where {T<:Real}
        meta["type"] = "Ionspots"
        meta["magnification"] = 1
        new{T}(spots, arr, meta)
    end
end

function Ionspots(spots, height::Integer, width::Integer, meta::Dict{String,Any})
    arr = _develop(spots, 1, height, width)
    Ionspots(spots, arr, meta)
end

Ionspots(spots, height::Integer, width::Integer) = Ionspots(spots, height, width, Dict{String,Any}())


"""
    develop(isp::Ionspots, m::Integer)

Convert an `Ionspots` to an `Ionimage` with an arbitrary magnification factor
(typically 1 ~ 5).

# Example
```julia-repl
julia> develop(isp, 1)
Ionimaging.Ionimage:
  date  : 2019-01-19T20:30:27.435
  size  : 600×800
  count : 1002 (accumulation 100)

julia> develop(isp, 2)
Ionimaging.Ionimage:
  date  : 2019-01-19T20:30:27.435
  size  : 1200×1600
  count : 1002 (accumulation 100)
```
"""
function develop(isp::Ionspots, m::Integer)
    height, width = size(isp.arr)
    arr = _develop(isp.spots, m, height, width)
    meta = copy(isp.meta)
    meta["magnification"] = m
    Ionimage(arr, meta)
end

function _develop!(arr::Matrix, spots, m::Integer)
    if isempty(spots)
        return arr
    end

    height, width = size(arr)
    for sp in spots
        y, x = magnify(sp, m)
        if 1 <= y <= height && 1 <= x <= width
            arr[y, x] += 1
        end
    end
    arr
end

function _develop(spots, m, height, width)
    arr = zeros(Int, height*m, width*m)
    _develop!(arr, spots, m)
end


# convert a coordinate in height×width to that in (height*m)×(width*m)
function magnify(coord, m)
    map(n -> magnify(n, m), coord)
end

function magnify(n::Real, m)
    round(Int, (n - 0.5)*m + 0.5, RoundNearestTiesUp)
end


function show(io::IO, ::MIME"text/plain", z::Ionspots)
    print("Ionimaging.Ionspots:")

    if haskey(z.meta, "starttime")
        timestr = Dates.format(z.meta["starttime"], "yyyy/mm/dd HH:MM:SS")
        Printf.@printf("\n  %-6s: %s", "date", timestr)
    end

    height, width = size(z.arr)
    Printf.@printf("\n  %-6s: %d×%d (1×)", "size", height, width)

    Printf.@printf("\n  %-6s: %d", count, sum(z.arr))
    if haskey(z.meta, "accumulation")
        Printf.@printf(" (%d accumulation)", z.meta["accumulation"])
    end
end

# AbstractArray interfaces

function similar(isp::Ionspots, ::Type{T}, dims::Dims) where {T}
    arr = similar(isp.arr, T, dims)
    spots = Vector{NTuple{2,Float64}}()
    meta = Dict{String,Any}()
    Ionspots(spots, arr, meta)
end

similar(isp::Ionspots) = similar(isp, eltype(isp.arr), size(isp.arr))
similar(isp::Ionspots, ::Type{T}) where {T} = similar(isp, T, size(isp.arr))
similar(isp::Ionspots, dims::Dims) = similar(isp, eltype(isp.arr), dims)
similar(isp::Ionspots, ::Type{T}, inds) where {T} = similar(isp, T, Base.to_shape(inds))

end
