# FIXME: Using two plotting library (PyPlot, GR) is somewhat weird.
#        However, it is difficult to do real-time plotting with PyPlot while GR
#        cannot have multiple windows.

# TODO: Rethink on the structure of cam.buffer
#       Currently, there is no guarantee that the memory space for image buffer
#       is successively allocated (maybe not)

module Ionimaging

export
    AbstractIonimage,
    Ionimage,
    Rawimage,
    Ionspots,
    develop,
    b,
    imaging,
    test,
    imagingsp,
    testsp,
    live,
    take,
    background,
    window,
    histogram,
    hotpixel,
    preview_imaging,
    preview_imagingsp,
    saveimg!,
    saveimg,
    readimg

include("Config/Config.jl")              # Config module
include("Image/Image.jl")                # Image module
include("Pco/Pco.jl")                    # Pco module
include("Types/Types.jl")                # Types module

import .Config: CONFIG, DATADIR, makeconf_example, loadconf
import .Image
import .Pco
import .Types: AbstractIonimage, Rawimage, Ionimage, Ionspots, develop
import Base: Filesystem
import Dates
import Printf
import ProgressMeter
import PyPlot
import Statistics


include("init.jl")  # __init__()


"""
    run(f::Function; kwargs...)

Open camera and execute the measurement function (`f`).

In principle, users don't need to use this function directly, however, all the
measurement functions uses this function. Thus, those functions share the
`kwargs` each other. The available options are listed below. The list of
options are mainly used for controlling the camera.

 * trig

The keyword for trigger mode. Without any special reason, use `0` or `2` .
(default: `0`)

| trig | description               |
|:-----|:--------------------------|
| `0`  | auto trigger              |
| `1`  | software trigger          |
| `2`  | external trigger          |
| `3`  | external exposure control |
| `4`  | external synchronized     |

 * delay

The delay time from trigger to exposure in second. (default: `0.0`)

 * exposure

The exposure time in second. (default: `0.01`)

 * pixelrate

The pixel scan rate of the camera, it is related to the buffer readout time and
thus the resulted frame-per-second (fps). (default: depending on the camera)

 * vbin

The vertical binning. (default: `1`)

 * hbin

The Horizontal binning. (default: `1`)

 * bitalign

This option controls the bit alignment of data from the camera. It shouldn't be
changed without any special reason. (default: `1`)

 * exsensor

This option controls the sensor format. Some pco camras have "extended" sensor
format, this option is to enable it. (default: `0`)

| exsensor | description     |
|:---------|:----------------|
| `0`      | standard format |
| `1`      | extended format |

 * hotpixcorr

Turning ON/OFF of hot pixel correction. (default: `0`)

| hotpixcorr | description |
|:-----------|:------------|
| `0`        | OFF         |
| `1`        | ON          |

 * convfactor

This option controls the current conversion factor.
(default: depending on the camera)

 * roi

This option changes the current region-of-interest (ROI) setting in pixels.
The ROI is defined by four integers in an array, like [y0, x0, y1, x1].
The coordinate (y0, x0) points the upper-left corner and (y1, x1) points the
bottom-right corner. Not all cameras support this option.

        (1, 1)
           -------------------------------------------
           |                                         |
           |  (y0, x0)                               |
           |      ---------------------              |
           |      |                   |              |
           |      |                   |              |
           |      |                   |              |
           |      |                   |              |
           |      |                   |              |
           |      |                   |              |
           |      ---------------------              |
           |                       (y1, x1)          |
           |                                         |
           |                                         |
           -------------------------------------------
                                               (height, width)
"""
function run(f::Function; kwargs...)
    interface = Config.CONFIG["interface"]["code"]
    Pco.opencamera(interface; kwargs...) do cam
        ok = Pco.healthcheck(cam)
        if !ok
            msg = "The camera is not healthy. Solve the hardware problem."
            throw(Pco.CameraError(msg))
        end

        meta = metadata(cam)
        meta["starttime"] = Dates.now()
        ret = f(cam, meta)
        meta["finishtime"] = Dates.now()
        # NOTE: `ret` is a tuple which probably includes `meta`
        ret
    end
end


# Return a Dictionary of basic camera information
function metadata(cam)
    delay, exposure = Pco.delay_exposure(cam)
    width, height = size(cam)
    hbin, vbin = Pco.binning(cam)
    Dict("camera"             => cam.spec[:camera],
         "sensor"             => cam.spec[:sensor],
         "interface"          => cam.spec[:interface],
         "resolution"         => [height, width],
         "dynamicrange"       => cam.spec[:depth],
         "pixelrate"          => Pco.pixelrate(cam),
         "conversionfactor"   => Pco.conversionfactor(cam),
         "sensorformat"       => Pco.sensorformat(cam),
         "triggermode"        => Pco.triggermode(cam),
         "delay"              => delay,
         "exposure"           => exposure,
         "binning"            => [vbin, hbin],
         "hotpixelcorrection" => Pco.hotpixelcorrection(cam))
end


# The basic structure of various measurement functions for accumulating images
function accumulateimage(count_accum::Integer, initialize::Function,
                         update::Function, finalize::Function, args; kwargs...)
    run(; trig=0, kwargs...) do cam, meta
        args = initialize(args, cam, meta, count_accum)
        count_done = 0
        try
            n = min(cam.bufmax÷2, count_accum)
            readbuf = Int[]
            while count_done < count_accum
                takingbuf = Pco.atake(cam, n)
                args = update(args, cam, readbuf, count_accum, count_done)
                Pco.waitfor(cam, takingbuf)
                Pco.cancel!(cam)
                count_done += length(takingbuf)
                n = min(cam.bufmax - length(takingbuf), count_accum - count_done)
                readbuf = takingbuf
            end
            args = update(args, cam, readbuf, count_accum, count_done)
        catch e
            if isa(e, InterruptException)
                print("\n")
                @warn "The measurement is cancelled."
            else
                rethrow()
            end
        end
        finalize(args, meta, count_accum, count_done)
    end
end

"""
    take(; bg=nothing, kwargs...)

Take a raw image.

The `bg` keyword is employed for background correction.

The `kwargs` will be directly passed to [`Ionimaging.run`](@ref).
See the help of it.

The internal trigger is used by default.

# Example
```julia-repl
julia> img = take();

julia> bg = background();

julia> img = take(bg=bg);  # background correction
```
"""
function take(; kwargs...)
    opt, camera_parameters = Config.take(; kwargs...)
    img, meta = run(; trig=0, camera_parameters...) do cam, meta
        T = Image.image_eltype(opt.bg)
        img = zeros(T, size(cam))  # row-major
        bgᵀ = Image.transpose_background(opt.bg)
        Image.check_sizes(img, bgᵀ)
        Pco.take!(cam, img)
        Image.background_correction!(img, bgᵀ)
        meta["accumulation"] = 1
        img, meta
    end
    img = transpose(img)  # transpose the image to column-major order
    img[img .< 0] .= 0    # suppress negative pixels
    Rawimage(img, meta)
end

"""
    take(count_accum::Integer; bg=nothing, progress=false, kwargs...)

Accumulate a raw image.

The `bg` keyword is employed for background correction.

If `progress` keyword is false, the progress message would be suppressed.

The `kwargs` will be directly passed to [`Ionimaging.run`](@ref).
See the help of it.

The internal trigger is used by default.

# Example
```julia-repl
julia> img = take(200);

julia> bg = background();

julia> img = take(200, bg=bg);  # background correction
```
"""
function take(count_accum::Integer; kwargs...)
    opt, camera_parameters = Config.take(; kwargs...)

    function initialize(args, cam, meta, count_accum)
        T = Image.image_eltype(opt.bg)
        img = zeros(T, size(cam))  # row-major
        bgᵀ = Image.transpose_background(opt.bg)
        Image.check_sizes(img, bgᵀ)
        p = ProgressMeter.Progress(count_accum, 1)
        img, bgᵀ, p
    end

    function update(args, cam, bufnrs, count_accum, count_done)
        img, bgᵀ, p = args
        Image.sum!(img, cam, bufnrs, bgᵀ)
        opt.progress && ProgressMeter.update!(p, count_done)
        args
    end

    function finalize(args, meta, count_accum, count_done)
        img = args[1]
        img[img .< 0] .= 0    # suppress negative pixels
        img = transpose(img)  # transpose the image to column-major order
        meta["accumulation"] = count_done
        img, meta
    end

    img, meta = accumulateimage(count_accum, initialize, update, finalize,
                                (); camera_parameters...)
    Rawimage(img, meta)
end


"""
    background(count_accum=200; kwargs...)

This is an equivalent of `take(count_accum).÷count_accum`. It would be useful
to get a background signal image.
"""
function background(count_accum=200; kwargs...)
    bg = take(count_accum; trig=0, kwargs...)
    bg.arr .÷= count_accum
    bg
end


function showprogress(count_accum, count_done, count_ion, count_laser)
    if count_done == 0
        showprogress()
        return
    end

    msg = Printf.@sprintf("%7d / %-7d  %10d (%9.2f)", count_done, count_accum,
                          count_ion, count_ion/count_laser)
    println(msg)
end

function showprogress()
    msg = Printf.@sprintf("\n%7s / %-7s  %10s (%9s)", "Done", "Total",
                          "Ion Count", "per Laser")
    println(msg)
    Printf.@printf("%s\n", "="^(length(msg) + 2))
end


function showsummary(img, count_accum, count_done)
    count_ion = sum(img)
    msg = Printf.@sprintf("%7d / %-7d  %10d (%9.2f)", count_done, count_accum,
                   count_ion, count_ion/count_done)
    Printf.@printf("%s\n", "-"^(length(msg) + 2))
    println(msg)
    Printf.@printf("%s\n", "="^(length(msg) + 2))
    println("")
end


# The array of images taken
const b = AbstractIonimage[]

function _save(ionimage::AbstractIonimage)
    # store image into b
    push!(b, ionimage)
    @info "Stored in b[$(length(b))]"

    # save image to file
    datadir = Config.DATADIR[]
    if datadir == ""
        return
    end

    filename = "$(length(b)).hdf5"
    if !isdir(datadir)
        Filesystem.mkpath(datadir)
    end
    path = joinpath(datadir, filename)
    if isfile(path)
        @error "'$(filename)' already exists, failed to save."
        return
    end

    saveimg!(path, ionimage)
    @info "Saved to '$(path)'"
end


"""
    imaging(count_accum::Integer, threshold; save=true, bg=nothing, mask=[],
            interval=2, monitor=false, progress=true, kwargs...)

Accumulate ion images with event counting. `count_accum` is the number of
accumulation and `threshold` is the threshold for event counting.

If `save` keyword is true, the result will be stored to the global variable `b`
and saved as a file in `Ionimage.DATADIR[]`.

The `bg` keyword is used for background correction.

The `mask` keyword is employed to mask hot pixels.

The `interval` keyword is employed to control the interval of monitor
window update. In general, image plotting is costly, and thus too frequent
monitor update drops the rate of measurement. This option is useful when
working with high repetition rate. The default is 2 and the monitor is updated
every `{interval} × {number of internal image buffers} / 2` triggers.
`{number of internal memory}` would be 16 for almost all cameras. Hence, in
this case, the monitor window is updated every `2 × 16 / 2 = 8` triggers.

If `monitor` keyword is false, the monitor window would not show up.

If `progress` keyword is false, the progress message would be suppressed.

The `kwargs` will be directly passed to `Ionimaging.run`. See the help of
[`Ionimaging.run`](@ref).

The external trigger is used by default.

# Example
```julia-repl
julia> imaging(100, 260);

julia> b
1-element Array{Ionimage,1}

julia> b[1]
Ionimaging.Ionimage:
  date  : 2019-01-19T20:30:27.435
  size  : 600x800
  count : 1002 (accumulation 100)

julia> bg = background();

julia> imaging(100, 260, bg=bg);

julia> hp = hotpixel();

julia> imaging(100, 260, mask=hp);
```

# REFERENCE
1. B. Y. Chang, R. C. Hoetzlein, J. A. Mueller, J. D. Geiser and P. L. Houston,
   Improved two-dimensional product imaging: The real-time ion-counting method,
   *Rev. Sci. Instrum.*, 1998, **69**, 1665–1670.
"""
function imaging(count_accum::Integer, threshold; kwargs...)
    opt, camera_parameters = Config.imaging(; kwargs...)

    function initialize(args, cam, meta, count_accum)
        T = Image.image_eltype(opt.bg)
        img = zeros(T, size(cam))  # row-major
        bgᵀ = Image.transpose_background(opt.bg)
        Image.check_sizes(img, bgᵀ)
        maskᵀ = Image.transpose_mask(opt.mask)
        plotbuf = Vector{Int}(undef, length(img))
        count_ion = 0
        count_laser = 0
        opt.progress && showprogress()
        img, bgᵀ, maskᵀ, plotbuf, count_ion, count_laser
    end

    function update(args, cam, bufnrs, count_accum, count_done)
        if isempty(bufnrs)
            return args
        end
        img, bgᵀ, maskᵀ, plotbuf, count_ion, count_laser = args
        count_ion += Image.eventcount!(img, cam, bufnrs, threshold, bgᵀ, maskᵀ)
        bufnum = length(bufnrs)
        count_laser += bufnum
        if count_done % (opt.interval*bufnum) == 0 || count_done == count_accum
            if opt.progress
                showprogress(count_accum, count_done, count_ion, count_laser)
                count_ion = 0
                count_laser = 0
            end
            opt.monitor && Image.show(img, plotbuf)
        end
        img, bgᵀ, maskᵀ, plotbuf, count_ion, count_laser
    end

    function finalize(args, meta, count_accum, count_done)
        img, _, _, _, _, _ = args
        opt.progress && showsummary(img, count_accum, count_done)
        img = transpose(img)  # transpose the image to column-major order
        meta["accumulation"] = count_done
        meta["threshold"] = threshold
        meta["mask"] = opt.mask
        img, meta
    end

    img, meta = accumulateimage(count_accum, initialize, update, finalize, ();
                                camera_parameters...)
    ionimage = Ionimage(img, meta)
    if opt.save && meta["accumulation"] == count_accum
        _save(ionimage)
    end
    ionimage
end


"""
    test(count_accum, threshold; kwargs...)

This is an equivalent of `imaging(count_accum, threshold, save=false)`.
See the help of [`imaging`](@ref)
"""
function test(count_accum, threshold; kwargs...)
    imaging(count_accum, threshold; save=false, kwargs...)
end


"""
    imagingsp(count_accum::Integer, threshold; save=true, bg=nothing, mask=[],
              interval=2, monitor=false, progress=true, kwargs...)

Accumulate ion images with sub-pixel ion counting. `count_accum` is the number
of accumulation and `threshold` is the threshold for ion counting.

If `save` keyword is true, the result will be stored to the global variable `b`
and saved as a file in `Ionimage.DATADIR[]`.

The `bg` keyword is used for background correction.

The `mask` keyword is employed to mask hot pixels.

The `interval` keyword is employed to control the interval of monitor
window update. In general, image plotting is costly, and thus too frequent
monitor update drops the rate of measurement. This option is useful when
working with high repetition rate. The default is 2 and the monitor is updated
every `{interval} × {number of internal image buffers} / 2` triggers.
`{number of internal memory}` would be 16 for almost all cameras. Hence, in
this case, the monitor window is updated every `2 × 16 / 2 = 16` triggers.

If `monitor` keyword is false, the monitor window would not show up.

If `progress` keyword is false, the progress message would be suppressed.

The `kwargs` will be directly passed to `Ionimaging.run`. See the
help of [`Ionimaging.run`](@ref).

The external trigger is used by default.

The result is returned as an Ionspots instance. It can be converted to an
Ionimage instance by using [`develop`](@ref) function with an arbitrary
magnification factor (typically 1 ~ 5).

# Example
```julia-repl
julia> imagingsp(100, 260);

julia> b
1-element Array{Ionimage,1}

julia> b[1]
Ionimaging.Ionspots:
  date  : 2019-01-19T20:30:27.435
  count : 1002 (100 accumulation)

julia> bg = background();

julia> imagingsp(100, 260, bg=bg);

julia> hp = hotpixel();

julia> imagingsp(100, 260, mask=hp);

julia> img_x1 = develop(b[1], 1)
Ionimaging.Ionimage:
  date  : 2019-01-19T20:30:27.435
  size  : 600x800
  count : 1002 (100 accumulation)

julia> img_x2 = develop(b[1], 2)
Ionimaging.Ionimage:
  date  : 2019-01-19T20:30:27.435
  size  : 1200x1600
  count : 1002 (100 accumulation)
```

# REFERENCE
1. W. Li, S. D. Chambreau, S. A. Lahankar and A. G. Suits, Megapixel ion
   imaging with standard video, *Rev. Sci. Instrum.*, 2005, **76**, 063106.
"""
function imagingsp(count_accum::Integer, threshold; kwargs...)
    opt, camera_parameters = Config.imagingsp(; kwargs...)

    function initialize(args, cam, meta, count_accum)
        spots = NTuple{2,Float64}[]
        T = Image.image_eltype(opt.bg)
        shape = size(cam)
        img = zeros(T, shape)  # row-major
        bgᵀ = Image.transpose_background(opt.bg)
        Image.check_sizes(img, bgᵀ)
        maskᵀ = Image.transpose_mask(opt.mask)
        checked = Array{Bool,2}(undef, shape)
        plotbuf = Vector{Int}(undef, length(img))
        count_ion = 0
        count_laser = 0
        opt.progress && showprogress()
        spots, img, bgᵀ, maskᵀ, checked, plotbuf, count_ion, count_laser
    end

    function update(args, cam, bufnrs, count_accum, count_done)
        if isempty(bufnrs)
            return args
        end
        spots, img, bgᵀ, maskᵀ, checked, plotbuf, count_ion, count_laser = args
        n = length(spots)
        spots = Image.countcentroid!(spots, cam, bufnrs, threshold, checked, bgᵀ, maskᵀ)
        increment = spots[n+1:end]
        count_ion += length(increment)
        Types._develop!(img, increment, 1)
        buflen = length(bufnrs)
        count_laser += buflen
        if count_done % (opt.interval*buflen) == 0 || count_done == count_accum
            if opt.progress
                showprogress(count_accum, count_done, count_ion, count_laser)
                count_ion = 0
                count_laser = 0
            end
            opt.monitor && Image.show(img, plotbuf)
        end
        spots, img, bgᵀ, maskᵀ, checked, plotbuf, count_ion, count_laser
    end

    function finalize(args, meta, count_accum, count_done)
        spots, img, _, _, _, _, _, _ = args
        opt.progress && showsummary(img, count_accum, count_done)
        img = transpose(img)                   # transpose the image to column-major order
        spots = map(s -> (s[2], s[1]), spots)  # convert the coordinates to column-major order
        meta["accumulation"] = count_done
        meta["threshold"] = threshold
        meta["mask"] = opt.mask
        spots, img, meta
    end

    spots, img, meta = accumulateimage(count_accum, initialize, update,
                                       finalize, (); camera_parameters...)
    ionspots = Ionspots(spots, img, meta)
    if opt.save && meta["accumulation"] == count_accum
        _save(ionspots)
    end
    ionspots
end


"""
    testsp(count_accum, threshold; bg=nothing, mask=[], kwargs...)

This is an equivalent of `imagingsp(count_accum, threshold, save=false)`.
See the help of [`imagingsp`](@ref)
"""
function testsp(count_accum, threshold; kwargs...)
    imagingsp(count_accum, threshold; save=false, kwargs...)
end


"""
    live(; bg=nothing, kwargs...)

Take images continuously and show it like a "video camera".
This function can be stopped by pressing CTRL and BREAK keys simultaneously.

The `bg` keyword is used for background correction.

The `kwargs` will be directly passed to `Ionimaging.run`. See the help of
[`Ionimaging.run`](@ref).

The internal trigger is used by default.
"""
function live(; kwargs...)
    opt, camera_parameters = Config.live(; kwargs...)
    run(; trig=0, camera_parameters...) do cam, meta
        img = Array{Int,2}(undef, size(cam))  # row-major
        bgᵀ = Image.transpose_background(opt.bg)
        plotbuf = Vector{Int}(undef, length(img))
        print("Press CTRL+Break or CTRL+C to stop...")
        try
            while true
                Pco.take!(cam, img)
                Image.background_correction!(img, bgᵀ)
                Image.show(img, plotbuf)
            end
        catch e
            if isa(e, InterruptException)
                println("   Stopped.")
            else
                rethrow()
            end
        end
        img, meta
    end
    nothing
end


const PREVIEW_FIGURE_NUMBER = Ref{UInt}(0)

"""
    preview_imaging(raw::AbstractMatrix{<:Real}, thr; mask=[], kwargs...)

This function shows the image `raw` (in gray scale) and signal pixels (in red)
found by using event counting method with the threshold `thr`.

The `mask` keyword is employed to mask hot pixels.

# Example
```julia-repl
julia> img = take(trig=2);

julia> preview_imaging(img, 230);
```

# REFERENCE
1. B. Y. Chang, R. C. Hoetzlein, J. A. Mueller, J. D. Geiser and P. L. Houston,
   Improved two-dimensional product imaging: The real-time ion-counting method,
   *Rev. Sci. Instrum.*, 1998, **69**, 1665–1670.
"""
function preview_imaging(raw::AbstractMatrix{<:Real}, thr; bg=nothing, mask=[],
                         kwargs...)
    height, width = size(raw)
    arr = float.(copy(raw))
    arr = Image.normalize(arr)

    arr = log10.(arr .+ 1)
    arr = Image.normalize(arr)

    colored = zeros(Float32, height, width, 3)
    colored[:, :, 1] = arr
    colored[:, :, 2] = arr
    colored[:, :, 3] = arr

    peak = zeros(Int32, size(raw))
    Image.eventcount!(peak, raw, thr, bg, mask)
    for x in 1:width, y in 1:height
        if peak[y, x] > 0
            colored[y, x, 1] = 0.8
            colored[y, x, 2] = 0.1
            colored[y, x, 3] = 0.1
        end
    end

    if PREVIEW_FIGURE_NUMBER[] == 0
        fig = PyPlot.figure()
        PREVIEW_FIGURE_NUMBER[] = fig.number
    else
        fig = PyPlot.figure(PREVIEW_FIGURE_NUMBER[])
    end
    fig.clf()
    fig.canvas.set_window_title("Peak detect")
    ax = fig.add_subplot(1, 1, 1)
    img = ax.imshow(colored; kwargs...)
    fig.canvas.draw()
    img
end

function preview_imaging(raw::AbstractIonimage, thr; kwargs...)
    preview_imaging(raw.arr, thr; kwargs...)
end


"""
    preview_imagingsp(raw::AbstractMatrix{<:Real}, thr, m=1; mask=[],
                      showblob=false, kwargs...)

This function shows the image `raw` (in gray scale) and the centroid pixels (in
red) found by using sub-pixel event counting method with the threshold `thr`.

`m` is the magnification factor. If `raw` is an 600×800 image and `m` is 2, the
obtained preview resolution would be 1200×1600.

The `mask` keyword is employed to mask hot pixels.

If `showblob` is true, the pixels with the values larger than `thr` will be
colored yellow.

# Example
```julia-repl
julia> img = take(trig=2);

julia> preview_imagingsp(img, 230);
```

# REFERENCE
1. W. Li, S. D. Chambreau, S. A. Lahankar and A. G. Suits, Megapixel ion
   imaging with standard video, *Rev. Sci. Instrum.*, 2005, **76**, 063106.
"""
function preview_imagingsp(raw::AbstractMatrix{<:Real}, thr, m=1;
                           bg=nothing, mask=[], showblob=false, kwargs...)
    height, width = size(raw)
    arr = float.(copy(raw))
    arr = Image.normalize(arr)

    arr = log10.(arr .+ 1)
    arr = Image.normalize(arr)

    colored = zeros(Float32, height*m, width*m, 3)
    for x in 1:width*m, y in 1:height*m
        y_arr = ceil(Int, y/m)
        x_arr = ceil(Int, x/m)
        colored[y, x, 1] = arr[y_arr, x_arr]
        colored[y, x, 2] = arr[y_arr, x_arr]
        colored[y, x, 3] = arr[y_arr, x_arr]
    end

    # color yellow BLOBs
    if showblob
        bloblist = Image.extractBLOB(raw, thr, bg, mask)
        for blob in bloblist
            for (y, x) in blob
                ymmin = Types.magnify(y - 0.5, m)
                xmmin = Types.magnify(x - 0.5, m)
                ymmax = Types.magnify(y + 0.5, m)
                xmmax = Types.magnify(x + 0.5, m)
                for xm in xmmin:xmmax, ym in ymmin:ymmax
                    colored[ym, xm, 1] = 0.9
                    colored[ym, xm, 2] = 0.9
                    colored[ym, xm, 3] = 0.3
                end
            end
        end
    end

    # color red centroid pixels
    spots = NTuple{2,Float64}[]
    Image.countcentroid!(spots, raw, thr, bg, mask)
    peak = zeros(Int32, (height*m, width*m))
    Types._develop!(peak, spots, m)
    for x in 1:width*m, y in 1:height*m
        if peak[y, x] > 0
            colored[y, x, 1] = 0.8
            colored[y, x, 2] = 0.1
            colored[y, x, 3] = 0.1
        end
    end

    if PREVIEW_FIGURE_NUMBER[] == 0
        fig = PyPlot.figure()
        PREVIEW_FIGURE_NUMBER[] = fig.number
    else
        fig = PyPlot.figure(PREVIEW_FIGURE_NUMBER[])
    end
    fig.clf()
    fig.canvas.set_window_title("Peak detect")
    ax = fig.add_subplot(1, 1, 1)
    img = ax.imshow(colored; kwargs...)
    fig.canvas.draw()
    img
end

function preview_imagingsp(raw::AbstractIonimage, thr, m=1; kwargs...)
    preview_imagingsp(raw.arr, thr, m; kwargs...)
end


"""
    window(arr::AbstractMatrix{<:Real}; kwargs...)

Open a new window to show an image `arr`.
"""
function window(arr::AbstractMatrix{<:Real}; kwargs...)
    fig = PyPlot.figure()
    ax = fig.add_subplot(1, 1, 1)
    img = ax.imshow(arr; kwargs...)
    fig.canvas.draw()
    img
end


const HISTOGRAM_FIGURE_NUMBER = Ref{UInt}(0)

"""
    histogram(arr::AbstractMatrix{<:Real}; xlim=nothing, ylim=nothing, kwargs...)

Show a histogram of the number of pixels v.s. those intensity. This is useful
to determine the threshold intensity a little higher than background signal.
"""
function histogram(arr::AbstractMatrix{<:Real}; xlim=nothing, ylim=nothing, kwargs...)
    x = reshape(arr, (:,))
    if HISTOGRAM_FIGURE_NUMBER[] == 0
        fig = PyPlot.figure()
        HISTOGRAM_FIGURE_NUMBER[] = fig.number
    else
        fig = PyPlot.figure(HISTOGRAM_FIGURE_NUMBER[])
    end
    fig.clf()
    fig.canvas.set_window_title("Counts v.s. pixel intensity histogram")
    ax = fig.add_subplot(1, 1, 1)
    ret = ax.hist(x; bins=30, log=true, kwargs...)
    ax.set_ylabel("Counts")
    ax.set_xlabel("Pixel intensity")
    if !isa(ylim, Nothing)
        ax.set_ylim(ylim[1], ylim[2])
    end
    if !isa(xlim, Nothing)
        ax.set_xlim(xlim[1], xlim[2])
    end
    ret
end


"""
    hotpixel(arr::Array{<:Real,2}, m::Real=12; bins=30)

Detect anomalously bright pixels. This method calculates the mean intensity and
its standard deviation of the image `arr`, and then search pixels having the
intensity stronger than `xc + m*s`; where `xc` is the mean intensity and `s` is
the standard deviation.
"""
function hotpixel(arr::AbstractMatrix{<:Real}, m::Real=12; bins=30)
    x = reshape(arr, (:,))
    xc = Statistics.mean(x)
    s = Statistics.std(x)
    thr = xc + m*s
    println("center    : $(xc)")
    println("std       : $(s)")
    println("threshold : $(thr)")
    println("")

    height, width = size(arr)
    hotpix = NTuple{2,Int}[]
    for x in 1:width, y in 1:height
        if arr[y, x] > thr
            push!(hotpix, (y, x))
        end
    end

    # show histogram
    n, bins, _ = histogram(arr, log=false, bins=bins)
    hotpix_counts = Int[0]
    for hp in hotpix
        y, x = hp
        for i in 1:length(bins)
            if bins[i] >= arr[y, x]
                push!(hotpix_counts, n[min(i, length(n))])
                break
            end
        end
    end
    ylim = [0, maximum(hotpix_counts) + 10]
    PyPlot.ylim(ylim[1], ylim[2])
    PyPlot.plot([thr, thr], ylim, "r:")

    hotpix
end

"""
    hotpixel(m::Real=12, accum::Integer=500; bins=30, kwargs...)

Accumulate images `accum` times and detect hotpixels.

The `kwargs` will be directly passed to `Ionimaging.run`. See the help of
[`Ionimaging.run`](@ref).

# Examples
```julia-repl
julia> hp = hotpixels();

julia> imaging(100, 240, mask=hp)
```
"""
function hotpixel(m::Real=12, accum::Integer=500; bins=30, kwargs...)
    ionimage = background(accum; kwargs...)
    hotpixel(ionimage, m, bins=bins)
end


# File I/O interfaces
include("fileio.jl")


end
