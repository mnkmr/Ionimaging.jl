import Printf

# Describe the specifications of the camera itself
function describe(cam::PcoCamera, outline=(:General, :Prameters, :Features))
    spec = cam.spec

    head(str) = printstyled(Printf.@sprintf("\n\n # %s\n", str),
                            color=:light_green)

    function title(t, str)
        printstyled(Printf.@sprintf("%-20s:", t), color=:light_blue)
        Printf.@printf(" %s\n", str)
    end

    notitle(str) = Printf.@printf("%21s %s\n", "", str)

    if :General in outline
        head("General descriptions")

        # camera name
        str = "$(spec[:camera]) (Serial number: $(spec[:serialnumber]))"
        title("camera", str)

        # sensor
        title("sensor", spec[:sensor])

        # interface
        title("interface", spec[:interface])

        # resolution
        height, width = spec[:resolution]
        str = Printf.@sprintf("%4d × %-4d pixel   (standard)", width, height)
        title("resolution", str)

        height_ex, width_ex = spec[:resolution_ex]
        str = Printf.@sprintf("%4d × %-4d pixel   (extended)", width_ex, height_ex)
        notitle(str)

        # dynamic range
        str = Printf.@sprintf("%d bit (0 ~ %d)", spec[:depth], 2^spec[:depth]-1)
        title("dynamic range", str)
    end


    if :Parameters in outline
        head("Parameters")

        # horizontal binning
        if isempty(spec[:hbins])
            str = "not available"
        elseif length(spec[:hbins]) == 1
            str = Printf.@sprintf("%s is available", spec[:hbins][1])
        else
            str = Printf.@sprintf("either one in %s is available", spec[:hbins])
        end
        title("horizontal binning", str)

        # vertical binning
        if isempty(spec[:vbins])
            str = "not available"
        elseif length(spec[:vbins]) == 1
            str = Printf.@sprintf("%s is available", spec[:vbins][1])
        else
            str = Printf.@sprintf("either one in %s is available", spec[:vbins])
        end
        title("vertical binning", str)

        # region-of-interest
        if spec[:ROI_hstep] == 0
            title("ROI", "not available")
        else
            str = Printf.@sprintf("%-6s = %3dn + 1 pixel   (%-6s ≧ %3d)",
                                  "width", spec[:ROI_hstep], "width", spec[:hmin])
            title("ROI", str)

            str = Printf.@sprintf("%-6s = %3dn + 1 pixel   (%-6s ≧ %3d)",
                                  "height", spec[:ROI_vstep], "height", spec[:vmin])
            notitle(str)
        end

        # software ROI
        if spec[:softROI_hstep] == 0
            title("software ROI", "not available")
        else
            str = Printf.@sprintf("%-6s = %3dn + 1 pixel   (%-6s ≧ %3d)",
                                  "width", spec[:softROI_hstep], "width", spec[:hmin])
            title("software ROI", str)

            str = Printf.@sprintf("%-6s = %3dn + 1 pixel   (%-6s ≧ %3d)",
                                  "height", spec[:softROI_vstep], "height", spec[:vmin])
            notitle(str)
        end

        # pixel rate
        if isempty(spec[:pixelrates])
            str = "not changeable"
        elseif length(spec[:pixelrates]) == 1
            str = Printf.@sprintf("%s is available", spec[:pixelrates][1])
        else
            str = Printf.@sprintf("either one in %s is available", spec[:pixelrates])
        end
        title("pixel rate", str)

        # conversion factor
        if isempty(spec[:convfactors])
            str = "not changeable"
        elseif length(spec[:convfactors]) == 1
            str = Printf.@sprintf("%s is available", spec[:convfactors][1])
        else
            str = Printf.@sprintf("either one in %s is available", spec[:convfactors])
        end
        title("conversion factor", str)

        function floattime_to_string(f)
            v, b = splitfloat(f)
            if b == 0x002 && v >= 1e3 && rem(Float64(v), 1e3, RoundNearest) < 1e-9
                str = "$(round(Int, v/1e3))s"
            else
                str = "$(v)$(b == 0x000 ? "ns" : b == 0x001 ? "μs" : "ms")"
            end
            str
        end

        # delay time
        delaymin = floattime_to_string(spec[:delaymin])
        delaymax = floattime_to_string(spec[:delaymax])
        delaystep = floattime_to_string(spec[:delaystep])
        if delaymin == delaymax
            str = Printf.@sprintf("fixed at %5s", delaymin)
        else
            str = Printf.@sprintf("%5s ~ %5s with %5s interval", delaymin,
                                  delaymax, delaystep)
        end
        title("delay time", str)

        if spec[:IR] == 1
            delayminIR = floattime_to_string(spec[:delaymin_IR])
            delaymaxIR = floattime_to_string(spec[:delaymax_IR])
            if delayminIR == delaymaxIR
                str = Printf.@sprintf("fixed at %5s (IR sensitivity mode)", delaymaxIR)
            else
                str = Printf.@sprintf("%5s ~ %5s with %5s interval (IR sensitivity mode)",
                                      delayminIR, delaymaxIR, delaystep)
            end
            notitle(str)
        end

        # exposure time
        exposuremin = floattime_to_string(spec[:exposuremin])
        exposuremax = floattime_to_string(spec[:exposuremax])
        exposurestep = floattime_to_string(spec[:exposurestep])
        str = Printf.@sprintf("%5s ~ %5s with %5s interval", exposuremin,
                              exposuremax, exposurestep)
        title("exposure time", str)

        if spec[:IR] == 1
            exposureminIR = floattime_to_string(spec[:exposuremin_IR])
            exposuremaxIR = floattime_to_string(spec[:exposuremax_IR])
            str = Printf.@sprintf("%5s ~ %5s with %5s interval (IR sensitivity mode)",
                                  exposureminIR, exposuremaxIR, exposurestep)
            notitle(str)
        end

        # Analog/Digital Converter
        if spec[:ADC] == 1
            str = Printf.@sprintf("%d ADC is available", spec[:ADC])
        else
            str = Printf.@sprintf("%d ADCs are available", spec[:ADC])
        end
        title("ADC", str)

        if spec[:GeneralCaps1][:COOLING_SETPOINTS]
            if length(spec[:coolsetpoints]) == 1
                str = Printf.@sprintf("%s is available", spec[:coolsetpoints][1])
            else
                str = Printf.@sprintf("either one in %s is available",
                                      spec[:coolsetpoints])
            end
            title("cooling setpoints", str)
            str = Printf.@sprintf("%d by default", spec[:coolsetdef])
            notitle(str)
        else
            title("cooling setpoints", "not changeable")
        end
    end


    if :Features in outline
        head("Features")

        # Time table
        str = spec[:timetable] == 1 ? "available" : "unavailable"
        title("time table", str)

        # Double image mode
        str = spec[:doubleimage] == 1 ? "available" : "unavailable"
        title("double image", str)

        # Power down mode
        str = spec[:powerdownmode] == 1 ? "available" : "unavailable"
        title("power down mode", str)

        # Offset regulation
        str = spec[:offsetregulation] == 1 ? "available" : "unavailable"
        title("offset regulation", str)

        # Noise filter
        str = spec[:GeneralCaps1][:NOISE_FILTER] ? "available" : "unavailable"
        title("noise filter", str)

        # Hot pixel filter
        str = spec[:GeneralCaps1][:HOTPIX_FILTER] ? "available" : "unavailable"
        title("hot pixel filter", str)

        # Hot pixel correction
        str = spec[:GeneralCaps1][:HOT_PIXEL_CORRECTION] ? "available" : "unavailable"
        if spec[:GeneralCaps1][:HOTPIX_ONLY_WITH_NOISE_FILTER]
            str *= "  (only with noise filter)"
        end
        title("hot pixel correction", str)

        # Record stop mode
        str = spec[:GeneralCaps1][:RECORD_STOP] ? "available" : "unavailable"
        title("record stop mode", str)

        # External exposure control
        str = spec[:GeneralCaps1][:NO_EXTEXPCTRL] ? "unavailable" : "available"
        title("external exposure", str)

        # Time stamp
        if spec[:GeneralCaps1][:NO_TIMESTAMP]
            str = "unavailable"
        else
            str = "available"
            if spec[:GeneralCaps1][:TIMESTAMP_ASCII_ONLY]
                str *= "  (only ASCII time stamp is available)"
            end
        end
        title("time stamp", str)

        # Acquire mode
        str = spec[:GeneralCaps1][:NO_ACQUIREMODE] ? "unavailable" : "available"
        title("acquire mode", str)

        # Internal recorder
        str = spec[:GeneralCaps1][:NO_RECORDER] ? "unavailable" : "available"
        title("internal recorder", str)

        # Fast timing mode
        str = spec[:GeneralCaps1][:FAST_TIMING] ? "available" : "unavailable"
        title("fast timing mode", str)

        # Meta data
        str = spec[:GeneralCaps1][:METADATA] ? "available" : "unavailable"
        title("Meta Data", str)

        # Set/GetFrameRate
        str = spec[:GeneralCaps1][:SETFRAMERATE_ENABLED] ? "available" : "unavailable"
        title("Set/GetFrameRate", str)

        # Correlated double image mode
        str = spec[:GeneralCaps1][:CDI_MODE] ? "available" : "unavailable"
        title("correlated double image mode", str)

        # Internal color correction matrix
        str = spec[:GeneralCaps1][:CCM] ? "available" : "unavailable"
        title("internal color correction matrix", str)

        # Trigger mode external sync
        str = spec[:GeneralCaps1][:EXTERNAL_SYNC] ? "available" : "unavailable"
        title("trigger mode external sync", str)

        # Global shutter operation mode
        str = spec[:GeneralCaps1][:NO_GLOBAL_SHUTTER] ? "unavailable" : "available"
        title("global shutter operation mode", str)

        # Global reset operation mode
        str = spec[:GeneralCaps1][:GLOBAL_RESET_MODE] ? "available" : "unavailable"
        title("Global reset operation mode", str)

        # Extended acquire
        str = spec[:GeneralCaps1][:EXT_ACQUIRE] ? "available" : "unavailable"
        title("extended acquire", str)

        # Fan LED control
        str = spec[:GeneralCaps1][:FAN_LED_CONTROL] ? "available" : "unavailable"
        title("fan LED control", str)

        # Camera ROI constraints
        if spec[:GeneralCaps1][:ROI_VERT_SYMM_TO_HORZ_AXIS] &&
        spec[:GeneralCaps1][:ROI_HORZ_SYMM_TO_VERT_AXIS]
            title("camera ROI constraints",
                "Vertical ROI must be symmetrical to horizontal axis")
            notitle("Horizontal ROI must be symmetrical to vertical axis")
        elseif spec[:GeneralCaps1][:ROI_VERT_SYMM_TO_HORZ_AXIS]
            title("camera ROI constraints",
                "Vertical ROI must be symmetrical to horizontal axis")
        elseif spec[:GeneralCaps1][:ROI_HORZ_SYMM_TO_VERT_AXIS]
            title("camera ROI constraints",
                "Horizontal ROI must be symmetrical to vertical axis")
        else
            title("camera ROI constraints", "nothing")
        end

        # User interface command
        str = spec[:GeneralCaps1][:USER_INTERFACE] ? "available" : "unavailable"
        title("user interface command", str)

        # Hardware I/O descripter
        str = spec[:GeneralCaps1][:HW_IO_SIGNAL_DESCRIPTOR] ? "available" : "unavailable"
        title("hardware I/O descripter", str)

        # HDSDI interface with 1.5 Gbit datarate
        if spec[:GeneralCaps3][:HDSDI_1G5]
            title("HDSDI interface with 1.5 Gbit datarate", "available")
        end

        # HDSDI interface with 3 Gbit datarate
        if spec[:GeneralCaps3][:HDSDI_3G]
            title("HDSDI interface with 3 Gbit datarate", "available")
        end

        # Unmodulated IRIG B
        if spec[:GeneralCaps3][:IRIG_B_UNMODULATED]
            title("unmodulated IRIG B", "Unmodulated IRIG B can be evaluated")
        end

        # Modulated IRIG B
        if spec[:GeneralCaps3][:IRIG_B_MODULATED]
            title("modulated IRIG B", "Modulated IRIG B can be evaluated")
        end

        # Camera Sync mode
        if spec[:GeneralCaps3][:CAMERA_SYNC]
            title("camera sync mode", "available")
        end

        # Fast Sensor readout
        if spec[:GeneralCaps3][:HS_READOUT_MODE]
            title("fast sensor readout", "available")
        end

        # External synchronized trigger in 1Hz mode
        if spec[:GeneralCaps3][:EXT_SYNC_1HZ_MODE]
            title("External synchronized trigger",
                "In trigger mode [external synchronized] multiples of 1Hz can be evaluated")
        end
    end

    nothing
end
