struct PcoPixelfly <:PcoCamera
    handle::HANDLE
    spec::Dict{Symbol,Any}
    bufmax::Int
    buffer::Dict{Int,ImageBuffer}
    queue::Vector{Int}
    available::Vector{Int}

    function PcoPixelfly(handle, spec)
        buffer = Dict{Int,ImageBuffer}()
        queue = Vector{Int}()
        available = Vector{Int}()
        new(handle, spec, 16, buffer, queue, available)
    end
end


function IRsensitivity(cam::PcoPixelfly)
    ir = Ref(WORD(0))
    @rccheck API.GetIRSensitivity(cam.handle, ir)
    ir[]
end


function IRsensitivity!(cam::PcoPixelfly, ir::Integer)
    @rccheck API.SetIRSensitivity(cam.handle, ir)
end
