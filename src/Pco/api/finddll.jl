# Search SC2_Cam.dll
function finddll()
    if haskey(ENV, "PCO_SC2CAMDLL")
        return ENV["PCO_SC2CAMDLL"]
    end
    if !Sys.iswindows()
        return ""
    end

    userlocal = ENV["AppData"]
    allusers = ENV["PROGRAMFILES(x86)"]

    runtime = "pco.runtime"
    sdk = "pco.sdk"

    bindir = Sys.WORD_SIZE == 32 ? "bin" : "bin64"
    dllname = "SC2_Cam.dll"

    dllpath = ""
    for parentdir in [userlocal, allusers], packagedir in [runtime, sdk]
        path = joinpath(parentdir, "Digital Camera Toolbox", packagedir, bindir, dllname)
        if isfile(path)
            dllpath = path
            break
        end
    end
    dllpath
end

