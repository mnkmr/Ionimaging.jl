#----------------------------------------------------------------------
# INTERFACE CODES: The list of valid values for Openstruct.wInterfaceType
# !!!NOTE!!! : This is different from the INTERFACETYPE!
#----------------------------------------------------------------------

const PCO_INTERFACE_FW      =  1         # Firewire interface
const PCO_INTERFACE_CL_MTX  =  2         # Cameralink Matrox Solios / Helios
const PCO_INTERFACE_CL_ME3  =  3         # Cameralink Silicon Software Me3
const PCO_INTERFACE_CL_NAT  =  4         # Cameralink National Instruments
const PCO_INTERFACE_GIGE    =  5         # Gigabit Ethernet
const PCO_INTERFACE_USB     =  6         # USB 2.0
const PCO_INTERFACE_CL_ME4  =  7         # Cameralink Silicon Software Me4
const PCO_INTERFACE_USB3    =  8         # USB 3.0 and USB 3.1 Gen1
const PCO_INTERFACE_WLAN    =  9         # WLan (Only control path, not data path)
const PCO_INTERFACE_CLHS    = 11         # Cameralink HS

const PCO_INTERFACE_CL_SER  = 10
const PCO_INTERFACE_GENERIC = 20

const PCO_INTERFACE_VIRTUAL = 0x1000     # reserved for virtual camera


#----------------------------------------------------------------------
# CAMERATYPES: The list of possible values for CameraType.wCamType
#----------------------------------------------------------------------
# pco.camera types
const CAMERATYPE_PCO1200HS        = 0x0100
const CAMERATYPE_PCO1300          = 0x0200
const CAMERATYPE_PCO1600          = 0x0220
const CAMERATYPE_PCO2000          = 0x0240
const CAMERATYPE_PCO4000          = 0x0260

# pco.1300 types
const CAMERATYPE_ROCHEHTC         = 0x0800 # Roche OEM
const CAMERATYPE_284XS            = 0x0800
const CAMERATYPE_KODAK1300OEM     = 0x0820 # Kodak OEM

# pco.1400 types
const CAMERATYPE_PCO1400          = 0x0830
const CAMERATYPE_NEWGEN           = 0x0840 # Roche OEM
const CAMERATYPE_PROVEHR          = 0x0850 # Zeiss OEM

# pco.usb.pixelfly
const CAMERATYPE_PCO_USBPIXELFLY  = 0x0900

# pco.dimax types
const CAMERATYPE_PCO_DIMAX_STD           = 0x1000
const CAMERATYPE_PCO_DIMAX_TV            = 0x1010

const CAMERATYPE_PCO_DIMAX_AUTOMOTIVE    = 0x1020   # obsolete and not used for the pco.dimax, please remove from your sources!
const CAMERATYPE_PCO_DIMAX_CS            = 0x1020   # code is now used for pco.dimax CS

const CAMERASUBTYPE_PCO_DIMAX_Weisscam   = 0x0064   # 100 = Weisscam, all features
const CAMERASUBTYPE_PCO_DIMAX_HD         = 0x80FF   # pco.dimax HD
const CAMERASUBTYPE_PCO_DIMAX_HD_plus    = 0xC0FF   # pco.dimax HD+
const CAMERASUBTYPE_PCO_DIMAX_X35        = 0x00C8   # 200 = Weisscam/P+S HD35

const CAMERASUBTYPE_PCO_DIMAX_HS1        = 0x207F
const CAMERASUBTYPE_PCO_DIMAX_HS2        = 0x217F
const CAMERASUBTYPE_PCO_DIMAX_HS4        = 0x237F

const CAMERASUBTYPE_PCO_DIMAX_CS_AM_DEPRECATED = 0x407F
const CAMERASUBTYPE_PCO_DIMAX_CS_1       = 0x417F
const CAMERASUBTYPE_PCO_DIMAX_CS_2       = 0x427F
const CAMERASUBTYPE_PCO_DIMAX_CS_3       = 0x437F
const CAMERASUBTYPE_PCO_DIMAX_CS_4       = 0x447F

# pco.sensicam types                        # tbd., all names are internal ids
const CAMERATYPE_SC3_SONYQE              = 0x1200 # SC3 based - Sony 285
const CAMERATYPE_SC3_EMTI                = 0x1210 # SC3 based - TI 285SPD
const CAMERATYPE_SC3_KODAK4800           = 0x1220 # SC3 based - Kodak KAI-16000

# pco.edge types
const CAMERATYPE_PCO_EDGE                = 0x1300 # pco.edge 5.5 (Sensor CIS2521) Interface: CameraLink , rolling shutter
const CAMERATYPE_PCO_EDGE_42             = 0x1302 # pco.edge 4.2 (Sensor CIS2020) Interface: CameraLink , rolling shutter
const CAMERATYPE_PCO_EDGE_GL             = 0x1310 # pco.edge 5.5 (Sensor CIS2521) Interface: CameraLink , global  shutter
const CAMERATYPE_PCO_EDGE_USB3           = 0x1320 # pco.edge     (all sensors   ) Interface: USB 3.0    ,(all shutter modes)
const CAMERATYPE_PCO_EDGE_HS             = 0x1340 # pco.edge     (all sensors   ) Interface: high speed ,(all shutter modes) 
const CAMERATYPE_PCO_EDGE_MT             = 0x1304 # pco.edge MT2 (all sensors   ) Interface: CameraLink Base, rolling shutter

const CAMERASUBTYPE_PCO_EDGE_SPRINGFIELD = 0x0006
const CAMERASUBTYPE_PCO_EDGE_31          = 0x0031
const CAMERASUBTYPE_PCO_EDGE_42          = 0x0042
const CAMERASUBTYPE_PCO_EDGE_55          = 0x0055
const CAMERASUBTYPE_PCO_EDGE_DEVELOPMENT = 0x0100
const CAMERASUBTYPE_PCO_EDGE_X2          = 0x0200
const CAMERASUBTYPE_PCO_EDGE_RESOLFT     = 0x0300
const CAMERASUBTYPE_PCO_EDGE_GOLD        = 0x0FF0
const CAMERASUBTYPE_PCO_EDGE_DUAL_CLOCK  = 0x000D
const CAMERASUBTYPE_PCO_EDGE_DICAM       = 0xDC00
const CAMERASUBTYPE_PCO_EDGE_42_LT       = 0x8042

# pco.flim types
const CAMERATYPE_PCO_FLIM                = 0x1400 # pco.flim

# pco.flow types
const CAMERATYPE_PCO_FLOW                = 0x1500 # pco.flow

# pco.panda types
const CAMERATYPE_PCO_PANDA               = 0x1600 # pco.panda (deprecated, use CAMERATYPE_PCO_FAMILY_PANDA for the future)

# camera types for pco camera families
const CAMERATYPE_PCO_FAMILY_PANDA        = 0x1600 # pco.panda
const CAMERATYPE_PCO_FAMILY_EDGE         = 0x1800 # pco.edge
const CAMERATYPE_PCO_FAMILY_DICAM        = 0x1700 # pco.dicam
const CAMERATYPE_PCO_FAMILY_DIMAX        = 0x1900 # pco.dimax

### NOTE: Probably not used or not maintaind below because they are obviously
#         duplicated to each other

# # PANDA Family
# const CAMERASUBTYPE_PCO_PANDA_42         = 0x0000 # pco.panda 4.2
# const CAMERASUBTYPE_PCO_PANDA_42_BI      = 0x0001 # pco.panda 4.2 bi
# const CAMERASUBTYPE_PCO_PANDA_150        = 0x0002 # pco.panda 15

# # EDGE Family
# const CAMERASUBTYPE_PCO_EDGE_42_BI       = 0x0001 # pco.edge 4.2 bi

# # DICAM Family
# cosnt CAMERASUBTYPE_PCO_DICAM_C1         = 0x0001 # pco.dicam C1
# cosnt CAMERASUBTYPE_PCO_DICAM_C2         = 0x0002 # reserved
# cosnt CAMERASUBTYPE_PCO_DICAM_C3         = 0x0003 # reserved
# cosnt CAMERASUBTYPE_PCO_DICAM_C4         = 0x0004 # pco.dicam C4

# Virtual camera for development
const CAMERATYPE_PCO_VIRTUAL             = 0xFFFF


# ----------------------------------------------------------------------
# SENSORTYPES: The list of possible values for Description.wSensorTypeDESC
# ----------------------------------------------------------------------
const SENSOR_ICX285AL            = 0x0010      # Sony
const SENSOR_ICX285AK            = 0x0011      # Sony
const SENSOR_ICX263AL            = 0x0020      # Sony
const SENSOR_ICX263AK            = 0x0021      # Sony
const SENSOR_ICX274AL            = 0x0030      # Sony
const SENSOR_ICX274AK            = 0x0031      # Sony
const SENSOR_ICX407AL            = 0x0040      # Sony
const SENSOR_ICX407AK            = 0x0041      # Sony
const SENSOR_ICX414AL            = 0x0050      # Sony
const SENSOR_ICX414AK            = 0x0051      # Sony
const SENSOR_ICX407BLA           = 0x0060      # Sony UV type

const SENSOR_KAI2000M            = 0x0110      # Kodak
const SENSOR_KAI2000CM           = 0x0111      # Kodak
const SENSOR_KAI2001M            = 0x0120      # Kodak
const SENSOR_KAI2001CM           = 0x0121      # Kodak
const SENSOR_KAI2002M            = 0x0122      # Kodak slow roi
const SENSOR_KAI2002CM           = 0x0123      # Kodak slow roi

const SENSOR_KAI4010M            = 0x0130      # Kodak
const SENSOR_KAI4010CM           = 0x0131      # Kodak
const SENSOR_KAI4011M            = 0x0132      # Kodak slow roi
const SENSOR_KAI4011CM           = 0x0133      # Kodak slow roi

const SENSOR_KAI4020M            = 0x0140      # Kodak
const SENSOR_KAI4020CM           = 0x0141      # Kodak
const SENSOR_KAI4021M            = 0x0142      # Kodak slow roi
const SENSOR_KAI4021CM           = 0x0143      # Kodak slow roi
const SENSOR_KAI4022M            = 0x0144      # Kodak 4022 monochrom
const SENSOR_KAI4022CM           = 0x0145      # Kodak 4022 color

const SENSOR_KAI11000M           = 0x0150      # Kodak
const SENSOR_KAI11000CM          = 0x0151      # Kodak
const SENSOR_KAI11002M           = 0x0152      # Kodak slow roi
const SENSOR_KAI11002CM          = 0x0153      # Kodak slow roi

const SENSOR_KAI16000AXA         = 0x0160      # Kodak t:4960x3324, e:4904x3280, a:4872x3248
const SENSOR_KAI16000CXA         = 0x0161      # Kodak

const SENSOR_MV13BW              = 0x1010      # Micron
const SENSOR_MV13COL             = 0x1011      # Micron

const SENSOR_CIS2051_V1_FI_BW    = 0x2000      #Fairchild front illuminated
const SENSOR_CIS2051_V1_FI_COL   = 0x2001
const SENSOR_CIS1042_V1_FI_BW    = 0x2002
const SENSOR_CIS2051_V1_BI_BW    = 0x2010      #Fairchild back illuminated

const SENSOR_TC285SPD            = 0x2120      # TI 285SPD

const SENSOR_CYPRESS_RR_V1_BW    = 0x3000      # CYPRESS RoadRunner V1 B/W
const SENSOR_CYPRESS_RR_V1_COL   = 0x3001      # CYPRESS RoadRunner V1 Color

const SENSOR_CMOSIS_CMV12000_BW  = 0x3100   # CMOSIS CMV12000 4096x3072 b/w
const SENSOR_CMOSIS_CMV12000_COL = 0x3101   # CMOSIS CMV12000 4096x3072 color

const SENSOR_QMFLIM_V2B_BW       = 0x4000      # CSEM QMFLIM V2B B/W

# const SENSOR_GPIXEL_X2_BW        = 0x5000  # Gpixel 2k
# const SENSOR_GPIXEL_X2_COL       = 0x5001  # Gpixel 2k

# const SENSOR_GPIXEL_X5_BW        = 0x5100  # Gpixel 5k

const SENSOR_GPIXEL_GSENSE2020_BW    = 0x5000  # Gpixel GSENSE2020 4.2M
const SENSOR_GPIXEL_GSENSE2020_COL   = 0x5001  # Gpixel GSENSE2020 4.2M

const SENSOR_GPIXEL_GSENSE2020BI_BW  = 0x5002  # Gpixel GSENSE2020BSI 4.2M-BI

const SENSOR_GPIXEL_GSENSE5130_BW    = 0x5004  # Gpixel GSENSE5130 15M
const SENSOR_GPIXEL_GSENSE5130_COL   = 0x5005  # Gpixel GSENSE5130 15M


# ----------------------------------------------------------------------
# INTERFACETYPES: The list of possible values for CameraType.wInterfaceType
# !!!NOTE!!! : This is different from the INTERFACE CODES!
# ----------------------------------------------------------------------
const INTERFACE_FIREWIRE        = 0x0001
const INTERFACE_CAMERALINK      = 0x0002
const INTERFACE_USB             = 0x0003
const INTERFACE_ETHERNET        = 0x0004
const INTERFACE_SERIAL          = 0x0005
const INTERFACE_USB3            = 0x0006
const INTERFACE_CAMERALINKHS    = 0x0007
const INTERFACE_COAXPRESS       = 0x0008
const INTERFACE_USB31_GEN1      = 0x0009
const INTERFACE_VIRTUAL         = 0x1000  # reserved for virtual camera

const INTERFACE_TYPES = [
    (INTERFACE_FIREWIRE,     "FireWire"),
    (INTERFACE_CAMERALINK,   "Camera Link"),
    (INTERFACE_USB,          "USB 2.0"),
    (INTERFACE_ETHERNET,     "GigE"),
    (INTERFACE_SERIAL,       "Serial Interface"),
    (INTERFACE_USB3,         "USB 3.0"),
    (INTERFACE_CAMERALINKHS, "Camera Link HS"),
    (INTERFACE_COAXPRESS,    "CoaXPress"),
    (INTERFACE_USB31_GEN1,   "USB 3.1 Gen1"),
]


# ----------------------------------------------------------------------
# The list of trigger mode choice for API.GetTriggerMode/SetTriggerMode
# ----------------------------------------------------------------------
const TRIGGER_MODE_AUTOTRIGGER                       = 0x0000
const TRIGGER_MODE_SOFTWARETRIGGER                   = 0x0001
const TRIGGER_MODE_EXTERNALTRIGGER                   = 0x0002
const TRIGGER_MODE_EXTERNALEXPOSURECONTROL           = 0x0003
const TRIGGER_MODE_SOURCE_HDSDI                      = 0x0102
const TRIGGER_MODE_EXTERNAL_SYNCHRONIZED             = 0x0004
const TRIGGER_MODE_FAST_EXTERNALEXPOSURECONTROL      = 0x0005
const TRIGGER_MODE_EXTERNAL_CDS                      = 0x0006
const TRIGGER_MODE_SLOW_EXTERNALEXPOSURECONTROL      = 0x0007


# ----------------------------------------------------------------------
# dwGeneralCaps1 member of the Camera Description structure
# ----------------------------------------------------------------------

const GENERALCAPS1_NOISE_FILTER                       = 0x00000001
const GENERALCAPS1_HOTPIX_FILTER                      = 0x00000002
const GENERALCAPS1_HOTPIX_ONLY_WITH_NOISE_FILTER      = 0x00000004
const GENERALCAPS1_TIMESTAMP_ASCII_ONLY               = 0x00000008

const GENERALCAPS1_DATAFORMAT2X12                     = 0x00000010
const GENERALCAPS1_RECORD_STOP                        = 0x00000020 # Record stop event mode
const GENERALCAPS1_HOT_PIXEL_CORRECTION               = 0x00000040
const GENERALCAPS1_NO_EXTEXPCTRL                      = 0x00000080 # Ext. Exp. ctrl not possible

const GENERALCAPS1_NO_TIMESTAMP                       = 0x00000100
const GENERALCAPS1_NO_ACQUIREMODE                     = 0x00000200
const GENERALCAPS1_DATAFORMAT4X16                     = 0x00000400
const GENERALCAPS1_DATAFORMAT5X16                     = 0x00000800

const GENERALCAPS1_NO_RECORDER                        = 0x00001000 # Camera has no internal memory
const GENERALCAPS1_FAST_TIMING                        = 0x00002000 # Camera can be set to fast timing mode (PIV)
const GENERALCAPS1_METADATA                           = 0x00004000 # Camera can produce metadata
const GENERALCAPS1_SETFRAMERATE_ENABLED               = 0x00008000 # Camera allows Set/GetFrameRate cmd

const GENERALCAPS1_CDI_MODE                           = 0x00010000 # Camera has Correlated Double Image Mode
const GENERALCAPS1_CCM                                = 0x00020000 # Camera has CCM
const GENERALCAPS1_EXTERNAL_SYNC                      = 0x00040000 # Camera can be synced externally
const GENERALCAPS1_NO_GLOBAL_SHUTTER                  = 0x00080000 # Camera does not support global shutter
const GENERALCAPS1_GLOBAL_RESET_MODE                  = 0x00100000 # Camera supports global reset rolling readout
const GENERALCAPS1_EXT_ACQUIRE                        = 0x00200000 # Camera supports extended acquire command
const GENERALCAPS1_FAN_LED_CONTROL                    = 0x00400000 # Camera supports fan and LED control command

const GENERALCAPS1_ROI_VERT_SYMM_TO_HORZ_AXIS         = 0x00800000 # Camera vert.ROI must be symmetrical to horizontal axis
const GENERALCAPS1_ROI_HORZ_SYMM_TO_VERT_AXIS         = 0x01000000 # Camera horz.ROI must be symmetrical to vertical axis

const GENERALCAPS1_COOLING_SETPOINTS                  = 0x02000000 # Camera has cooling setpoints instead of cooling range
const GENERALCAPS1_USER_INTERFACE                     = 0x04000000 # Camera has user interface commands

# const GENERALCAPS_ENHANCE_DESCRIPTOR_x                = 0x10000000 # reserved for future desc.
const GENERALCAPS1_ENHANCED_DESCRIPTOR_INTENSIFIED    = 0x20000000
const GENERALCAPS1_HW_IO_SIGNAL_DESCRIPTOR            = 0x40000000
const GENERALCAPS1_ENHANCED_DESCRIPTOR_2              = 0x80000000


# ----------------------------------------------------------------------
# dwGeneralCaps2 is for internal use only
# ----------------------------------------------------------------------


# ----------------------------------------------------------------------
# dwGeneralCaps3 member of the Camera Description structure
# ----------------------------------------------------------------------

const GENERALCAPS3_HDSDI_1G5                          = 0x00000001 # with HD/SDI interface, 1.5 GBit data rate
const GENERALCAPS3_HDSDI_3G                           = 0x00000002 # with HD/SDI interface, 3.0 GBit data rate
const GENERALCAPS3_IRIG_B_UNMODULATED                 = 0x00000004 # can evaluate an IRIG B unmodulated signal
const GENERALCAPS3_IRIG_B_MODULATED                   = 0x00000008 # can evaluate an IRIG B modulated signal
const GENERALCAPS3_CAMERA_SYNC                        = 0x00000010 # has camera sync mode implemented
const GENERALCAPS3_RESERVED0                          = 0x00000020 # reserved
const GENERALCAPS3_HS_READOUT_MODE                    = 0x00000040 # special fast sensor readout mode 
const GENERALCAPS3_EXT_SYNC_1HZ_MODE                  = 0x00000080 # in trigger mode external synchronized, multiples of 
                                                                   # 1 F/s can be set (until now: 100 Hz)


# ----------------------------------------------------------------------
# The list of masks for the return values of API.GetHealthStatus
# ----------------------------------------------------------------------

# Warning Bits
const WARNING_POWERSUPPLYVOLTAGERANGE  = 0x00000001
const WARNING_POWERSUPPLYTEMPERATURE   = 0x00000002
const WARNING_CAMERATEMPERATURE        = 0x00000004
const WARNING_SENSORTEMPERATURE        = 0x00000008
const WARNING_EXTERNAL_BATTERY_LOW     = 0x00000010
const WARNING_OFFSET_REGULATION_RANGE  = 0x00000020
const WARNING_CAMERARAM                = 0x00020000

# Error Bits
const ERROR_POWERSUPPLYVOLTAGERANGE    = 0x00000001
const ERROR_POWERSUPPLYTEMPERATURE     = 0x00000002
const ERROR_CAMERATEMPERATURE          = 0x00000004
const ERROR_SENSORTEMPERATURE          = 0x00000008
const ERROR_EXTERNAL_BATTERY_LOW       = 0x00000010
const ERROR_FIRMWARE_CORRUPTED         = 0x00000020
const ERROR_CAMERAINTERFACE            = 0x00010000
const ERROR_CAMERARAM                  = 0x00020000
const ERROR_CAMERAMAINBOARD            = 0x00040000
const ERROR_CAMERAHEADBOARD            = 0x00080000

# Status Bits
const STATUS_DEFAULT_STATE             = 0x00000001
const STATUS_SETTINGS_VALID            = 0x00000002
const STATUS_RECORDING_ON              = 0x00000004
const STATUS_READ_IMAGE_ON             = 0x00000008
const STATUS_FRAMERATE_VALID           = 0x00000010
const STATUS_SEQ_STOP_TRIGGERED        = 0x00000020
const STATUS_LOCKED_TO_EXTSYNC         = 0x00000040
const STATUS_EXT_BATTERY_AVAILABLE     = 0x00000080
const STATUS_IS_IN_POWERSAVE           = 0x00000100
const STATUS_POWERSAVE_LEFT            = 0x00000200
const STATUS_LOCKED_TO_IRIG            = 0x00000400
const STATUS_IS_IN_BOOTLOADER          = 0x80000000

const WARNING_BITS = [
    (WARNING_POWERSUPPLYVOLTAGERANGE, "Power supply voltage near limits"),
    (WARNING_POWERSUPPLYTEMPERATURE, "Power supply temperature near limit"),
    (WARNING_CAMERATEMPERATURE, "Camera temperature near limit (board temperature / FPGA temperature)"),
    (WARNING_SENSORTEMPERATURE, "Image sensor temperature near limit (for cooled camera versions only)"),
    (WARNING_EXTERNAL_BATTERY_LOW, "External battery nearly discharged"),
    (WARNING_OFFSET_REGULATION_RANGE, "Offset regulation range near limit"),
]
const ERROR_BITS = [
    (ERROR_POWERSUPPLYVOLTAGERANGE, "Power supply voltage out of limits"),
    (ERROR_POWERSUPPLYTEMPERATURE, "Power supply temperature out of limit"),
    (ERROR_CAMERATEMPERATURE, "Camera temperature out of limit (board temperature / FPGA temperature)"),
    (ERROR_SENSORTEMPERATURE, "Image sensor temperature out of limit (for cooled camera versions only)"),
    (ERROR_EXTERNAL_BATTERY_LOW, "External battery completely discharged"),
    (ERROR_CAMERAINTERFACE, "Camera interface failure"),
    (ERROR_CAMERARAM, "Camera RAM module failure"),
    (ERROR_CAMERAMAINBOARD, "Camera main board failure"),
    (ERROR_CAMERAHEADBOARD, "Camera head board failure"),
]
