module Kernel32

include("../typealias.jl")


const WAIT_OBJECT_0 = 0x00000000
const WAIT_TIMEOUT = 0x00000102
const WAIT_FAILED = 0xFFFFFFFF


function WaitForSingleObject(hHandle::HANDLE, dwMilliseconds::Integer)
    ccall((:WaitForSingleObject, "Kernel32"), DWORD, (HANDLE, DWORD),
          hHandle, dwMilliseconds)
end


function WaitForMultipleObjects(nCount::Integer, lpHandles::Vector{HANDLE},
                                fWaitAll::Integer, dwMilliseconds::Integer)
    ccall((:WaitForMultipleObjects, "Kernel32"), DWORD,
          (DWORD, Ref{HANDLE}, BOOL, DWORD),
          nCount, lpHandles, fWaitAll, dwMilliseconds)
end


end
