# Check the camera status and display the results
function healthcheck(cam::PcoCamera)
    pass = true

    temp = temperature(cam)
    body = "Camera=$(temp[:Camera]) ℃"
    ccd = haskey(temp, :CCD) ? ", CCD=$(temp[:CCD]) ℃" : ""
    @info "Temperature: $(body)$(ccd)"

    warning, error, stat = status(cam)

    # Status Bits
    if stat & API.STATUS_SETTINGS_VALID != 0
        @info "Settings are valid"
    else
        @warn "Settings were changed but not yet checked and accepted"
    end

    if stat & API.STATUS_RECORDING_ON != 0
        @debug "Recording state is on"
    else
        @debug "Recording state is off"
    end

    if stat & API.STATUS_READ_IMAGE_ON != 0
        @debug "Sensor data readout is running"
    else
        @debug "No sensor data readout at the moment"
    end

    if stat & API.STATUS_FRAMERATE_VALID != 0
        @debug "Valid image timing was set from PCO_SetFrameRate call"
    else
        @debug "Valid image timing was set from PCO_SetDelayExposureTime call"
    end

    if stat & API.STATUS_SEQ_STOP_TRIGGERED != 0
        @debug "A trigger signal for stopping the sequence has already arrived and the camera does capture the additional frames of the sequence"
    end

    if stat & API.STATUS_LOCKED_TO_EXTSYNC != 0
        @debug "The internal PLL is locked to the external sync signal"
    end

    if stat & API.STATUS_EXT_BATTERY_AVAILABLE != 0
        @debug "A rechargable battery pack is connected"
        if stat & API.STATUS_IS_IN_POWERSAVE
            @debug "Camera is in power save mode. Normal operation is not possible, but recorded image data is maintained as long as possible. To readout the data the camera must be connected to the normal power supply"
        end
    end

    if stat & API.STATUS_POWERSAVE_LEFT != 0
        @debug "Camera has been in power save mode and power was reconnected. Image data from last recording can be readout, but no other settings are valid"
    end

    if stat & API.STATUS_LOCKED_TO_IRIG != 0
        @debug "An IRIG time code signal is connected to the appropriate input and the camera is locked to this signal. Camera timestamp information (date and time) is adopted to the external time code"
    end

    # Warning Bits
    for (flag, msg) in API.WARNING_BITS
        if warning & flag != 0
            @warn msg
        end
    end

    # Error Bits
    for (flag, msg) in API.ERROR_BITS
        if error & flag != 0
            @error msg
            pass = false
        end
    end

    pass
end
