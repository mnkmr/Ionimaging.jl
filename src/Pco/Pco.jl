module Pco

export
    PcoCamera,
    PcoPixelfly,
    PcoUnknown,
    ImageBuffer,
    resetlib,
    status,
    arm!,
    imageparameters!,
    reset!,
    reboot!,
    size,
    sensorformat,
    sensorformat!,
    roi,
    roi!,
    binning,
    binning!,
    pixelrate,
    pixelrate!,
    conversionfactor,
    conversionfactor!,
    IRsensitivity,
    IRsensitivity!,
    delay_exposure,
    delay_exposure!,
    triggermode,
    triggermode!,
    trigger,
    recordingstate,
    recordingstate!,
    timestamp,
    timestamp!,
    bitalignment,
    bitalignment!,
    hotpixelcorrection,
    hotpixelcorrection!,
    allocate!,
    free!,
    getimage,
    add!,
    queue!,
    waitfor,
    cancel!,
    errortext,
    describe,
    healthcheck,
    initialize!,
    take!,
    take,
    atake,
    fetch!,
    fetch,
    count_available,
    count_queued,
    opencamera,
    describecamera,
    searchcamera

include("typealias.jl")
include("Api/Api.jl")           # API module
include("Kernel32/Kernel32.jl") # Kernel32 module
include("jlapi.jl")
include("edge.jl")
include("pixelfly.jl")
include("virtualcamera.jl")
include("unknowncamera.jl")

import .API
import Base: close


"""
    opencamera(interface::Integer=0xffff; kwargs...)

Open a camera and return the camera object. If interface is omitted, it will
scan all the possible interfaces to search a connected camera.

| #      | interface           |
|:------:|:-------------------:|
| 1      | FireWire            |
| 2      | Camera Link         |
| 3      | USB 2.0             |
| 4      | GigE                |
| 5      | Serial interface    |
| 6      | USB 3.0             |
| 7      | Camera Link HS      |
| 8      | CoaXPress           |
| 9      | USB 3.0 Gen1        |
| 0x1000 | Virtual Camera      |
| 0xffff | Scan all interfaces |

`kwargs` will be directly passed to [`initialize!`](@ref) inside this function.
See the help of [`initialize!`](@ref)
"""
function opencamera(interface::Integer=0xffff; kwargs...)
    if interface == API.PCO_INTERFACE_VIRTUAL
        handle, _ = _open_virtual()
        spec = _info_virtual()
    else
        handle, _ = _open(interface)
        spec = _info(handle)
    end

    # This block will be extended for future
    cam = if spec[:cameratype] == API.CAMERATYPE_PCO_USBPIXELFLY
        PcoPixelfly(handle, spec)
    elseif spec[:cameratype] == API.CAMERATYPE_PCO_EDGE ||
           spec[:cameratype] == API.CAMERATYPE_PCO_EDGE_42 ||
           spec[:cameratype] == API.CAMERATYPE_PCO_EDGE_GL ||
           spec[:cameratype] == API.CAMERATYPE_PCO_EDGE_USB3 ||
           spec[:cameratype] == API.CAMERATYPE_PCO_EDGE_HS ||
           spec[:cameratype] == API.CAMERATYPE_PCO_EDGE_MT
        PcoEdge(handle, spec)
    elseif spec[:cameratype] == API.CAMERATYPE_PCO_VIRTUAL
        PcoVirtual(handle, spec)
    else
        # Not yet implemented
        PcoUnknown(handle, spec)
    end

    try
        initialize!(cam; kwargs...)
    catch e
        close(cam)
        rethrow()
    end
    cam
end

function opencamera(f::Function, interface::Integer=0xffff; kwargs...)
    cam = opencamera(interface; kwargs...)
    try
        f(cam)
    finally
        close(cam)
    end
end


function close(cam::PcoCamera)
    _close(cam)
end


function describecamera(cam::PcoCamera, outline=(:General, :Prameters, :Features))
    describe(cam, outline)
end

function describecamera(interface::Integer=0xffff,
                        outline=(:General, :Prameters, :Features); kwargs...)
    opencamera(interface; kwargs...) do cam
        describe(cam, outline)
    end
end


function searchcamera(interface::Integer=0xffff)
    if interface == API.PCO_INTERFACE_VIRTUAL
        handle, openstruct = _open_virtual()
        spec = _info_virtual()
    else
        handle, openstruct = _open(interface)
        spec = _info(handle)
        _close(handle)
    end
    interface = openstruct.wInterfaceType
    spec, interface
end


end # module
