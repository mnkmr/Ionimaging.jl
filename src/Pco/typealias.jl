const VOID = Cvoid
const HANDLE = Ptr{VOID}
const SHORT = Cshort
const INT = Cint
const LONG = Clong
const WORD = Cushort
const DWORD = Culong
const BYTE = Cuchar
const BOOL = Cint
