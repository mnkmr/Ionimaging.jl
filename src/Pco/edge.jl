struct PcoEdge <:PcoCamera
    handle::HANDLE
    spec::Dict{Symbol,Any}
    bufmax::Int
    buffer::Dict{Int,ImageBuffer}
    queue::Vector{Int}
    available::Vector{Int}

    function PcoEdge(handle, spec)
        buffer = Dict{Int,ImageBuffer}()
        queue = Vector{Int}()
        available = Vector{Int}()
        new(handle, spec, 16, buffer, queue, available)
    end
end


function roi!(cam::PcoEdge, y0, x0, y1, x1)
    @rccheck API.SetROI(cam.handle, x0, y0, x1, y1)
end
