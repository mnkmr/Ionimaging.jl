# The camera unimplemented

struct PcoUnknown <:PcoCamera
    handle::HANDLE
    spec::Dict{Symbol,Any}
    bufmax::Int
    buffer::Dict{Int,ImageBuffer}
    queue::Vector{Int}
    available::Vector{Int}

    function PcoUnknown(handle, spec)
        buffer = Dict{Int,ImageBuffer}()
        queue = Vector{Int}()
        available = Vector{Int}()
        new(handle, spec, 16, buffer, queue, available)
    end
end
