struct Openstruct
    wSize::WORD
    wInterfaceType::WORD
    wCameraNumber::WORD
    wCameraNumAtInterface::WORD
    wOpenFlags::NTuple{10,WORD}
    dwOpenFlags::NTuple{5,DWORD}
    wOpenPtr::NTuple{6,Ptr{Cvoid}}
    zzwDummy::NTuple{8,WORD}

    function Openstruct(wInterfaceType, wCameraNumber, wOpenFlags, dwOpenFlags,
                        wOpenPtr)
        new(SIZE_OPENSTRUCT, wInterfaceType, wCameraNumber, 0, wOpenFlags,
            dwOpenFlags, wOpenPtr, Tuple(zeros(WORD, 8)))
    end
end

function Openstruct(;wInterfaceType=0, wCameraNumber=0,
                    wOpenFlags=Tuple(zeros(WORD, 10)),
                    dwOpenFlags=Tuple(zeros(DWORD, 5)),
                    wOpenPtr=Tuple([Ptr{Cvoid}(0) for x in 1:6]))
    Openstruct(wInterfaceType, wCameraNumber, wOpenFlags, dwOpenFlags,
               wOpenPtr)
end

const SIZE_OPENSTRUCT = sizeof(Openstruct)


struct Description
    wSize::WORD
    wSensorTypeDESC::WORD
    wSensorSubTypeDESC::WORD
    wMaxHorzResStdDESC::WORD
    wMaxVertResStdDESC::WORD
    wMaxHorzResExtDESC::WORD
    wMaxVertResExtDESC::WORD
    wDynResDESC::WORD
    wMaxBinHorzDESC::WORD
    wBinHorzSteppingDESC::WORD
    wMaxBinVertDESC::WORD
    wBinVertSteppingDESC::WORD
    wRoiHorStepsDESC::WORD
    wRoiVertStepsDESC::WORD
    wNumADCsDESC::WORD
    wMinSizeHorzDESC::WORD
    dwPixelRateDESC::NTuple{4,DWORD}
    ZZdwDummypr::NTuple{20,DWORD}
    wConvFactDESC::NTuple{4,WORD}
    sCoolingSetpoints::NTuple{10,SHORT}
    ZZwDummycv::NTuple{8,WORD}
    wSoftRoiHorStepsDESC::WORD
    wSoftRoiVertStepsDESC::WORD
    wIRDESC::WORD
    wMinSizeVertDESC::WORD
    dwMinDelayDESC::DWORD
    dwMaxDelayDESC::DWORD
    dwMinDelayStepDESC::DWORD
    dwMinExposureDESC::DWORD
    dwMaxExposureDESC::DWORD
    dwMinExposureStepDESC::DWORD
    dwMinDelayIRDESC::DWORD
    dwMaxDelayIRDESC::DWORD
    dwMinExposureIRDESC::DWORD
    dwMaxExposureIRDESC::DWORD
    wTimeTableDESC::WORD
    wDoubleImageDESC::WORD
    sMinCoolSetDESC::SHORT
    sMaxCoolSetDESC::SHORT
    sDefaultCoolSetDESC::SHORT
    wPowerDownModeDESC::WORD
    wOffsetRegulationDESC::WORD
    wColorPatternDESC::WORD
    wPatternTypeDESC::WORD
    wDummy1::WORD
    wDummy2::WORD
    wNumCoolingSetpoints::WORD
    dwGeneralCapsDESC1::DWORD
    dwGeneralCapsDESC2::DWORD
    dwExtSyncFrequency::NTuple{4,DWORD}
    dwGeneralCapsDESC3::DWORD
    dwGeneralCapsDESC4::DWORD
    ZZdwDummy::NTuple{40,DWORD}

    function Description()
        new(SIZE_DESCRIPTION_STRUCTURE, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
            0, 0, Tuple(zeros(DWORD, 4)), Tuple(zeros(DWORD, 20)),
            Tuple(zeros(WORD, 4)), Tuple(zeros(SHORT, 10)),
            Tuple(zeros(WORD, 8)), 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
            0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, Tuple(zeros(DWORD,4)), 0, 0,
            Tuple(zeros(DWORD,40)))
    end
end

const SIZE_DESCRIPTION_STRUCTURE = sizeof(Description)


struct Description2
    wSize::WORD
    ZZwAlignDummy1::WORD
    dwMinPeriodicalTimeDESC2::DWORD
    dwMaxPeriodicalTimeDESC2::DWORD
    dwMinPeriodicalConditionDESC2::DWORD
    dwMaxNumberOfExposuresDESC2::DWORD
    lMinMonitorSignalOffsetDESC2::LONG
    dwMaxMonitorSignalOffsetDESC2::DWORD
    dwMinPeriodicalStepDESC2::DWORD
    dwStartTimeDelayDESC2::DWORD
    dwMinMonitorStepDESC2::DWORD
    dwMinDelayModDESC2::DWORD
    dwMaxDelayModDESC2::DWORD
    dwMinDelayStepModDESC2::DWORD
    dwMinExposureModDESC2::DWORD
    dwMaxExposureModDESC2::DWORD
    dwMinExposureStepModDESC2::DWORD
    dwModulateCapsDESC2::DWORD
    dwReserved::NTuple{16,DWORD}
    ZZdwDummy::NTuple{41,DWORD}

    function Description2()
        new(SIZE_DESCRIPTION2_STRUCTURE, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
            0, 0, 0, 0, Tuple(zeros(DWORD,16)), Tuple(zeros(DWORD,41)))
    end
end

const SIZE_DESCRIPTION2_STRUCTURE = sizeof(Description2)


struct SC2_Hardware_DESC
    szName::NTuple{16,Cchar}
    wBatchNo::WORD
    wRevision::WORD
    wVariant::WORD
    ZZwDummy::NTuple{20,WORD}

    function SC2_Hardware_DESC()
        new(Tuple(zeros(Cchar,16)), 0, 0, 0, Tuple(zeros(WORD,20)))
    end
end


struct SC2_Firmware_DESC
    szName::NTuple{16,Cchar}
    bMinorRev::BYTE
    bMajorRev::BYTE
    wVariant::WORD
    ZZwDummy::NTuple{22,WORD}

    function SC2_Firmware_DESC()
        new(Tuple(zeros(Cchar,16)), 0, 0, 0, Tuple(zeros(WORD,22)))
    end
end


const MAXVERSIONHW = 10
struct HW_Vers
    BoardNum::WORD
    Board::NTuple{MAXVERSIONHW,SC2_Hardware_DESC}

    function HW_Vers()
        new(0, Tuple(map(i -> SC2_Hardware_DESC(), 1:MAXVERSIONHW)))
    end
end


const MAXVERSIONFW = 10
struct FW_Vers
    DeviceNum::WORD
    Device::NTuple{MAXVERSIONFW,SC2_Firmware_DESC}

    function FW_Vers()
        new(0, Tuple(map(i -> SC2_Firmware_DESC(), 1:MAXVERSIONFW)))
    end
end


struct CameraType
    wSize::WORD
    wCamType::WORD
    wCamSubType::WORD
    ZZwAlignDummy1::WORD
    dwSerialNumber::DWORD
    dwHWVersion::DWORD
    dwFWVersion::DWORD
    wInterfaceType::WORD
    strHardwareVersion::HW_Vers
    strFirmwareVersion::FW_Vers
    ZZwDummy::NTuple{39,WORD}

    function CameraType()
        new(SIZE_CAMERATYPE, 0, 0, 0, 0, 0, 0, 0, HW_Vers(), FW_Vers(),
            Tuple(zeros(WORD,39)))
    end
end

const SIZE_CAMERATYPE = sizeof(CameraType)



struct General
    wSize::WORD
    ZZwAlignDummy1::WORD
    strCamType::CameraType
    dwCamHealthWarnings::DWORD
    dwCamHealthErrors::DWORD
    dwCamHealthStatus::DWORD
    sCCDTemperature::SHORT
    sCamTemperature::SHORT
    sPowerSupplyTemperature::SHORT
    ZZwDummy::NTuple{37,WORD}

    function General()
        new(SIZE_GENERAL, 0, CameraType(), 0, 0, 0, 0, 0, 0,
            Tuple(zeros(WORD,37)))
    end
end

const SIZE_GENERAL = sizeof(General)


struct Buflist
    sBufNr::SHORT
    ZZwAlignDummy::WORD
    dwStatusDll::DWORD
    dwStatusDrv::DWORD

    function Buflist(bufnr)
        new(bufnr, 0, 0, 0)
    end
end
