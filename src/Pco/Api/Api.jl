# The list of native dll function (SC_Cam.dll) calls
module API

include("../typealias.jl")
include("constants.jl")
include("finddll.jl")
include("structs.jl")

import Libdl: dlopen, dlsym

const DLL = Ref{Ptr{Cvoid}}(0)
const DLL_FOUND = Ref{Bool}(false)
function __init__()
    dllpath = finddll()
    if !isfile(dllpath)
        @warn "PCO.API: SC_Cam.dll was not found"
        return
    end
    @debug "'$(dllpath)' is loaded"
    DLL_FOUND[] = true
    DLL[] = dlopen(dllpath)
end


# ----------------------------------------------------------------------
#    CAMERA ACCESS
# ----------------------------------------------------------------------

function OpenCamera(ph, wCamNum::Integer)
    F = dlsym(DLL[], :PCO_OpenCamera)
    ccall(F, Cuint, (Ref{HANDLE}, WORD), ph, wCamNum)
end


function OpenCameraEx(ph, strOpenStruct)
    F = dlsym(DLL[], :PCO_OpenCameraEx)
    ccall(F, Cuint, (Ref{HANDLE}, Ref{Openstruct}), ph, strOpenStruct)
end


function CloseCamera(ph::HANDLE)
    F = dlsym(DLL[], :PCO_CloseCamera)
    ccall(F, Cuint, (HANDLE,), ph)
end


function ResetLib()
    F = dlsym(DLL[], :PCO_ResetLib)
    ccall(F, Cuint, ())
end


function CheckDeviceAvailability(ph::HANDLE, wNumIf::Integer)
    F = dlsym(DLL[], :PCO_CheckDeviceAvailability)
    ccall(F, Cuint, (HANDLE, WORD), ph, wNumIf)
end


# ----------------------------------------------------------------------
#    CAMERA DESCRIPTION
# ----------------------------------------------------------------------

function GetCameraDescription(ph::HANDLE, strDescription)
    F = dlsym(DLL[], :PCO_GetCameraDescription)
    ccall(F, Cuint, (HANDLE, Ref{Description}), ph, strDescription)
end


function GetCameraDescriptionEx(ph::HANDLE, strDescription, wType::Integer)
    F = dlsym(DLL[], :PCO_GetCameraDescriptionEx)
    ccall(F, Cuint, (HANDLE, Ref{Description}, WORD), ph, strDescription, wType)
end


# ----------------------------------------------------------------------
#    GENERAL CAMERA STATUS
# ----------------------------------------------------------------------

function GetGeneral(ph::HANDLE, strGeneral)
    F = dlsym(DLL[], :PCO_GetGeneral)
    ccall(F, Cuint, (HANDLE, Ref{Description}), ph, strGeneral)
end


function GetCameraType(ph::HANDLE, strCamType)
    F = dlsym(DLL[], :PCO_GetCameraType)
    ccall(F, Cuint, (HANDLE, Ref{CameraType}), ph, strCamType)
end


function GetCameraHealthStatus(ph::HANDLE, dwWarn, dwErr, dwStatus)
    F = dlsym(DLL[], :PCO_GetCameraHealthStatus)
    ccall(F, Cuint, (HANDLE, Ref{DWORD}, Ref{DWORD}, Ref{DWORD}),
          ph, dwWarn, dwErr, dwStatus)
end


function GetTemperature(ph::HANDLE, sCCDTemp, sCamTemp, sPowTemp)
    F = dlsym(DLL[], :PCO_GetTemperature)
    ccall(F, Cuint, (HANDLE, Ref{SHORT}, Ref{SHORT}, Ref{SHORT}),
          ph, sCCDTemp, sCamTemp, sPowTemp)
end


const MAX_INFOSTRING_LEN = 500

function GetInfoString(ph::HANDLE, dwinfotype::Integer, buf, size_in::Integer)
    F = dlsym(DLL[], :PCO_GetInfoString)
    ccall(F, Cuint, (HANDLE, DWORD, Ref{Cchar}, WORD),
          ph, dwinfotype, buf, size_in)
end


function GetCameraName(ph::HANDLE, szCameraName, wSZCameraNameLen::Integer)
    F = dlsym(DLL[], :PCO_GetCameraName)
    ccall(F, Cuint, (HANDLE, Ref{Cchar}, WORD),
          ph, szCameraName, wSZCameraNameLen)
end


function GetFirmwareInfo(ph::HANDLE, wDeviceBlock::Integer, pstrFirmWareVersion)
    F = dlsym(DLL[], :PCO_GetFirmwareInfo)
    ccall(F, Cuint, (HANDLE, WORD, Ref{FW_Vers}),
          ph, wDeviceBlock, pstrFirmWareVersion)
end


function GetColorCorrectionMatrix(ph::HANDLE, pdMatrix)
    F = dlsym(DLL[], :PCO_GetColorCorrectionMatrix)
    ccall(F, Cuint, (HANDLE, Ref{Cdouble}), ph, pdMatrix)
end


# ----------------------------------------------------------------------
#    GENERAL CAMERA CONTROL
# ----------------------------------------------------------------------

function ArmCamera(ph::HANDLE)
    F = dlsym(DLL[], :PCO_ArmCamera)
    ccall(F, Cuint, (HANDLE,), ph)
end


function SetImageParameters(ph::HANDLE, wxres::Integer, wyres::Integer,
                            dwFlags::Integer, param, ilen)
    F = dlsym(DLL[], :PCO_SetImageParameters)
    ccall(F, Cuint, (HANDLE, WORD, WORD, DWORD, Ref{Cvoid}, Cint),
          ph, wxres, wyres, dwFlags, param, ilen)
end


function ResetSettingsToDefault(ph::HANDLE)
    F = dlsym(DLL[], :PCO_ResetSettingsToDefault)
    ccall(F, Cuint, (HANDLE,), ph)
end


function SetTimeouts(ph::HANDLE, buf_in, size_in::Integer)
    F = dlsym(DLL[], :PCO_SetTimeouts)
    ccall(F, Cuint, (HANDLE, Ref{Cvoid}, Cuint), ph, buf_in, size_in)
end


function RebootCamera(ph::HANDLE)
    F = dlsym(DLL[], :PCO_RebootCamera)
    ccall(F, Cuint, (HANDLE,), ph)
end


function GetCameraSetup(ph::HANDLE, wType, dwSetup, wLen::Array{WORD})
    F = dlsym(DLL[], :PCO_GetCameraSetup)
    ccall(F, Cuint, (HANDLE, Ref{WORD}, Ref{DWORD}, Ref{WORD}),
          ph, wType, dwSetup, wLen)
end


# ----------------------------------------------------------------------
#    IMAGE SENSOR
# ----------------------------------------------------------------------

function GetSizes(ph::HANDLE, wXResAct, wYResAct, wXResMax, wYResMax)
    F = dlsym(DLL[], :PCO_GetSizes)
    ccall(F, Cuint, (HANDLE, Ref{WORD}, Ref{WORD}, Ref{WORD}, Ref{WORD}),
          ph, wXResAct, wYResAct, wXResMax, wYResMax)
end


function GetSensorFormat(ph::HANDLE, wSensor)
    F = dlsym(DLL[], :PCO_GetSensorFormat)
    ccall(F, Cuint, (HANDLE, Ref{WORD}), ph, wSensor)
end


function SetSensorFormat(ph::HANDLE, wSensor::Integer)
    F = dlsym(DLL[], :PCO_SetSensorFormat)
    ccall(F, Cuint, (HANDLE, WORD), ph, wSensor)
end


function GetROI(ph::HANDLE, wRoiX0, wRoiY0, wRoiX1, wRoiY1)
    F = dlsym(DLL[], :PCO_GetROI)
    ccall(F, Cuint, (HANDLE, Ref{WORD}, Ref{WORD}, Ref{WORD}, Ref{WORD}),
          ph, wRoiX0, wRoiY0, wRoiX1, wRoiY1)
end


function SetROI(ph::HANDLE, wRoiX0::Integer, wRoiY0::Integer, wRoiX1::Integer,
                wRoiY1::Integer)
    F = dlsym(DLL[], :PCO_SetROI)
    ccall(F, Cuint, (HANDLE, WORD, WORD, WORD, WORD),
          ph, wRoiX0, wRoiY0, wRoiX1, wRoiY1)
end


function GetBinning(ph::HANDLE, wBinHorz, wBinVert)
    F = dlsym(DLL[], :PCO_GetBinning)
    ccall(F, Cuint, (HANDLE, Ref{WORD}, Ref{WORD}), ph, wBinHorz, wBinVert)
end


function SetBinning(ph::HANDLE, wBinHorz::Integer, wBinVert::Integer)
    F = dlsym(DLL[], :PCO_SetBinning)
    ccall(F, Cuint, (HANDLE, WORD, WORD), ph, wBinHorz, wBinVert)
end


function GetPixelRate(ph::HANDLE, dwPixelRate)
    F = dlsym(DLL[], :PCO_GetPixelRate)
    ccall(F, Cuint, (HANDLE, Ref{DWORD}), ph, dwPixelRate)
end


function SetPixelRate(ph::HANDLE, dwPixelRate::Integer)
    F = dlsym(DLL[], :PCO_SetPixelRate)
    ccall(F, Cuint, (HANDLE, DWORD), ph, dwPixelRate)
end


function GetConversionFactor(ph::HANDLE, wConvFact)
    F = dlsym(DLL[], :PCO_GetConversionFactor)
    ccall(F, Cuint, (HANDLE, Ref{WORD}), ph, wConvFact)
end


function SetConversionFactor(ph::HANDLE, wConvFact::Integer)
    F = dlsym(DLL[], :PCO_SetConversionFactor)
    ccall(F, Cuint, (HANDLE, WORD), ph, wConvFact)
end


function GetIRSensitivity(ph::HANDLE, wIR)
    F = dlsym(DLL[], :PCO_GetIRSensitivity)
    ccall(F, Cuint, (HANDLE, Ref{WORD}), ph, wIR)
end


function SetIRSensitivity(ph::HANDLE, wIR::Integer)
    F = dlsym(DLL[], :PCO_SetIRSensitivity)
    ccall(F, Cuint, (HANDLE, WORD), ph, wIR)
end


# ----------------------------------------------------------------------
#    TIMING CONTROL
# ----------------------------------------------------------------------

function GetDelayExposureTime(ph::HANDLE, dwDelay, dwExposure, wTimeBaseDelay,
                              wTimeBaseExposure)
    F = dlsym(DLL[], :PCO_GetDelayExposureTime)
    ccall(F, Cuint, (HANDLE, Ref{DWORD}, Ref{DWORD}, Ref{WORD}, Ref{WORD}),
          ph, dwDelay, dwExposure, wTimeBaseDelay, wTimeBaseExposure)
end


function SetDelayExposureTime(ph::HANDLE, dwDelay::Integer,
                              dwExposure::Integer, wTimeBaseDelay::Integer,
                              wTimeBaseExposure::Integer)
    F = dlsym(DLL[], :PCO_SetDelayExposureTime)
    ccall(F, Cuint, (HANDLE, DWORD, DWORD, WORD, WORD),
          ph, dwDelay, dwExposure, wTimeBaseDelay, wTimeBaseExposure)
end


function GetTriggerMode(ph::HANDLE, wTriggerMode)
    F = dlsym(DLL[], :PCO_GetTriggerMode)
    ccall(F, Cuint, (HANDLE, Ref{WORD}), ph, wTriggerMode)
end


function SetTriggerMode(ph::HANDLE, wTriggerMode::Integer)
    F = dlsym(DLL[], :PCO_SetTriggerMode)
    ccall(F, Cuint, (HANDLE, WORD), ph, wTriggerMode)
end


function ForceTrigger(ph::HANDLE, wTriggered)
    F = dlsym(DLL[], :PCO_ForceTrigger)
    ccall(F, Cuint, (HANDLE, Ref{WORD}), ph, wTriggered)
end


# ----------------------------------------------------------------------
#    RECORDING CONTROL
# ----------------------------------------------------------------------

function GetRecordingState(ph::HANDLE, wRecState)
    F = dlsym(DLL[], :PCO_GetRecordingState)
    ccall(F, Cuint, (HANDLE, Ref{WORD}), ph, wRecState)
end


function SetRecordingState(ph::HANDLE, wRecState::Integer)
    F = dlsym(DLL[], :PCO_SetRecordingState)
    ccall(F, Cuint, (HANDLE, WORD), ph, wRecState)
end


function GetTimestampMode(ph::HANDLE, wTimeStampMode)
    F = dlsym(DLL[], :PCO_GetTimestampMode)
    ccall(F, Cuint, (HANDLE, Ref{WORD}), ph, wTimeStampMode)
end


function SetTimestampMode(ph::HANDLE, wTimeStampMode::Integer)
    F = dlsym(DLL[], :PCO_SetTimestampMode)
    ccall(F, Cuint, (HANDLE, WORD), ph, wTimeStampMode)
end


# ----------------------------------------------------------------------
#    IMAGE INFORMATION
# ----------------------------------------------------------------------

function GetBitAlignment(ph::HANDLE, wBitAlignment)
    F = dlsym(DLL[], :PCO_GetBitAlignment)
    ccall(F, Cuint, (HANDLE, Ref{WORD}), ph, wBitAlignment)
end


function SetBitAlignment(ph::HANDLE, wBitAlignment::Integer)
    F = dlsym(DLL[], :PCO_SetBitAlignment)
    ccall(F, Cuint, (HANDLE, WORD), ph, wBitAlignment)
end


function GetHotPixelCorrectionMode(ph::HANDLE, wHotPixelCorrectionMode)
    F = dlsym(DLL[], :PCO_GetHotPixelCorrectionMode)
    ccall(F, Cuint, (HANDLE, Ref{WORD}), ph, wHotPixelCorrectionMode)
end


function SetHotPixelCorrectionMode(ph::HANDLE, wHotPixelCorrectionMode::Integer)
    F = dlsym(DLL[], :PCO_SetHotPixelCorrectionMode)
    ccall(F, Cuint, (HANDLE, WORD), ph, wHotPixelCorrectionMode)
end


# ----------------------------------------------------------------------
#    BUFFER MANAGEMENT
# ----------------------------------------------------------------------

function AllocateBuffer(ph::HANDLE, sBufNr, dwSize::Integer, wBuf, hEvent)
    F = dlsym(DLL[], :PCO_AllocateBuffer)
    ccall(F, Cuint, (HANDLE, Ref{SHORT}, DWORD, Ptr{Ptr{WORD}}, Ref{HANDLE}),
          ph, sBufNr, dwSize, wBuf, hEvent)
end


function FreeBuffer(ph::HANDLE, sBufNr::Integer)
    F = dlsym(DLL[], :PCO_FreeBuffer)
    ccall(F, Cuint, (HANDLE, SHORT), ph, sBufNr)
end


function GetBufferStatus(ph::HANDLE, sBufNr::Integer, dwStatusDLL, dwStatusDrv)
    F = dlsym(DLL[], :PCO_GetBufferStatus)
    ccall(F, Cuint, (HANDLE, SHORT, Ref{DWORD}, Ref{DWORD}),
          ph, sBufNr, dwStatusDLL, dwStatusDrv)
end


# ----------------------------------------------------------------------
#    IMAGE ACQUISITION
# ----------------------------------------------------------------------

function GetImageEx(ph::HANDLE, wSegment::Integer, dw1stImage::Integer,
                    dwLastImage::Integer, sBufNr::Integer, wXRes::Integer,
                    wYRes::Integer, wBitPerPixel::Integer)
    F = dlsym(DLL[], :PCO_GetImageEx)
    ccall(F, Cuint, (HANDLE, WORD, DWORD, DWORD, SHORT, WORD, WORD, WORD),
          ph, wSegment, dw1stImage, dwLastImage, sBufNr, wXRes, wYRes,
          wBitPerPixel)
end


function AddBufferEx(ph::HANDLE, dw1stImage::Integer, dwLastImage::Integer,
                     sBufNr::Integer, wXRes::Integer, wYRes::Integer,
                     wBitPerPixel::Integer)
    F = dlsym(DLL[], :PCO_AddBufferEx)
    ccall(F, Cuint, (HANDLE, DWORD, DWORD, SHORT, WORD, WORD, WORD),
          ph, dw1stImage, dwLastImage, sBufNr, wXRes, wYRes, wBitPerPixel)
end


function CancelImages(ph::HANDLE)
    F = dlsym(DLL[], :PCO_CancelImages)
    ccall(F, Cuint, (HANDLE,), ph)
end


function GetPendingBuffer(ph::HANDLE)
    n = Vector{Cint}(undef, 1)
    F = dlsym(DLL[], :PCO_GetPendingBuffer)
    ccall(F, Cuint, (HANDLE, Ref{Cint}), ph, n)
end


function WaitforBuffer(ph::HANDLE, nr_of_buffer::Integer, bl, timeout::Integer)
    F = dlsym(DLL[], :PCO_WaitforBuffer)
    ccall(F, Cuint, (HANDLE, INT, Ref{Buflist}, Cint),
          ph, nr_of_buffer, bl, timeout)
end


# ----------------------------------------------------------------------
#    DEBUG
# ----------------------------------------------------------------------

function GetErrorTextSDK(dwError::DWORD, pszErrorString,
                         dwErrorStringLength::Integer)
    F = dlsym(DLL[], :PCO_GetErrorTextSDK)
    ccall(F, Cvoid, (DWORD, Ref{Cchar}, DWORD),
          dwError, pszErrorString, dwErrorStringLength)
end


end
