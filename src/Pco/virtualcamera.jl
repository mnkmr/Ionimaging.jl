# The camera for development without real cameras
# TODO: check option values
# TODO: implement trigger(cam::PcoVirtual)

struct PcoVirtual <:PcoCamera
    handle::HANDLE
    spec::Dict{Symbol,Any}
    bufmax::Int
    buffer::Dict{Int,ImageBuffer}
    queue::Vector{Int}
    available::Vector{Int}
    status::Dict{Symbol,Any}
    generate::Function

    function PcoVirtual(handle, spec)
        buffer = Dict{Int,ImageBuffer}()
        queue = Vector{Int}()
        available = Vector{Int}()
        status = copy(PCOVIRTUAL_DEFAULT_PARAMETERS)
        status[:resolution] = (PCOVIRTUAL_DEFAULT_RESOLUTION[][1],
                               PCOVIRTUAL_DEFAULT_RESOLUTION[][2])
        status[:roi] = (1, 1, status[:resolution][1], status[:resolution][2])
        generate = PCOVIRTUAL_DEFAULT_IMAGEGENERATOR[]
        new(handle, spec, 16, buffer, queue, available, status, generate)
    end
end


const PCOVIRTUAL_DEFAULT_RESOLUTION = Ref{NTuple{2,Int}}((1392, 1080))
const PCOVIRTUAL_DEFAULT_PARAMETERS = Dict{Symbol,Any}(
    :recordingstate => 0,
    :resolution => (1392, 1080),
    :SoftRoiActionBit => 0x00,
    :sensorformat => 0,
    :roi => (1, 1, 1392, 1080),
    :binning => (1, 1),
    :pixelrate => 12000000,
    :convfactor => 100,
    :IRsensitivity => 0,
    :delay => 0,
    :exposure => 0.001,
    :triggermode => 0,
    :recordingstate => 0,
    :timestamp => 0,
    :bitalignment => 0,
    :hotpixelcorrection => 0)
const PCOVIRTUAL_DEFAULT_IMAGEGENERATOR = Ref{Function}((w, h) -> rand(UInt16.(0:255), (w, h)))


# ----------------------------------------------------------------------
#    CAMERA ACCESS
# ----------------------------------------------------------------------

function _open_virtual()
    ph = HANDLE(0)
    openstruct = API.Openstruct(wInterfaceType=API.CAMERATYPE_PCO_VIRTUAL)
    ph, openstruct
end


function _close(cam::PcoVirtual) end


# ----------------------------------------------------------------------
#    CAMERA DESCRIPTION & GENERAL CAMERA STATUS
# ----------------------------------------------------------------------

function _info_virtual()
    Dict{Symbol,Any}(
        :camera => "virtual camera",
        :sensor => "virtual sensor",
        :cameratype => API.CAMERATYPE_PCO_VIRTUAL,
        :serialnumber => 0,
        :interface => "virtual interface",
        :interfacetype => API.INTERFACE_VIRTUAL,
        :hwversions => [],
        :fwversions => [],
        :sensortype => 0,
        :resolution => (2048, 2048),
        :resolution_ex => (600, 800),
        :depth => 14,
        :hbins => [1, 2, 3, 4],
        :vbins => [1, 2, 3, 4],
        :ROI_hstep => 0,
        :ROI_vstep => 0,
        :softROI_hstep => 0,
        :softROI_vstep => 0,
        :ADC => 1,
        :hmin => 64,
        :vmin => 64,
        :pixelrate => [12000000, 24000000],
        :convfactor => [100, 150],
        :IR => 0,
        :delaymin => 0.0,
        :delaymax => 0.0,
        :delaystep => 0.0,
        :exposuremin => 1e-6,
        :exposuremax => 1.0,
        :exposurestep => 1e-6,
        :delaymin_IR => 0.0,
        :delaymax_IR => 0.0,
        :exposuremin_IR => 1e-6,
        :exposuremax_IR => 1.0,
        :timetable => 0,
        :doubleimage => 0,
        :coolsetnum => 0,
        :coolsetpoints => [],
        :coolsetdef => 0,
        :coolsetmin => 0,
        :coolsetmax => 0,
        :powerdownmode => 0,
        :offsetregulation => 0,
        :colorpat => 0,
        :colorpattype => 0,
        :extfrequency => [],
        :GeneralCaps1 => Dict{Symbol,Bool}(),
        :GeneralCaps3 => Dict{Symbol,Bool}())
end


function status(cam::PcoVirtual)
    DWORD(0), DWORD(0), DWORD(0)
end


function temperature(cam::PcoVirtual)
    temp = Dict{Symbol,Float64}()
    temp[:Camera] = 38.0
    temp
end


# ----------------------------------------------------------------------
#    GENERAL CAMERA CONTROL
# ----------------------------------------------------------------------

function arm!(cam::PcoVirtual) end


function imageparameters!(cam::PcoVirtual, height::Integer, width::Integer,
                          flags=0x00)
    (current_height, current_width) = cam.status[:resolution]
    if current_height != height
        msg = "imageparameters!: height mismatch: current $(current_height) <=> assigned $(height)"
        throw(CameraError(msg))
    end
    if current_width != width
        msg = "imageparameters!: width mismatch: current $(current_width) <=> assigned $(width)"
        throw(CameraError(msg))
    end
    cam.status[:SoftRoiActionBit] = flags
end


function reset!(cam::PcoVirtual)
    for k in keys(PCOVIRTUAL_DEFAULT_PARAMETERS)
        cam.status[k] = PCOVIRTUAL_DEFAULT_PARAMETERS[k]
    end
    cam.status[:resolution] = (PCOVIRTUAL_DEFAULT_RESOLUTION[][1],
                               PCOVIRTUAL_DEFAULT_RESOLUTION[][2])
    cam.status[:roi] = (1, 1, cam.status[:resolution][1], cam.status[:resolution][2])
end


function reboot!(cam::PcoVirtual) end


# ----------------------------------------------------------------------
#    IMAGE SENSOR
# ----------------------------------------------------------------------

size(cam::PcoVirtual) = cam.status[:resolution]
sensorformat(cam::PcoVirtual) = cam.status[:sensorformat]
roi(cam::PcoVirtual) = cam.status[:roi]
binning(cam::PcoVirtual) = cam.status[:binning]
pixelrate(cam::PcoVirtual) = cam.status[:pixelrate]
conversionfactor(cam::PcoVirtual) = cam.status[:convfactor]
IRsensitivity(cam::PcoVirtual) = cam.status[:IRsensitivity]


function sensorformat!(cam::PcoVirtual, sf::Integer)
    if sf != 0 && sf != 1
        throw(CameraError("sensorformat: invalid argument: $(sf)"))
    end
    cam.status[:sensorformat] = Int(sf)
    nothing
end


function roi!(cam::PcoVirtual, y0, x0, y1, x1)
    cam.status[:sensorformat] = map(Int, (y0, x0, y1, x1))
    nothing
end


function binning!(cam::PcoVirtual, hbin, vbin)
    cam.status[:binning] = map(Int, (hbin, vbin))
    nothing
end


function pixelrate!(cam::PcoVirtual, rate)
    cam.status[:pixelrate] = Int(rate)
    nothing
end


function conversionfactor!(cam::PcoVirtual, c)
    cam.status[:convfactor] = Int(c)
    nothing
end


function IRsensitivity!(cam::PcoVirtual, ir::Integer)
    cam.status[:IRsensitivity] = Int(ir)
    nothing
end


# ----------------------------------------------------------------------
#    TIMING CONTROL
# ----------------------------------------------------------------------

delay_exposure(cam::PcoVirtual) = (cam.status[:delay], cam.status[:exposure])
triggermode(cam::PcoVirtual) = cam.status[:triggermode]


function delay_exposure!(cam::PcoVirtual, d, e)
    cam.status[:delay] = Float64(d)
    cam.status[:exposure] = Float64(e)
    nothing
end


function triggermode!(cam::PcoVirtual, mode::Integer)
    cam.status[:triggermode] = Int(mode)
    nothing
end


# ----------------------------------------------------------------------
#    RECORDING CONTROL
# ----------------------------------------------------------------------

recordingstate(cam::PcoVirtual) = cam.status[:recordingstate]
timestamp(cam::PcoVirtual) = cam.status[:timestamp]


function recordingstate!(cam::PcoVirtual, state::Integer)
    cam.status[:recordingstate] = Int(state)
    nothing
end


function timestamp!(cam::PcoVirtual, mode::Integer)
    cam.status[:timestamp] = Int(mode)
    nothing
end


# ----------------------------------------------------------------------
#    IMAGE INFORMATION
# ----------------------------------------------------------------------

bitalignment(cam::PcoVirtual) = cam.status[:bitalignment]
hotpixelcorrection(cam::PcoVirtual) = cam.status[:hotpixelcorrection]


function bitalignment!(cam::PcoVirtual, ba::Integer)
    cam.status[:bitalignment] = Int(ba)
    nothing
end


function hotpixelcorrection!(cam::PcoVirtual, mode::Integer)
    cam.status[:hotpixelcorrection] = Int(mode)
    nothing
end


# ----------------------------------------------------------------------
#    BUFFER MANAGEMENT
# ----------------------------------------------------------------------

status(cam::PcoVirtual, bufnr::Integer) = (0, 0)


function allocate!(cam::PcoVirtual, bufnr=-1)
    if bufnr == -1
        bufnr = length(keys(cam.buffer))
    end
    width, height = size(cam)
    img = Array{WORD,2}(undef, width, height)
    depth = cam.spec[:depth]
    event = C_NULL
    buf = ImageBuffer(bufnr, img, depth, event)
    cam.buffer[bufnr] = buf
    push!(cam.available, bufnr)
    bufnr
end


function free!(cam, bufnr::Integer)
    pop!(cam.buffer, bufnr)
    filter!(i -> i != bufnr, cam.available)
    filter!(i -> i != bufnr, cam.queue)
    nothing
end


# ----------------------------------------------------------------------
#    IMAGE ACQUISITION
# ----------------------------------------------------------------------

function getimage(cam::PcoVirtual, segment::Integer, first::Integer,
                  last::Integer, bufnr::Integer, height::Integer,
                  width::Integer, depth::Integer)
    cam.generate(height, width)
end


function getimage(cam::PcoVirtual, segment::Integer, imgnr::Integer,
                  bufnr::Integer)
    buf = cam.buffer[bufnr]
    height, width = size(buf.image)
    depth = buf.depth
    getimage(cam, segment, imgnr, imgnr, bufnr, height, width, depth)
end


function add!(cam::PcoVirtual, first::Integer, last::Integer, bufnr::Integer,
              height::Integer, width::Integer, depth::Integer)
    push!(cam.queue, bufnr)
    bufnr
end

function add!(cam::PcoVirtual, first::Integer, last::Integer, bufnr::Integer)
    buf = cam.buffer[bufnr]
    height, width = size(buf.image)
    depth = buf.depth
    add!(cam, first, last, bufnr, height, width, depth)
end

function add!(cam::PcoVirtual, imgnr::Integer, bufnr::Integer)
    add!(cam, imgnr, imgnr, bufnr)
end


function queue!(cam::PcoVirtual, imgnr=0)
    bufnr = popfirst!(cam.available)
    add!(cam, imgnr, bufnr)
end


function waitfor(cam::PcoVirtual, bufnrs, timeout)
    w, h = cam.status[:resolution]
    for i in bufnrs
        copyto!(cam.buffer[i].image, cam.generate(w, h))
    end
    for bnr in bufnrs
        filter!(x -> x != bnr, cam.queue)
        push!(cam.available, bnr)
    end
    nothing
end

function waitfor(cam::PcoVirtual, bufnr::Integer, timeout)
    w, h = cam.status[:resolution]
    copyto!(cam.buffer[bufnr].image, cam.generate(w, h))
    filter!(x -> x != bufnr, cam.queue)
    push!(cam.available, bufnr)
    nothing
end


function cancel!(cam::PcoVirtual)
    while !isempty(cam.queue)
        push!(cam.available, popfirst!(cam.queue))
    end
end


# ----------------------------------------------------------------------
#    Utility
# ----------------------------------------------------------------------

# Describe the specifications of the camera itself
_describe(cam::PcoVirtual, outline) = println("This is a virtual camera")

# Check the camera status and display the results
healthcheck(cam::PcoVirtual) = true


# ----------------------------------------------------------------------
#    Test
# ----------------------------------------------------------------------

# set image generator for the virtual camera
function set_testimage(size)
    PCOVIRTUAL_DEFAULT_RESOLUTION[] = (size[2], size[1])
    nothing
end

function set_testimage(h::Integer, w::Integer)
    set_testimage((h, w))
end

function set_testimage(f::Function, size; autowrap=true)
    if autowrap
        PCOVIRTUAL_DEFAULT_IMAGEGENERATOR[] = (w, h) -> collect(transpose(f(w, h)))
    else
        PCOVIRTUAL_DEFAULT_IMAGEGENERATOR[] = f
    end
    set_testimage(size)
    nothing
end

function set_testimage(f::Function, h::Integer, w::Integer; kwargs...)
    set_testimage(f, (h, w); kwargs...)
end
