# Julia-style API to control cameras: wrappers of PCO APIs
# TODO: Check setting values

import Base: size, fetch


# The top of camera type tree, all cameras are a subtype of this type.
abstract type PcoCamera end


# The struct representing an image buffer in a camera
# A camera can have maximum 16 buffers allocated by allocate!()
struct ImageBuffer
    bufnr::Int
    image::Array{WORD,2}
    depth::Int
    event::HANDLE
end


# An error representing that something fatal happened on the camera
struct CameraError <: Exception
    msg::String
end

function Base.showerror(io::IO, err::CameraError)
    print(io, "Pco.CameraError: $(err.msg)")
end


# The macro to report if an API call returns error code
# TODO: Do not throw error if only warning
macro rccheck(apicall)
    str_apicall = "Pco." * sprint(Base.show_unquoted, apicall)
    return esc(quote
        rc = $apicall
        if rc != 0
            func = $str_apicall
            txt = errortext(rc)
            throw(CameraError("$(func) : $(txt)"))
        end
    end)
end


# ----------------------------------------------------------------------
#    CAMERA ACCESS
# ----------------------------------------------------------------------

function _open(interface::Integer)
    ph = Ref{HANDLE}(0)
    refopenstruct = Ref(API.Openstruct(wInterfaceType=interface))
    @rccheck API.OpenCameraEx(ph, refopenstruct)
    ph[], refopenstruct[]
end

_open() = _open(0xffff)


function _close(cam::PcoCamera)
    cancel!(cam)

    recstate = Ref(WORD(0))
    rc = API.GetRecordingState(cam.handle, recstate)
    if rc != 0
        @error "API.GetRecordingState : $(errortext(rc))"
    end
    if recstate[] != 0
        rc = API.SetRecordingState(cam.handle, false)
        if rc != 0
            @error "API.SetRecordingState : $(errortext(rc))"
        end
    end

    for bufnr in keys(cam.buffer)
        rc = API.FreeBuffer(cam.handle, bufnr)
        if rc != 0
            @error "API.FreeBuffer : $(errortext(rc))"
            break
        end
    end
    _close(cam.handle)
end

function _close(handle::HANDLE)
    rc = API.CloseCamera(handle)
    if rc != 0
        txt = errortext(rc)
        @error "API.CloseCamera : $(txt)"
    end
end


function resetlib()
    rc = API.ResetLib()
    if rc != 0
        txt = errortext(rc)
        @error "API.ResetLib : $(txt)"
    end
end


# ----------------------------------------------------------------------
#    CAMERA DESCRIPTION & GENERAL CAMERA STATUS
# ----------------------------------------------------------------------

const CAMERA_NAME_LEN = 40

function _name(handle::HANDLE)
    camnamelen = CAMERA_NAME_LEN
    camname = Vector{Cchar}(undef, camnamelen)
    @rccheck API.GetCameraName(handle, camname, camnamelen)
    camname[end] = 0
    unsafe_string(pointer(camname))
end


function _infostring(handle::HANDLE, code::Integer)
    buf = Vector{Cchar}(undef, API.MAX_INFOSTRING_LEN)
    @rccheck API.GetInfoString(handle, code, buf, API.MAX_INFOSTRING_LEN)
    buf[end] = 0
    unsafe_string(pointer(buf))
end


function _info(handle::HANDLE)
    camtype = Ref(API.CameraType())
    desc = Ref(API.Description())
    @rccheck API.GetCameraType(handle, camtype)
    @rccheck API.GetCameraDescriptionEx(handle, desc, 0x000)

    spec = Dict{Symbol,Any}()
    spec[:camera] = _name(handle)
    spec[:sensor] = _infostring(handle, 0x002)
    interpret!(spec, camtype[])
    interpret!(spec, desc[])

    spec
end


function interpret!(spec::Dict{Symbol,Any}, camtype::API.CameraType)
    # Camera type
    spec[:cameratype] = camtype.wCamType

    # Serial number of camera
    spec[:serialnumber] = camtype.dwSerialNumber

    # Interface type
    spec[:interface] = "UNKNOWN"
    spec[:interfacetype] = camtype.wInterfaceType
    for (code, interface) in API.INTERFACE_TYPES
        if camtype.wInterfaceType == code
            spec[:interface] = interface
        end
    end

    # Hardware version structures
    hwver = camtype.strHardwareVersion
    spec[:hwversions] = map(interpret, hwver.Board[1:hwver.BoardNum])

    # Firmware version structures
    fwver = camtype.strFirmwareVersion
    spec[:fwversions] = map(interpret, fwver.Device[1:fwver.DeviceNum])

    spec
end


function interpret!(spec::Dict{Symbol,Any}, desc::API.Description)
    # Sensor type
    spec[:sensortype] = desc.wSensorTypeDESC

    # Maximal resolution in pixels for standard format
    spec[:resolution] = Int.((desc.wMaxVertResStdDESC, desc.wMaxHorzResStdDESC))

    # Maximal resolution in pixels for extended format
    spec[:resolution_ex] = Int.((desc.wMaxVertResExtDESC, desc.wMaxHorzResExtDESC))

    # Dynamic resolution in bits/pixel
    spec[:depth] = Int(desc.wDynResDESC)

    # Horizontal binning
    if desc.wBinHorzSteppingDESC == 0
        spec[:hbins] = collect(1:desc.wMaxBinHorzDESC)
    elseif desc.wBinHorzSteppingDESC == 1
        spec[:hbins] = [2^x for x in 0:floor(log(2, desc.wMaxBinHorzDESC))]
    else
        spec[:hbins] = []
    end

    # Vertical binning
    if desc.wBinVertSteppingDESC == 0
        spec[:vbins] = collect(1:desc.wMaxBinVertDESC)
    elseif desc.wBinVertSteppingDESC == 1
        spec[:vbins] = [2^x for x in 0:floor(log(2, desc.wMaxBinVertDESC))]
    else
        spec[:vbins] = []
    end

    # Region-Of-Interest
    spec[:ROI_hstep] = Int(desc.wRoiHorStepsDESC)
    spec[:ROI_vstep] = Int(desc.wRoiHorStepsDESC)
    spec[:softROI_hstep] = Int(desc.wSoftRoiHorStepsDESC)
    spec[:softROI_vstep] = Int(desc.wSoftRoiVertStepsDESC)

    # ADC
    spec[:ADC] = Int(desc.wNumADCsDESC)

    # Minimum size of sub-image
    spec[:hmin] = Int(desc.wMinSizeHorzDESC)
    spec[:vmin] = Int(desc.wMinSizeVertDESC)

    # Pixel-rate
    spec[:pixelrates] = [Int(x) for x in desc.dwPixelRateDESC if x != 0]

    # Conversion factor
    spec[:convfactors] = [Int(x) for x in desc.wConvFactDESC if x != 0]

    # IR sensitivity
    spec[:IR] = desc.wIRDESC

    # delay time range
    spec[:delaymin] = desc.dwMinDelayDESC*1e-9
    spec[:delaymax] = desc.dwMaxDelayDESC*1e-3
    spec[:delaystep] = desc.dwMinDelayStepDESC*1e-9

    # exposure time range
    spec[:exposuremin] = desc.dwMinExposureDESC*1e-9
    spec[:exposuremax] = desc.dwMaxExposureDESC*1e-3
    spec[:exposurestep] = desc.dwMinExposureStepDESC*1e-9

    # delay time range (IR sensitivity mode)
    spec[:delaymin_IR] = desc.dwMinDelayIRDESC*1e-9
    spec[:delaymax_IR] = desc.dwMaxDelayIRDESC*1e-3

    # exposure time range (IR sensitivity mode)
    spec[:exposuremin_IR] = desc.dwMinExposureIRDESC*1e-9
    spec[:exposuremax_IR] = desc.dwMaxExposureIRDESC*1e-3

    # time table
    spec[:timetable] = desc.wTimeTableDESC

    # double image mode
    spec[:doubleimage] = desc.wDoubleImageDESC

    # cooling setpoint
    spec[:coolsetnum] = Int(desc.wNumCoolingSetpoints)
    spec[:coolsetpoints] = Int.(desc.sCoolingSetpoints[1:spec[:coolsetnum]])
    spec[:coolsetdef] = Int(desc.sDefaultCoolSetDESC)
    spec[:coolsetmin] = Int(desc.sMinCoolSetDESC)
    spec[:coolsetmax] = Int(desc.sMaxCoolSetDESC)

    # power down mode
    spec[:powerdownmode] = desc.wPowerDownModeDESC

    # offset regulation
    spec[:offsetregulation] = desc.wOffsetRegulationDESC

    # color pattern
    spec[:colorpat] = desc.wColorPatternDESC
    spec[:colorpattype] = desc.wPatternTypeDESC

    # Predefined values for external sync mode
    spec[:extfrequency] = [Int(x) for x in desc.dwExtSyncFrequency if x != 0]

    # GeneralCaps1
    cap1 = desc.dwGeneralCapsDESC1
    GeneralCaps1 = Dict{Symbol,Bool}()
    GeneralCaps1[:NOISE_FILTER] = cap1 & API.GENERALCAPS1_NOISE_FILTER != 0
    GeneralCaps1[:HOTPIX_FILTER] = cap1 & API.GENERALCAPS1_HOTPIX_FILTER != 0
    GeneralCaps1[:HOTPIX_ONLY_WITH_NOISE_FILTER] = cap1 & API.GENERALCAPS1_HOTPIX_ONLY_WITH_NOISE_FILTER != 0
    GeneralCaps1[:TIMESTAMP_ASCII_ONLY] = cap1 & API.GENERALCAPS1_TIMESTAMP_ASCII_ONLY != 0
    GeneralCaps1[:DATAFORMAT2X12] = cap1 & API.GENERALCAPS1_TIMESTAMP_ASCII_ONLY != 0
    GeneralCaps1[:RECORD_STOP] = cap1 & API.GENERALCAPS1_RECORD_STOP != 0
    GeneralCaps1[:HOT_PIXEL_CORRECTION] = cap1 & API.GENERALCAPS1_HOT_PIXEL_CORRECTION != 0
    GeneralCaps1[:NO_EXTEXPCTRL] = cap1 & API.GENERALCAPS1_NO_EXTEXPCTRL != 0
    GeneralCaps1[:NO_TIMESTAMP] = cap1 & API.GENERALCAPS1_NO_TIMESTAMP != 0
    GeneralCaps1[:NO_ACQUIREMODE] = cap1 & API.GENERALCAPS1_NO_ACQUIREMODE != 0
    GeneralCaps1[:DATAFORMAT4X16] = cap1 & API.GENERALCAPS1_DATAFORMAT4X16 != 0
    GeneralCaps1[:DATAFORMAT5X16] = cap1 & API.GENERALCAPS1_DATAFORMAT5X16 != 0
    GeneralCaps1[:NO_RECORDER] = cap1 & API.GENERALCAPS1_NO_RECORDER != 0
    GeneralCaps1[:FAST_TIMING] = cap1 & API.GENERALCAPS1_FAST_TIMING != 0
    GeneralCaps1[:METADATA] = cap1 & API.GENERALCAPS1_METADATA != 0
    GeneralCaps1[:SETFRAMERATE_ENABLED] = cap1 & API.GENERALCAPS1_SETFRAMERATE_ENABLED != 0
    GeneralCaps1[:CDI_MODE] = cap1 & API.GENERALCAPS1_CDI_MODE != 0
    GeneralCaps1[:CCM] = cap1 & API.GENERALCAPS1_CCM != 0
    GeneralCaps1[:EXTERNAL_SYNC] = cap1 & API.GENERALCAPS1_EXTERNAL_SYNC != 0
    GeneralCaps1[:NO_GLOBAL_SHUTTER] = cap1 & API.GENERALCAPS1_NO_GLOBAL_SHUTTER != 0
    GeneralCaps1[:GLOBAL_RESET_MODE] = cap1 & API.GENERALCAPS1_GLOBAL_RESET_MODE != 0
    GeneralCaps1[:EXT_ACQUIRE] = cap1 & API.GENERALCAPS1_EXT_ACQUIRE != 0
    GeneralCaps1[:FAN_LED_CONTROL] = cap1 & API.GENERALCAPS1_FAN_LED_CONTROL != 0
    GeneralCaps1[:ROI_VERT_SYMM_TO_HORZ_AXIS] = cap1 & API.GENERALCAPS1_ROI_VERT_SYMM_TO_HORZ_AXIS != 0
    GeneralCaps1[:ROI_HORZ_SYMM_TO_VERT_AXIS] = cap1 & API.GENERALCAPS1_ROI_HORZ_SYMM_TO_VERT_AXIS != 0
    GeneralCaps1[:COOLING_SETPOINTS] = cap1 & API.GENERALCAPS1_COOLING_SETPOINTS != 0
    GeneralCaps1[:USER_INTERFACE] = cap1 & API.GENERALCAPS1_USER_INTERFACE != 0
    GeneralCaps1[:ENHANCED_DESCRIPTOR_INTENSIFIED] = cap1 & API.GENERALCAPS1_ENHANCED_DESCRIPTOR_INTENSIFIED != 0
    GeneralCaps1[:HW_IO_SIGNAL_DESCRIPTOR] = cap1 & API.GENERALCAPS1_HW_IO_SIGNAL_DESCRIPTOR != 0
    GeneralCaps1[:ENHANCED_DESCRIPTOR_2] = cap1 & API.GENERALCAPS1_ENHANCED_DESCRIPTOR_2 != 0
    spec[:GeneralCaps1] = GeneralCaps1

    cap3 = desc.dwGeneralCapsDESC3
    GeneralCaps3 = Dict{Symbol,Bool}()
    GeneralCaps3[:HDSDI_1G5] = cap3 & API.GENERALCAPS3_HDSDI_1G5 != 0
    GeneralCaps3[:HDSDI_3G] = cap3 & API.GENERALCAPS3_HDSDI_3G != 0
    GeneralCaps3[:IRIG_B_UNMODULATED] = cap3 & API.GENERALCAPS3_IRIG_B_UNMODULATED != 0
    GeneralCaps3[:IRIG_B_MODULATED] = cap3 & API.GENERALCAPS3_IRIG_B_MODULATED != 0
    GeneralCaps3[:CAMERA_SYNC] = cap3 & API.GENERALCAPS3_CAMERA_SYNC != 0
    GeneralCaps3[:RESERVED0] = cap3 & API.GENERALCAPS3_RESERVED0 != 0
    GeneralCaps3[:HS_READOUT_MODE] = cap3 & API.GENERALCAPS3_HS_READOUT_MODE != 0
    GeneralCaps3[:EXT_SYNC_1HZ_MODE] = cap3 & API.GENERALCAPS3_EXT_SYNC_1HZ_MODE != 0
    spec[:GeneralCaps3] = GeneralCaps3

    spec
end

function interpret!(spec, desc2::API.Description2)
end

function interpret(hwdesc::API.SC2_Hardware_DESC)
    name = unsafe_string(pointer(push!([x for x in hwdesc.szName], 0)))
    (name, Int(hwdesc.wBatchNo), Int(hwdesc.wRevision), Int(hwdesc.wVariant))
end

function interpret(fwdesc::API.SC2_Firmware_DESC)
    name = unsafe_string(pointer(push!([x for x in fwdesc.szName], 0)))
    (name, Int(fwdesc.bMajorRev), Int(fwdesc.bMinorRev), Int(fwdesc.wVariant))
end


function status(cam::PcoCamera)
    warning = Ref(DWORD(0))
    error = Ref(DWORD(0))
    status = Ref(DWORD(0))
    @rccheck API.GetCameraHealthStatus(cam.handle, warning, error, status)
    warning[], error[], status[]
end


function temperature(cam::PcoCamera)
    ccd = Ref(SHORT(0))
    body = Ref(SHORT(0))
    extra = Ref(SHORT(0))
    @rccheck API.GetTemperature(cam.handle, ccd, body, extra)
    temp = Dict{Symbol,Float64}()
    temp[:Camera] = body[]
    if abs(ccd[]) != typemin(SHORT)
        temp[:CCD] = ccd[]/10
    end
    if extra[] != 0
        temp[:Extra] = extra[]
    end
    temp
end


# ----------------------------------------------------------------------
#    GENERAL CAMERA CONTROL
# ----------------------------------------------------------------------

function arm!(cam::PcoCamera)
    @rccheck API.ArmCamera(cam.handle)
end


function imageparameters!(cam::PcoCamera, height::Integer, width::Integer,
                          flags=0x00)
    param = Ref(Cvoid())
    ilen = 0
    @rccheck API.SetImageParameters(cam.handle, width, height, flags, param,
                                    ilen)
end


function reset!(cam::PcoCamera)
    @rccheck API.ResetSettingsToDefault(cam.handle)
end


function reboot!(cam::PcoCamera)
    rc = API.RebootCamera(cam.handle)
    if rc != 0
        txt = errortext(rc)
        @error "API.RebootCamera : $(txt)"
        return nothing
    end

    rc = API.CloseCamera(cam.handle)
    if rc != 0
        txt = errortext(rc)
        @error "API.CloseCamera : $(txt)"
    end
end


# ----------------------------------------------------------------------
#    IMAGE SENSOR
# ----------------------------------------------------------------------

function _size(handle::HANDLE)
    height = Ref(WORD(0))
    width = Ref(WORD(0))
    maxheight = Ref(WORD(0))
    maxwidth = Ref(WORD(0))
    @rccheck API.GetSizes(handle, width, height, maxwidth, maxheight)
    map(x -> Int(x[]), (width, height, maxwidth, maxheight))
end


function size(cam::PcoCamera)
    width, height, _, _ = _size(cam.handle)
    width, height
end


function sensorformat(cam::PcoCamera)
    sf = Ref(WORD(0))
    @rccheck API.GetSensorFormat(cam.handle, sf)
    sf[]
end


function sensorformat!(cam::PcoCamera, sf::Integer)
    @rccheck API.SetSensorFormat(cam.handle, sf)
end


function roi(cam::PcoCamera)
    x0 = Ref(WORD(0))
    y0 = Ref(WORD(0))
    x1 = Ref(WORD(0))
    y1 = Ref(WORD(0))
    @rccheck API.GetROI(cam.handle, x0, y0, x1, y1)
    map(x -> Int(x[]), (y0, x0, y1, x1))
end


function roi!(cam::PcoCamera)
    @warn "The current camera does not support `Pco.roi!`"
end


function binning(cam::PcoCamera)
    hbin = Ref(WORD(0))
    vbin = Ref(WORD(0))
    @rccheck API.GetBinning(cam.handle, hbin, vbin)
    map(x -> Int(x[]), (hbin, vbin))
end


function binning!(cam::PcoCamera, hbin, vbin)
    @rccheck API.SetBinning(cam.handle, hbin, vbin)
end


function pixelrate(cam::PcoCamera)
    rate = Ref(DWORD(0))
    @rccheck API.GetPixelRate(cam.handle, rate)
    Int(rate[])
end


function pixelrate!(cam::PcoCamera, rate)
    @rccheck API.SetPixelRate(cam.handle, rate)
end


function conversionfactor(cam::PcoCamera)
    c = Ref(WORD(0))
    @rccheck API.GetConversionFactor(cam.handle, c)
    Int(c[])
end


function conversionfactor!(cam::PcoCamera, c)
    @rccheck API.SetConversionFactor(cam.handle, c)
end


function IRsensitivity(cam::PcoCamera)
    @warn "The current camera does not support `Pco.IRsensitivity`"
end


function IRsensitivity!(cam::PcoCamera, ir::Integer)
    @warn "The current camera does not support `Pco.IRsensitivity!`"
end


# ----------------------------------------------------------------------
#    TIMING CONTROL
# ----------------------------------------------------------------------

function delay_exposure(cam::PcoCamera)
    delay = Ref(DWORD(0))
    exposure = Ref(DWORD(0))
    delay_base = Ref(WORD(0))
    exposure_base = Ref(WORD(0))
    @rccheck API.GetDelayExposureTime(cam.handle, delay, exposure, delay_base,
                                      exposure_base)
    d = floatvalue(delay[], delay_base[])
    e = floatvalue(exposure[], exposure_base[])
    d, e
end


function floatvalue(v, b)
    if b == 0x000
        v*1e-9
    elseif b == 0x001
        v*1e-6
    elseif b == 0x002
        v*1e-3
    else
        # should not reach here
        error("Unknown base value: $(b)")
    end
end


function delay_exposure!(cam::PcoCamera, d, e)
    delay, delay_base = splitfloat(d)
    exposure, exposure_base = splitfloat(e)
    @rccheck API.SetDelayExposureTime(cam.handle, delay, exposure, delay_base,
                                      exposure_base)
end


function splitfloat(f)
    if f >= 1e-3 && rem(f, 1e-3, RoundNearest) < 1e-9
        b = 0x002
        v = round(DWORD, f/1e-3)
    elseif f >= 1e-6 && rem(f, 1e-6, RoundNearest) < 1e-9
        b = 0x001
        v = round(DWORD, f/1e-6)
    else
        b = 0x000
        v = round(DWORD, f/1e-9)
    end
    v, b
end


function triggermode(cam::PcoCamera)
    mode = Ref(WORD(0))
    @rccheck API.GetTriggerMode(cam.handle, mode)
    mode[]
end


function triggermode!(cam::PcoCamera, mode::Integer)
    @rccheck API.SetTriggerMode(cam.handle, mode)
end


function trigger(cam::PcoCamera)
    triggered = Ref(WORD(0))
    @rccheck API.ForceTrigger(cam.handle, triggered)
    triggered[]
end


# ----------------------------------------------------------------------
#    RECORDING CONTROL
# ----------------------------------------------------------------------

function recordingstate(cam::PcoCamera)
    state = Ref(WORD(0))
    @rccheck API.GetRecordingState(cam.handle, state)
    state[]
end


function recordingstate!(cam::PcoCamera, state::Integer)
    @rccheck API.SetRecordingState(cam.handle, state)
end


function timestamp(cam::PcoCamera)
    mode = Ref(WORD(0))
    @rccheck API.GetTimestampMode(cam.handle, mode)
    mode[]
end


function timestamp!(cam::PcoCamera, mode::Integer)
    if !(mode in 0:3)
        @warn "time stamp mode should be 0, 1, 2, or 3" mode
        return nothing
    end
    @rccheck API.SetTimestampMode(cam.handle, mode)
end


# ----------------------------------------------------------------------
#    IMAGE INFORMATION
# ----------------------------------------------------------------------

function bitalignment(cam::PcoCamera)
    ba = Ref(WORD(0))
    @rccheck API.GetBitAlignment(cam.handle, ba)
    ba[]
end


function bitalignment!(cam::PcoCamera, ba::Integer)
    @rccheck API.SetBitAlignment(cam.handle, ba)
end


function hotpixelcorrection(cam::PcoCamera)
    mode = Ref(WORD(0))
    @rccheck API.GetHotPixelCorrectionMode(cam.handle, mode)
    mode[]
end


function hotpixelcorrection!(cam::PcoCamera, mode::Integer)
    @rccheck API.SetHotPixelCorrectionMode(cam.handle, mode)
end


# ----------------------------------------------------------------------
#    BUFFER MANAGEMENT
# ----------------------------------------------------------------------

function allocate!(cam::PcoCamera, bufnr=-1)
    bufnrptr = Ref(SHORT(0))
    bufnrptr[] = bufnr
    width, height = size(cam)
    bytesize = width*height*sizeof(WORD)
    img = Array{WORD,2}(undef, width, height)
    refimg = [Ref(img, i) for i in 1:size(img, 1):length(img)]
    event = Ref(HANDLE(0))
    event[] = C_NULL
    @rccheck API.AllocateBuffer(cam.handle, bufnrptr, bytesize, refimg, event)

    bufnr = Int(bufnrptr[])
    depth = cam.spec[:depth]
    buf = ImageBuffer(bufnr, img, depth, event[])
    cam.buffer[bufnr] = buf
    push!(cam.available, bufnr)
    bufnr
end


function free!(cam::PcoCamera, bufnr::Integer)
    pop!(cam.buffer, bufnr)
    filter!(i -> i != bufnr, cam.available)
    filter!(i -> i != bufnr, cam.queue)
    @rccheck API.FreeBuffer(cam.handle, bufnr)
end


function status(cam::PcoCamera, bufnr::Integer)
    statusdll = Ref(DWORD(0))
    statusdrv = Ref(DWORD(0))
    @rccheck API.GetBufferStatus(cam.handle, bufnr, statusdll, statusdrv)
    statusdll[], statusdrv[]
end


# ----------------------------------------------------------------------
#    IMAGE ACQUISITION
# ----------------------------------------------------------------------

function getimage(cam::PcoCamera, segment::Integer, first::Integer,
                  last::Integer, bufnr::Integer, height::Integer,
                  width::Integer, depth::Integer)
    @rccheck API.GetImageEx(cam.handle, segment, first, last, bufnr, width,
                            height, depth)
    buf = cam.buffer[bufnr]
    buf.image
end

function getimage(cam::PcoCamera, segment::Integer, imgnr::Integer,
                  bufnr::Integer)
    buf = cam.buffer[bufnr]
    height, width = size(buf.image)
    depth = buf.depth
    getimage(cam, segment, imgnr, imgnr, bufnr, height, width, depth)
end


function add!(cam::PcoCamera, first::Integer, last::Integer, bufnr::Integer,
              height::Integer, width::Integer, depth::Integer)
    push!(cam.queue, bufnr)
    @rccheck API.AddBufferEx(cam.handle, first, last, bufnr, width, height,
                             depth)
    bufnr
end

function add!(cam::PcoCamera, first::Integer, last::Integer, bufnr::Integer)
    buf = cam.buffer[bufnr]
    height, width = size(buf.image)
    depth = buf.depth
    add!(cam, first, last, bufnr, height, width, depth)
end

function add!(cam::PcoCamera, imgnr::Integer, bufnr::Integer)
    add!(cam, imgnr, imgnr, bufnr)
end


## FIXME: API.WaitforBuffer doesn't work with multiple buffers...?
##        I could not get correct images...

# function waitfor(cam::PcoCamera, bufnrs::Vector{Int}, timeout)
#     buflist = map(API.Buflist, bufnrs)
#     nbuf = length(buflist)
#     @rccheck API.WaitforBuffer(cam.handle, nbuf, buflist, timeout)
#     empty!(cam.queue)
#     nothing
# end

# function waitfor(cam::PcoCamera, bufnr::Integer, timeout)
#     waitfor(cam, [bufnr], timeout)
# end

const WAITALL = 1
function waitfor(cam::PcoCamera, bufnrs, timeout=DEFAULT_TIMEOUT)
    bufnrs = copy(bufnrs)
    events = map(i -> cam.buffer[i].event, bufnrs)
    timeout_in_ms = round(Int, timeout*1000)
    rc = Kernel32.WaitForMultipleObjects(length(bufnrs), events, WAITALL,
                                         timeout_in_ms)
    if rc == Kernel32.WAIT_TIMEOUT
        msg = "Waiting for trigger timed out: $(timeout) seconds"
        throw(CameraError(msg))
    elseif rc == Kernel32.WAIT_FAILED
        # FIXME: report more detail with GetLastError and FormatMessage
        msg = "Error occurred in WaitForMultipleObjects call"
        throw(CameraError(msg))
    end

    for bnr in bufnrs
        filter!(x -> x != bnr, cam.queue)
        push!(cam.available, bnr)
    end
    nothing
end

function waitfor(cam::PcoCamera, bufnr::Integer, timeout=DEFAULT_TIMEOUT)
    event = cam.buffer[bufnr].event
    timeout_in_ms = round(Int, timeout*1000)
    rc = Kernel32.WaitForSingleObject(event, timeout_in_ms)
    if rc == Kernel32.WAIT_TIMEOUT
        msg = "Waiting for trigger timed out: $(timeout) seconds"
        throw(CameraError(msg))
    elseif rc == Kernel32.WAIT_FAILED
        # FIXME: report more detail with GetLastError and FormatMessage
        msg = "Error occurred in WaitForSingleObject call"
        throw(CameraError(msg))
    end

    filter!(x -> x != bufnr, cam.queue)
    push!(cam.available, bufnr)
    nothing
end


function cancel!(cam::PcoCamera)
    rc = API.CancelImages(cam.handle)
    if rc != 0
        txt = errortext(rc)
        @error "API.CancelImages : $(txt)"
    end
    while !isempty(cam.queue)
        push!(cam.available, popfirst!(cam.queue))
    end
end


# ----------------------------------------------------------------------
#    DEBUG
# ----------------------------------------------------------------------

function errortext(rc)
    len = 200
    txt = Vector{Cchar}(undef, len)
    API.GetErrorTextSDK(rc, txt, len)
    txt[end] = 0
    unsafe_string(pointer(txt))
end


# ----------------------------------------------------------------------
#    Utility
# ----------------------------------------------------------------------

# Describe the specifications of the camera itself
include("describe.jl")

# Check the camera status and display the results
include("healthcheck.jl")


isdimensionmatched(a, b) = size(a, 1) == size(b, 1) && size(a, 2) == size(b, 2)


function pending_bufnrs(cam, bufnrs)
    if isempty(cam.queue) || isempty(bufnrs)
        return Int[]
    end

    pending = Int[]
    for bnr in bufnrs
        if bnr in cam.queue
            push!(pending, bnr)
        end
    end
    pending
end


# ----------------------------------------------------------------------
#     HIGH-LEVEL API
# ----------------------------------------------------------------------

# The following functions are the typical implementations of those
# representative methods for a camera.
# Those functions can be extended for a specific camera as a specialized
# methods in each camera implementation.

const DEFAULT_TRIGGERMODE = 0x0000         # auto trigger
const DEFAULT_DELAY_TIME = 0.0             # in seconds
const DEFAULT_EXPOSURE_TIME = 0.01         # in seconds
const DEFAULT_PIXELRATE = nothing
const DEFAULT_HORIZONTAL_BINNING = 1
const DEFAULT_VERTICAL_BINNING = 1
const DEFAULT_BITALIGNMENT = 1             # 0: MSB, 1: LSB
const DEFAULT_SENSOR_FORMAT = 0            # 0: Normal, 1: Extended
const DEFAULT_HOTPIXEL_CORRECTION = 0      # 0: off, 1: on
const DEFAULT_CONVERSION_FACTOR = nothing
const DEFAULT_ROI = nothing
const DEFAULT_TIMEOUT = 5                  # in seconds

# Initializing process in opencamera()
# TODO: Cover features as possible
"""
    initialize!(cam::PcoCamera; trig=0, delay=0.0, exposure=0.01,
                                pixelrate=nothing, hbin=1, vbin=1,
                                bitalign=1, exsensor=0, hotpixcorr=0
                                convfactor=nothing)

Initialize camera and set camera parameters. The parameters can be assigned
through the keyword arguments. Check the camera manual and pco SDK manual for
the details.

To see the available values for the options, check the output of
describecamera().

 * trig

The keyword for trigger mode. Without any special reason, use `0` or `2` .
(default: `0`)

| trig | description               |
|:-----|:--------------------------|
| `0`  | auto trigger              |
| `1`  | software trigger          |
| `2`  | external trigger          |
| `3`  | external exposure control |
| `4`  | external synchronized     |

 * delay

The delay time from trigger to exposure in second. (default: `0.0`)

 * exposure

The exposure time in second. (default: `0.01`)

 * pixelrate

The pixel scan rate of the camera, it is related to the buffer readout time and
thus the resulted frame-per-second (fps). (default: depending on the camera)

 * hbin

The Horizontal binning. (default: `1`)

 * vbin

The vertical binning. (default: `1`)

 * bitalign

This option controls the bit alignment of data from the camera. It shouldn't be
changed without any special reason. (default: `1`)

 * exsensor

This option controls the sensor format. Some pco camras have "extended" sensor
format, this option is to enable it. (default: `0`)

| exsensor | description     |
|:---------|:----------------|
| `0`      | standard format |
| `1`      | extended format |

 * hotpixcorr

Turning ON/OFF of hot pixel correction. (default: `0`)

| hotpixcorr | description |
|:-----------|:------------|
| `0`        | OFF         |
| `1`        | ON          |

 * convfactor

This option controls the current conversion factor.
(default: depending on the camera)
"""
function initialize!(cam::PcoCamera;
                     trig=DEFAULT_TRIGGERMODE,
                     delay=DEFAULT_DELAY_TIME,
                     exposure=DEFAULT_EXPOSURE_TIME,
                     pixrate=DEFAULT_PIXELRATE,
                     hbin=DEFAULT_HORIZONTAL_BINNING,
                     vbin=DEFAULT_VERTICAL_BINNING,
                     bitalign=DEFAULT_BITALIGNMENT,
                     exsensor=DEFAULT_SENSOR_FORMAT,
                     hotpixcorr=DEFAULT_HOTPIXEL_CORRECTION,
                     convfactor=DEFAULT_CONVERSION_FACTOR,
                     roi=DEFAULT_ROI)
    reset!(cam)
    sensorformat!(cam, exsensor)
    triggermode!(cam, trig)
    delay_exposure!(cam, delay, exposure)
    binning!(cam, hbin, vbin)
    bitalignment!(cam, bitalign)
    hotpixelcorrection!(cam, hotpixcorr)
    if pixrate != nothing
        pixelrate!(cam, pixrate)
    end
    if convfactor != nothing
        conversionfactor!(cam, convfactor)
    end
    if roi != nothing && cam.spec[:ROI_hstep] != 0
        roi!(cam, roi...)
    end

    arm!(cam)
    for i in 1:cam.bufmax
        allocate!(cam)
    end
    recordingstate!(cam, true)
end


"""
    atake(cam::PcoCamera, n::Integer)

Take images asynchronously. This function quit immediately and the images can
be obtained by [`fetch`](@ref).

The `n` is the number of taken images; it should be 1 <= n <= 16.

The returned value is a three-dimensional array; height x width x `n`.

# Example
```julia-repl
julia> cam = opencamera();

julia> bufnrs = atake(cam, 16);  # quit immediately

julia> imgs = fetch(cam, bufnrs)
```
"""
function atake(cam::PcoCamera, n::Integer)
    if n < 1
        return Int[]
    end
    if count_available(cam) < n
        throw(CameraError("Short of the available camera buffers"))
    end

    bufnrs = Int[]
    for _ in 0:n-1
        n = popfirst!(cam.available)
        add!(cam, 0, n)
        push!(bufnrs, n)
    end
    bufnrs
end


"""
    fetch!(cam::PcoCamera, dest, bufnrs[, timeout=5])

Fetch images scheduled by [`atake`](@ref) function and save to the assigned array.
If `timeout` is omitted, this function will be timed-out in five seconds.

In comparison with [`fetch`](@ref), this function can save memory with a pre-allocated
array.

# Example
```julia-repl
julia> cam = opencamera();

julia> width, height = size(cam);

julia> imgs = Array{Int,3}(undef, width, height, 16)

julia> bufnrs = atake(cam, 16);  # quit immediately

julia> fetch!(cam, imgs, bufnrs)
```
"""
function fetch!(cam::PcoCamera, dest, bufnrs, timeout=DEFAULT_TIMEOUT)
    pending = pending_bufnrs(cam, bufnrs)
    if !isempty(pending)
        waitfor(cam, pending, timeout)
    end

    buf = cam.buffer[bufnrs[1]].image
    !isdimensionmatched(dest, buf) && throw(DimensionMismatch("Invalid size of destination array"))
    for n in 1:length(bufnrs)
        buf = cam.buffer[bufnrs[n]].image
        copyto!(view(dest, :, :, n), buf)
    end
    dest
end


"""
    fetch(cam::PcoCamera, bufnrs[, timeout=5])

Fetch images scheduled by [`atake`](@ref) function. If `timeout` is omitted, this
function will be timed-out in five seconds.

In comparison with [`fetch!`](@ref), this function is easy to use but always allocate
a new memory space for the returned value.

The returned value is a three-dimensional array; height x width x `n`.

# Example
```julia-repl
julia> cam = opencamera();

julia> bufnrs = atake(cam, 16);  # quit immediately

julia> imgs = fetch(cam, bufnrs)
```
"""
function fetch(cam::PcoCamera, bufnrs, timeout=DEFAULT_TIMEOUT)
    if isempty(bufnrs)
        return Array{Int,3}[]
    end
    buf = cam.buffer[bufnrs[1]].image
    width, height = size(cam)
    n = length(bufnrs)
    dest = Array{Int,3}(undef, width, height, n)
    fetch!(cam, dest, bufnrs, timeout)
end


"""
    take!(cam::PcoCamera, dest[, timeout=5])

Take an image and save to the assigned array. If `timeout` is omitted, this
process will be timed-out in five seconds.

In comparison with [`take`](@ref), this function can save memory with a pre-allocated
array.

# Example
```julia-repl
julia> cam = opencamera();

julia> resolution = size(cam);

julia> img = Array{Int,2}(undef, resolution)

julia> take!(cam, img)  # take an image and save to `img`
```
"""
function take!(cam::PcoCamera, dest::Matrix{<:Real}, timeout=DEFAULT_TIMEOUT)
    bufnrs = atake(cam, 1)
    waitfor(cam, bufnrs, timeout)
    cancel!(cam)

    buf = cam.buffer[bufnrs[1]].image
    !isdimensionmatched(dest, buf) && throw(DimensionMismatch("Invalid size of destination array"))
    copyto!(dest, buf)
end


"""
    take(cam::PcoCamera[, timeout=5])

Take an image. If `timeout` is omitted, this process will be timed-out in five
seconds.

In comparison with [`take!`](@ref), this function is easy to use but always allocate
a new memory space for the returned value.

# Example
```julia-repl
julia> cam = opencamera();

julia> img = take(cam)  # take an image
```
"""
function take(cam::PcoCamera, timeout=DEFAULT_TIMEOUT)
    width, height = size(cam)
    img = Array{Int,2}(undef, width, height)
    take!(cam, img, timeout)
end


"""
    count_available(cam::PcoCamera)

Return the number of available buffers for image acquisition.

# Example
```julia-repl
julia> cam = opencamera();

julia> n = count_available(cam::PcoCamera)
16

julia> bufnrs = atake(cam, n);  # Use all the available buffers

julia> images = fetch(cam, bufnrs);
```
"""
function count_available(cam::PcoCamera)
    length(cam.available)
end


"""
    count_queued(cam::PcoCamera)

Return the number of pending buffers for image acquisition.

# Example
```julia-repl
julia> cam = opencamera();

julia> n = count_available(cam::PcoCamera)
16

julia> bufnrs1 = atake(cam, n÷2);  # Use the half of available buffers

julia> count = count_queued(cam)
8

julia> bufnrs2 = atake(cam, n÷2);  # Use the rest of available buffers

julia> count = count_queued(cam)
16
```
"""
function count_queued(cam::PcoCamera)
    length(cam.queue)
end
