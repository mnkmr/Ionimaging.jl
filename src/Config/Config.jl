module Config

export
    CONFIG,
    DATADIR,
    makeconf_example,
    loadconf,
    camera_parameters

import Base: Filesystem
import Dates
import YAML

const CONFIG_FILE_NAME = "config.yaml"
const CONFIG_FILE_PATH = Ref{String}("")
const CONFIG = Dict{String,Any}()
const DATADIR = Ref{String}("")
const DEFAULT_INTERFACE_CODE = 0xffff
const CAMERA_PARAMETER_LIST = ["trig", "delay", "exposure", "pixelrate",
                               "hbin", "vbin", "exsensor", "hotpixcorr",
                               "convfactor", "roi"]


function initialize_CONFIG!()
    empty!(CONFIG)
    CONFIG["interface"] = Dict{String,Any}("code" => DEFAULT_INTERFACE_CODE)
    CONFIG["fileio"] = Dict{String,Any}("data_directory" => datadir())
    CONFIG["measurement"] = Dict{String,Any}()
    CONFIG["measurement"]["take"] = Dict{String,Any}()
    CONFIG["measurement"]["live"] = Dict{String,Any}()
    CONFIG["measurement"]["imaging"] = Dict{String,Any}("trig" => 2)
    CONFIG["measurement"]["imagingsp"] = Dict{String,Any}("trig" => 2)
    CONFIG
end


function makeconf_example(path::AbstractString=CONFIG_FILE_PATH[])
    if path == ""
        path = joinpath(confdir(), CONFIG_FILE_NAME)
    end
    confpath = expandpath(path)
    confdir = dirname(confpath)
    if !isdir(confdir)
        Filesystem.mkpath(confdir)
    end
    examplefile = joinpath(@__DIR__, "..", "..", "config.yaml.example")
    Filesystem.cp(examplefile, confpath)
    confpath
end


function loadconf(path::AbstractString=CONFIG_FILE_PATH[])
    if !isfile(path)
        @warn "The configuration file '$(path)' does not exist."
        return CONFIG
    end
    yaml = open(path) do file
        YAML.load(file)
    end

    if get(yaml, "interface", nothing) != nothing
        merge!(CONFIG["interface"], yaml["interface"])
    end

    if get(yaml, "fileio", nothing) != nothing
        merge!(CONFIG["fileio"], yaml["fileio"])
    end
    if CONFIG["fileio"]["data_directory"] != ""
        dt = Dates.now()
        yyyy = Dates.format(dt, "yyyy")
        mmdd = Dates.format(dt, "mmdd")
        parentdir = expandpath(CONFIG["fileio"]["data_directory"])
        DATADIR[] = joinpath(parentdir, yyyy, mmdd)
    end

    if get(yaml, "measurement", nothing) != nothing
        for k in ["take", "live", "imaging", "imagingsp"]
            if get(yaml["measurement"], k, nothing) != nothing
                merge!(CONFIG["measurement"][k], yaml["measurement"][k])
            end
        end
    end

    CONFIG
end


const CONFIG_ENV_NAME = "IONIMAGING_CONFIGDIR"

function confdir()
    if haskey(ENV, CONFIG_ENV_NAME)
        return ENV[CONFIG_ENV_NAME]
    end
    if Sys.iswindows()
        return joinpath(ENV["APPDATA"], "Ionimaging")
    end
    if haskey(ENV, "XDG_CONFIG_HOME")
        return joinpath(ENV["XDG_CONFIG_HOME"], "Ionimaging")
    end
    joinpath(ENV["HOME"], ".Ionimaging")
end


datadir() = Sys.iswindows() ? joinpath(ENV["USERPROFILE"], "Documents") : ""


if Sys.iswindows()
    function expandpath(lpSrc::AbstractString)
        nSize = 260
        lpDst = Vector{Cwchar_t}(undef, nSize + 1)
        ccall((:ExpandEnvironmentStringsW, "Kernel32.dll"),
            Culong, (Cwstring, Ref{Cwchar_t}, Culong), lpSrc, lpDst, nSize)
        lpDst[end] = 0x0000
        i = findfirst(x -> x === 0x0000, lpDst) - 1
        normpath(transcode(String, lpDst[1:i]))
    end
else
    # Not implemented!
    expandpath(lpSrc::AbstractString) = lpSrc
end


function camera_parameters(funcname::String, additional=NamedTuple())
    d = filter(p -> p.first in CAMERA_PARAMETER_LIST, CONFIG["measurement"][funcname])
    cam_par = NamedTuple{Tuple(Symbol.(keys(d)))}(values(d))
    ret = merge(cam_par, additional)
    if haskey(ret, :roi)
        ret = transpose_roi(ret)
    end
    ret
end


function transpose_roi(cam_par)
    roi = cam_par.roi
    merge(cam_par, (roi = (roi[2], roi[1], roi[4], roi[3]),))
end


function take(; bg=nothing, progress=true, kwargs...)
    opt = (bg = bg, progress = progress)
    cam_par = camera_parameters("take", kwargs)
    opt, cam_par
end


function imaging(; save=true, bg=nothing, mask=[], interval=2,
                 monitor=true, progress=true, kwargs...)
    opt = (save = save, bg = bg, mask = mask, interval = interval,
           monitor = monitor, progress = progress)
    cam_par = camera_parameters("imaging", kwargs)
    opt, cam_par
end


function imagingsp(; save=true, bg=nothing, mask=[], interval=2,
                   monitor=true, progress=true, kwargs...)
    opt = (save = save, bg = bg, mask = mask, interval = interval,
           monitor = monitor, progress = progress)
    cam_par = camera_parameters("imagingsp", kwargs)
    opt, cam_par
end


function live(; bg=nothing, kwargs...)
    opt = (bg = bg,)
    cam_par = camera_parameters("live", kwargs)
    opt, cam_par
end


end
