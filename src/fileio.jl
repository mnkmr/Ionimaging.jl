import HDF5

"""
    saveimg!(filepath, ionimage::AbstractIonimage, complv)

Save an image forcibly. This function overrides the file `filepath` even if it
is already exists. In interactive usage, it is recommended to use
[`saveimg`](@ref) instead for safety.
"""
function saveimg!(filepath, ionimage::AbstractIonimage, complv)
    HDF5.h5open(filepath, "w") do file
        file["image", "compress", complv] = ionimage.arr

        if isdefined(ionimage, :spots)
            spot_y, spot_x = split_to_arrays(ionimage.spots, Float64)
            file["spot_y", "compress", complv] = spot_y
            file["spot_x", "compress", complv] = spot_x
        end

        for (k, v) in ionimage.meta
            if k == "starttime" || k == "finishtime"
                HDF5.attrs(file["image"])[k] = "$(v)"
            elseif k == "mask"
                mask_y, mask_x = split_to_arrays(v, Int)
                HDF5.attrs(file["image"])["masked_pixel_y"] = mask_y
                HDF5.attrs(file["image"])["masked_pixel_x"] = mask_x
            else
                HDF5.attrs(file["image"])[k] = v
            end
        end
    end
    nothing
end

"""
    saveimg!(filepath, ionimage::AbstractIonimage; complv=1)

Save an image forcibly. This function overrides the file `filepath` even if it
is already exists. In interactive usage, it is recommended to use
[`saveimg`](@ref) instead for safety.

The image is saved in a HDF5 container format. The image data is stored at the
root of container with "image" tag, and the metadata is saved as its attributes.

This function compress the data using ZLIB (deflate algorithm). User can assign
the compression level through `complv`; 0 (no compression) to 9.

# Examples
```julia-repl
julia> raw = take();

julia> saveimg!("image.h5", raw)

julia> img = ionimage(100, 240)

julia> saveimg!("image.h5", raw)  # Override
```
"""
function saveimg!(filepath, ionimage::AbstractIonimage; complv=1)
    saveimg!(filepath, ionimage, complv)
end


"""
    saveimg(filepath, ionimage::AbstractIonimage; complv=1)

Save an image safely. This function fails if the `filepath` already exists.

The image is saved in a HDF5 container format. The image data is stored at the
root of container with "image" tag, and the metadata is saved as its attributes.

This function compress the data using ZLIB (deflate algorithm). User can assign
the compression level through `complv`; 0 (no compression) to 9.

# Examples
```julia-repl
julia> raw = take();

julia> saveimg("image.h5", raw)

julia> img = ionimage(100, 240)

julia> saveimg("image.h5", raw)  # Fail
┌ Error: 'image.h5' already exists, failed to save.
└ @ Ionimaging
```
"""
function saveimg(filepath, ionimage::AbstractIonimage; complv=1)
    if isfile(filepath)
        @error "'$(filepath)' already exists, failed to save."
        return
    end
    saveimg!(filepath, ionimage, complv)
end


"""
    readimg(filepath)

Read out an image from the file `filepath`.

# Examples
```julia-repl
julia> raw = take();

julia> saveimg("image.h5", raw)

julia> raw = readimg("image.h5")
```
"""
function readimg(filepath)
    if !isfile(filepath)
        throw(SystemError("opening file \"$(filepath)\": No such file"))
    end

    HDF5.h5open(filepath, "r") do file
        arr = HDF5.read(file, "image")

        df = Dates.DateFormat("y-m-dTH:M:S.s")
        imageattr = HDF5.attrs(file["image"])
        meta = Dict{String,Any}()
        for k in HDF5.names(imageattr)
            if k == "starttime" || k == "finishtime"
                timestr = HDF5.read(imageattr, k)
                meta[k] = Dates.DateTime(timestr, df)
            elseif k == "masked_pixel_y"
                mask_y = HDF5.read(imageattr, "masked_pixel_y")
                mask_x = HDF5.read(imageattr, "masked_pixel_x")
                meta["mask"] = merge_arrays(mask_y, mask_x, Int)
            elseif k == "masked_pixel_x"
                # Skip here
                # All we need has done in 'masked_pixel_y' key
            else
                meta[k] = HDF5.read(imageattr, k)
            end
        end

        t = get(meta, "type", "")
        if t == "Ionspots"
            spot_y = HDF5.read(file, "spot_y")
            spot_x = HDF5.read(file, "spot_x")
            spots = merge_arrays(spot_y, spot_x, Float64)
            Ionspots(spots, arr, meta)
        elseif t == "Ionimage"
            Ionimage(arr, meta)
        else
            Rawimage(arr, meta)
        end
    end
end


function split_to_arrays(tuplelist, T)
    if isempty(tuplelist)
        yarray = T[]
        xarray = T[]
    else
        yarray = map(t -> T(t[1]), tuplelist)
        xarray = map(t -> T(t[2]), tuplelist)
    end
    yarray, xarray
end


function merge_arrays(yarray, xarray, T)
    tuplelist = NTuple{2,T}[]
    for coord in zip(yarray, xarray)
        push!(tuplelist, coord)
    end
    tuplelist
end
