function __init__()
    # Default config
    Config.initialize_CONFIG!()

    # Load config file
    confdir = Config.confdir()
    confpath = joinpath(confdir, Config.CONFIG_FILE_NAME)
    Config.CONFIG_FILE_PATH[] = confpath
    if isfile(confpath)
        Config.loadconf()
    end

    # Set data directory
    datadir = Config.DATADIR[]
    if datadir != ""
        @info "Images will be saved into '$(datadir)'"
    end

    # Load data from the data directory
    if datadir != ""
        empty(b)
        messaged = false
        i = 1
        while true
            path = joinpath(datadir, "$(i).hdf5")
            if !isfile(path)
                break
            end
            if !messaged
                @info "Loading data from '$(datadir)'..."
                messaged = true
            end
            push!(b, readimg(path))
            i += 1
        end
    end

    # Search camera
    camera_found = false
    if Pco.API.DLL_FOUND[]
        try
            spec, interface = Pco.searchcamera(Config.CONFIG["interface"]["code"])
            Config.CONFIG["interface"]["code"] = interface
            @info "$(spec[:camera]) is found on $(spec[:interface])"
            camera_found = true
            Pco.describecamera(Config.CONFIG["interface"]["code"], (:General,:Parameters))
        catch e
            if isdefined(e, :msg) && occursin("SDK DLL error 800a300d", e.msg)
                @warn "No camera was found"
            else
                rethrow()
            end
        end
    end
    if !camera_found
        # If no camera is connected, use a virtual camera
        Config.CONFIG["interface"]["code"] = Pco.API.PCO_INTERFACE_VIRTUAL
        @info "The virtual camera will be used"
    end
    nothing
end
