module Image

import GR
import Printf

function image_eltype(bg)
    if isa(bg, Nothing)
        return Int64
    end
    if isa(bg, Integer)
        return Int64
    end
    if isa(bg, Real)
        return Float64
    end
    if isa(bg, AbstractMatrix{<:Integer})
        return Int64
    end
    if isa(bg, AbstractMatrix{<:Real})
        return Float64
    end
    error("Invalid type of background")
end


# To make sure the safety of @inbounds
function check_sizes(img, bg::Union{Nothing,Real}) end
function check_sizes(img, bg::AbstractMatrix{<:Real})
    if size(img) != size(bg)
        throw(DimensionMismatch("Invalid size of background matrix: image $(reverse(size(img))), background $(reverse(size(bg)))"))
    end
end


transpose_background(bg::Union{Nothing,Real}) = bg
function transpose_background(bg::AbstractMatrix{<:Real})
    collect(transpose(bg))  # row-major
end


function transpose_mask(mask)
    if isempty(mask)
        return mask
    end

    maskᵀ = NTuple{2,Int}[]
    for (y, x) in mask
        push!(maskᵀ, (x, y))
    end
    maskᵀ
end


function background_correction!(img, bg::Nothing)
    img
end

function background_correction!(img, bg::Real)
    height, width = size(img)
    for x in 1:width, y in 1:height
        @inbounds img[y, x] -= bg
    end
    img
end

function background_correction!(img, bg::AbstractMatrix{<:Real})
    height, width = size(img)
    for x in 1:width, y in 1:height
        @inbounds img[y, x] -= bg[y, x]
    end
    img
end


function ismasked(y, x, mask)
    if isempty(mask)
        return false
    end
    (y, x) in mask
end


function sum!(img, buf, y::Integer, x::Integer, bg::Nothing)
    @inbounds img[y, x] += buf[y, x]
    img
end

function sum!(img, buf, y::Integer, x::Integer, bg::Real)
    @inbounds img[y, x] += buf[y, x]
    @inbounds img[y, x] -= bg
    img
end

function sum!(img, buf, y::Integer, x::Integer, bg::AbstractMatrix{<:Real})
    @inbounds img[y, x] += buf[y, x]
    @inbounds img[y, x] -= bg[y, x]
    img
end

function sum!(img, buf, bg)
    height, width = size(buf)
    for x in 1:width, y in 1:height
        sum!(img, buf, y, x, bg)
    end
    img
end

function sum!(img, cam, bufnrs, bg)
    for bnr in bufnrs
        buf = cam.buffer[bnr]
        sum!(img, buf.image, bg)
    end
    img
end


# B. Y. Chang, R. C. Hoetzlein, J. A. Mueller, J. D. Geiser and P. L. Houston,
# Improved two-dimensional product imaging: The real-time ion-counting method,
# Rev. Sci. Instrum., 1998, 69, 1665–1670.
function eventcount!(img, buf::AbstractMatrix{<:Real}, thr, bg=nothing,
                     mask=[])
    height, width = size(buf)
    count = 0
    for x in 2:width-1, y in 2:height-1
        if !isnoise(buf, y, x, thr, bg) && ispeak(buf, y, x, bg) && !ismasked(y, x, mask)
            img[y, x] += 1
            count += 1
        end
    end
    count
end

function eventcount!(img, cam, bufnrs, thr, bg=nothing, mask=[])
    count = 0
    for bnr in bufnrs
        buf = cam.buffer[bnr]
        count += eventcount!(img, buf.image, thr, bg, mask)
    end
    count
end


function isnoise(buf, y, x, thr, bg::Nothing)
    @inbounds buf[y, x] < thr
end

function isnoise(buf, y, x, thr, bg::Real)
    @inbounds buf[y, x] < thr + bg
end

function isnoise(buf, y, x, thr, bg::AbstractMatrix{<:Real})
    @inbounds buf[y, x] < thr + bg[y, x]
end


const window = Matrix{Float64}(undef, 3, 3)

function ispeak(buf, y, x)
    @inbounds (buf[y-1, x] < buf[y, x] >= buf[y+1, x] &&
               buf[y, x-1] < buf[y, x] >= buf[y, x+1])
end

function ispeak(buf, y, x, bg::Nothing)
    ispeak(buf, y, x)
end

function ispeak(buf, y, x, bg::Real)
    global window
    window .= buf[y-1:y+1, x-1:x+1]
    window .-= bg
    ispeak(window, 2, 2)
end

function ispeak(buf, y, x, bg::AbstractMatrix{<:Real})
    global window
    window .= buf[y-1:y+1, x-1:x+1]
    window .-= bg[y-1:y+1, x-1:x+1]
    ispeak(window, 2, 2)
end


const AREAMIN = 3
const AREAMAX = 1000

# W. Li, S. D. Chambreau, S. A. Lahankar and A. G. Suits, Megapixel ion
# imaging with standard video, Rev. Sci. Instrum., 2005, 76, 063106.
function countcentroid!(spots, buf::AbstractMatrix{<:Real}, thr,
                        checked::Array{Bool,2}, bg=nothing, mask=[])
    bloblist = extractBLOB(buf, thr, checked, bg, mask)
    n = length(spots)
    for blob in bloblist
        if length(blob) <= AREAMIN || AREAMAX <= length(blob)
            continue
        end
        y, x, I = 0.0, 0.0, 0
        for (yb, xb) in blob
            @inbounds Ib = buf[yb, xb]
            y += yb*Ib
            x += xb*Ib
            I += Ib
        end
        coord = (y/I, x/I)
        push!(spots, coord)
    end
    spots
end

function countcentroid!(spots, buf::AbstractMatrix{<:Real}, thr,
                        bg=nothing, mask=[])
    checked = similar(buf, Bool)
    countcentroid!(spots, buf, thr, checked, bg, mask)
end

function countcentroid!(spots, cam, bufnrs, thr, checked::Array{Bool,2},
                        bg=nothing, mask=[])
    for bnr in bufnrs
        buf = cam.buffer[bnr]
        countcentroid!(spots, buf.image, thr, checked, bg, mask)
    end
    spots
end


# Binary Large OBject (BLOB) extraction
function extractBLOB(img, thr, checked::Matrix{Bool}, bg=nothing, mask=[])
    height, width = size(img)
    checked = fill!(checked, false)
    bloblist = Vector{NTuple{2,Int}}[]
    for x in 1:width, y in 1:height
        # Scan pixels until a new BLOB
        if !isnoise(img, y, x, thr, bg) && @inbounds !checked[y, x]
            # BLOB detection by Grass-Fire algorithm
            blob = NTuple{2,Int}[]
            grassfire!(blob, checked, img, y, x, thr, bg, mask)
            push!(bloblist, blob)
        end
    end
    bloblist
end

function extractBLOB(img, thr, bg=nothing, mask=[])
    checked = Array{Bool,2}(undef, size(img))
    extractBLOB(img, thr, checked, bg, mask)
end


# BLOB search by Grass-Fire algotithm
# REFERENCE:
# T. B. Moeslund, Introduction to Video and Image Processing: Building Real
# Systems and Applications, Springer London, London, 2012.
function grassfire!(blob::Vector{NTuple{2,Int}}, checked::Matrix{Bool},
                    img::AbstractMatrix{<:Real}, y::Integer, x::Integer,
                    thr::Real, bg=nothing, mask=[])
    height, width = size(img)
    checked[y, x] = true
    if !ismasked(y, x, mask)
        push!(blob, (y, x))
    end

    # check the below pixel
    if y+1 <= height && !isnoise(img, y+1, x, thr, bg) && !checked[y+1, x]
        grassfire!(blob, checked, img, y+1, x, thr, bg, mask)
    end

    # check the right pixel
    if x+1 <= width && !isnoise(img, y, x+1, thr, bg) && !checked[y, x+1]
        grassfire!(blob, checked, img, y, x+1, thr, bg, mask)
    end

    # back to the upper pixel
    if y-1 >= 1 && !isnoise(img, y-1, x, thr, bg) && !checked[y-1, x]
        grassfire!(blob, checked, img, y-1, x, thr, bg, mask)
    end

    # back to the left pixel
    if x-1 >= 1 && !isnoise(img, y, x-1, thr, bg) && !checked[y, x-1]
        grassfire!(blob, checked, img, y, x-1, thr, bg, mask)
    end
    blob
end


function normalize(c::Real, cmin, cmax)
    c = clamp(float(c), cmin, cmax) - cmin
    if cmin != cmax
        c /= cmax - cmin
    end
    c
end

function normalize(arr::AbstractMatrix)
    arr_min, arr_max = extrema(arr)
    normalize.(arr, arr_min, arr_max)
end


# The function for real-time image plot
# FIXME: This is very ugly, too many parameters are hard-coded...
function show(img, plotbuffer)
    GR.clearws()
    GR.setscale(0)      # determine log scale and flip axis
    width, height = size(img)
    cmin, cmax = extrema(img)
    cmin = max(cmin, 0)  # regards negative pixels as 0
    if length(img) != length(plotbuffer)
        throw(DimensionMismatch("Invalid size of plot buffer array"))
    end
    i = 1
    for y in 1:height, x in 1:width
        c = 1000 + 255*Image.normalize(img[x, y], cmin, cmax)
        @inbounds plotbuffer[i] = round(Int32, c)
        i += 1
    end

    left, right, bottom, top = plotimg(width, height, plotbuffer)
    plotcolorbar(right+0.01, bottom, top, cmin, cmax)

    GR.updatews()
    if GR.isinline()
        GR.show()
    end
end

function show(img::AbstractMatrix{T}) where {T<:Real}
    plotbuffer = Vector{T}(undef, length(img))
    show(img, plotbuffer)
end


function plotimg(width, height, arr)
    left = 0.0
    right = 0.85
    margin = (1 - (right - left)*height/width)/2
    bottom = margin
    top = 1.0 - margin
    GR.setcolormap(GR.COLORMAP_JET)
    GR.setviewport(left, right, bottom, top)
    GR.setwindow(0, 100, 0, 100)
    GR.cellarray(0, 100, 0, 100, width, height, arr)
    left, right, bottom, top
end


const COLORBAR = round.(Int32, 1000 .+ reverse!(collect(0:256)))
const COLORBARWIDTH = 0.032
const TICKNUM = 5

function plotcolorbar(left, bottom, top, cmin, cmax)
    GR.setviewport(left, left+COLORBARWIDTH, bottom+0.01, top-0.01)
    GR.setwindow(0, 100, 0, 100)
    GR.cellarray(0, 100, 0, 100, 1, 256, COLORBAR)
    tickvalues = unique(round.(Int, range(cmin, cmax, length=TICKNUM)))
    if length(tickvalues) == 1
        tickheights = [bottom]
    else
        tickheights = range(bottom, top-0.02, length=length(tickvalues))
    end
    GR.setcharheight(0.02)
    for i in 1:length(tickvalues)
        ticklabel = Printf.@sprintf("%d", tickvalues[i])
        GR.text(left+COLORBARWIDTH+0.01, tickheights[i], ticklabel)
    end
end

end
