interface:
    ############################################################################
    # Setting an appropriate number to the `code` key allow to find your
    # camera quicker when starting-up. Uncomment one of the following lines.
    ############################################################################

    # code: 0x0001    # Firewire interface
    # code: 0x0002    # Cameralink Matrox Solios / Helios
    # code: 0x0003    # Cameralink Silicon Software Me3
    # code: 0x0004    # Cameralink National Instruments
    # code: 0x0005    # Gigabit Ethernet
    # code: 0x0006    # USB 2.0
    # code: 0x0007    # Cameralink Silicon Software Me4
    # code: 0x0008    # USB 3.0 and USB 3.1 Gen1
    # code: 0x0009    # WLan (Only control path, not data path)
    # code: 0x0011    # Cameralink HS
    # code: 0x1000    # Virtual Camera for test
    # code: 0xffff    # Scan all the available interfaces to find camera.



measurement:
    take:
        #######################################################################
        # Trigger mode
        # Without any special reason, use `0` (internal) or `2` (external).
        #######################################################################

        # trig: 0                # Internal (auto) trigger
        # trig: 1                # Software trigger
        # trig: 2                # External trigger
        # trig: 3                # External exposure control
        # trig: 4                # External synchronized

        #######################################################################
        # Delay time from a trigger to shutter open
        # The available delay time depends on a camera. Check the spec sheet.
        #######################################################################

        # delay: 0.0             # No delay
        # delay: 0.001           # 1 millisecond delay

        #######################################################################
        # Exposure time
        # The available exposure time depends on a camera. Check the spec sheet.
        #######################################################################

        # exposure: 0.01         # 10 millisecond exposure

        #######################################################################
        # The pixel scan rate of the camera
        # it is related to the buffer readout time and thus the resulted
        # frame-per-second (fps).
        # The available pixel rate depends on a camera. Check the spec sheet.
        #######################################################################

        # pixelrate: 10000000    # 10 MHz

        #######################################################################
        # Vertical and horizontal binning
        # The available binning sizes depend on a camera. Check the spec sheet.
        #######################################################################

        # vbin: 1                # No vertical binning
        # vbin: 2                # Vertical binning with 2 pixel width

        # hbin: 1                # No horizontal binning
        # hbin: 2                # Horizontal binning with 2 pixel width

        #######################################################################
        # Sensor format
        # Some cameras have the second camera format. Check the spec sheet.
        #######################################################################

        # exsensor: 0            # Standard sensor format
        # exsensor: 1            # Extended sensor format

        #######################################################################
        # Hotpixel correction
        # Some cameras have the hotpixel correction mode. Check the spec sheet.
        #######################################################################

        # hotpixcorr: 0          # Hotpixel correction OFF
        # hotpixcorr: 1          # Hotpixel correction ON

        #######################################################################
        # Current conversion factor
        # The available values depend on a camera. Check the spec sheet.
        #######################################################################

        # convfactor: 100

        #######################################################################
        # Region-of-interest (ROI)
        # The available ROI size depends on a camera. Check the spec sheet.
        # (y0, x0) and (y1, x1) are the upper-left and bottom-right corners
        # respectively.
        #######################################################################

        # roi: [1, 1, 601, 801]  # [y0, x0, y1, x1]

        #######################################################################
        # Mute progress messaging
        #######################################################################

        # progress: true         # Messaging ON
        # progress: false        # Messaging OFF

    live:
        #######################################################################
        # The similar set of options with `take` are available
        #######################################################################

        # trig: 0
        # delay: 0.0
        # exposure: 0.01
        # pixelrate: 10000000
        # vbin: 1
        # hbin: 1
        # exsensor: 0
        # hotpixcorr: 0
        # convfactor: 100
        # roi: [1, 1, 601, 801]

    imaging:
        #######################################################################
        # The similar set of options with `take` are available
        #######################################################################

        # trig: 2
        # delay: 0.0
        # exposure: 0.01
        # pixelrate: 10000000
        # vbin: 1
        # hbin: 1
        # exsensor: 0
        # hotpixcorr: 0
        # convfactor: 100
        # roi: [1, 1, 601, 801]
        # progress: true

        #######################################################################
        # Mute the monitor window
        #######################################################################

        # monitor: true          # monitor ON
        # monitor: false         # monitor OFF

        #######################################################################
        # Monitor update interval
        # The monitor window is updated every
        #   {interval} × {number of internal image buffers} / 2
        # laser shots. The {number of internal image buffers} would be 16 for
        # almost all cameras.
        #######################################################################

        # interval: 2            # Update every 16 laser shots

    imagingsp:
        #######################################################################
        # The similar set of options with `take` are available
        #######################################################################

        # trig: 2
        # delay: 0.0
        # exposure: 0.01
        # pixelrate: 10000000
        # vbin: 1
        # hbin: 1
        # exsensor: 0
        # hotpixcorr: 0
        # convfactor: 100
        # roi: [1, 1, 601, 801]
        # progress: true
        # monitor: true
        # interval: 2



fileio:
    ############################################################################
    # The path to the directory to save experimental data.
    # Ionimaging package makes subfolders named by year and date under this
    # directory. It is recommended to use the full path to the directory.
    ############################################################################

    ### The plane expression
    # directory: "C:\\Users\\username\\Documents"

    ### Using environmental variables is ok
    # directory: "%USERPROFILE%\\Documents"
